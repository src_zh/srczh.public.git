package com.srczh.util.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.nio.charset.Charset;

import com.srczh.util.CharsetUtil;




/**
 * URL资源访问类
 */
public class UrlResource implements Resource, Serializable{
	private static final long serialVersionUID = 1L;
	
	protected URL url;
	protected String name;
	
	//-------------------------------------------------------------------------------------- Constructor start
	/**
	 * 构造
	 * @param url URL
	 */
	public UrlResource(URL url) {
		this(url, null);
	}
	
	/**
	 * 构造
	 * @param url URL，允许为空
	 * @param name 资源名称
	 */
	public UrlResource(URL url, String name) {
		this.url = url;
		this.name = name==null ? FileUtil.getName(url.getPath()) : "";
	}
	

	//-------------------------------------------------------------------------------------- Constructor end
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public URL getUrl(){
		return this.url;
	}
	
	@Override
	public InputStream getStream(){
		try {
			return url.openStream();
		} catch (IOException e) {
		}
		return null;
	}
	
	/**
	 * 获得Reader
	 * @param charset 编码
	 * @return {@link BufferedReader}
	 * @since 3.0.1
	 */
	public BufferedReader getReader(Charset charset){
		return IoUtil.getReader(getStream(), charset);
	}
	
	//------------------------------------------------------------------------------- read
	@Override
	public String readStr(Charset charset) {
		BufferedReader reader = null;
		try {
			reader = getReader(charset);
			return IoUtil.read(reader);
		} finally {
			IoUtil.close(reader);
		}
	}
	
	@Override
	public String readUtf8Str() {
		return readStr(CharsetUtil.CHARSET_UTF_8);
	}
	
	@Override
	public byte[] readBytes(){
		InputStream in = null;
		try {
			in = getStream();
			return IoUtil.readBytes(in);
		} finally {
			IoUtil.close(in);
		}
	}
	
	/**
	 * 获得File
	 * @return {@link File}
	 */
	public File getFile(){
		return FileUtil.file(this.url.toString());
	}
	
	/**
	 * 返回路径
	 * @return 返回URL路径
	 */
	@Override
	public String toString() {
		return (null == this.url) ? "null" : this.url.toString();
	}
}
