package com.srczh.util.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.srczh.util.StrUtil;




/**
 * 文件包装器，扩展文件对象
 */
public class SrcFile implements Serializable{
	private static final long serialVersionUID = 1L;
	
	protected File file;
	protected Charset charset;
	
	/** 默认编码：UTF-8 */
	public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

	// ------------------------------------------------------- Constructor start
	/**
	 * 构造
	 * @param file 文件
	 * @param charset 编码，使用 {@link CharsetUtil}
	 */
	public SrcFile(File file, Charset charset) {
		this.file = file;
		this.charset = charset;
	}
	// ------------------------------------------------------- Constructor end
	/**
	 * 构造
	 */
	public SrcFile(String filePath) {
		this(filePath, DEFAULT_CHARSET);
	}
	
	/**
	 * 构造
	 * @param filePath 文件路径，相对路径会被转换为相对于ClassPath的路径
	 * @param charset 编码，使用 {@link CharsetUtil}
	 */
	public SrcFile(String filePath, Charset charset) {
		this(FileUtil.file(filePath), charset);
	}
	
	/**
	 * 构造<br>
	 * 编码使用 {@link FileWrapper#DEFAULT_CHARSET}
	 * @param file 文件
	 */
	public SrcFile(File file) {
		this(file, DEFAULT_CHARSET);
	}
	// ------------------------------------------------------- Setters and Getters start start
	/**
	 * 获得文件
	 * @return 文件
	 */
	public File getFile() {
		return file;
	}

	/**
	 * 设置文件
	 * @param file 文件
	 * @return 自身
	 */
	public SrcFile setFile(File file) {
		this.file = file;
		return this;
	}

	/**
	 * 获得字符集编码
	 * @return 编码
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * 设置字符集编码
	 * @param charset 编码
	 * @return 自身
	 */
	public SrcFile setCharset(Charset charset) {
		this.charset = charset;
		return this;
	}
	

	/**
	 * 可读的文件大小
	 * @return 大小
	 */
	public String FileSize() {
		return FileUtil.readableFileSize(file.length());
	}
	
	// ------------------------------------------------------- w end
	
	/**
	 * 创建 FileReader
	 * @param file 文件
	 * @param charset 编码，使用 {@link CharsetUtil}
	 * @return {@link FileReader}
	 */
	public static SrcFile create(File file, Charset charset){
		return new SrcFile(file, charset);
	}
	
	/**
	 * 创建 FileReader, 编码：{@link FileWrapper#DEFAULT_CHARSET}
	 * @param file 文件
	 * @return {@link FileReader}
	 */
	public static SrcFile create(File file){
		return new SrcFile(file);
	}
	
	/**
	 * 读取文件所有数据<br>
	 * 文件的长度不能超过 {@link Integer#MAX_VALUE}
	 * 
	 * @return 字节码
	 * @ IO异常
	 */
	public byte[] readBytes()  {
		long len = file.length();
		if (len >= Integer.MAX_VALUE) {
			//throw new IORuntimeException("File is larger then max array size");
		}

		byte[] bytes = new byte[(int) len];
		FileInputStream in = null;
		int readLength;
		try {
			in = new FileInputStream(file);
			readLength = in.read(bytes);
			if(readLength < len){
				throw new IOException(StrUtil.format("File length is [{}] but read [{}]!", len, readLength));
			}
		} catch (Exception e) {
			//throw new IORuntimeException(e);
		} finally {
			IoUtil.close(in);
		}

		return bytes;
	}

	/**
	 * 读取文件内容
	 * 
	 * @return 内容
	 * @ IO异常
	 */
	public String readString() {
		return new String(readBytes(), this.charset);
	}

	/**
	 * 从文件中读取每一行数据
	 * 
	 * @param <T> 集合类型
	 * @param collection 集合
	 * @return 文件中的每行内容的集合
	 * @ IO异常
	 */
	public <T extends Collection<String>> T readLines(T collection)  {
		BufferedReader reader = null;
		try {
			reader = FileUtil.getReader(file, charset);
			String line;
			while (true) {
				line = reader.readLine();
				if (line == null) {
					break;
				}
				collection.add(line);
			}
			return collection;
		} catch (IOException e) {
			//throw new IORuntimeException(e);
		} finally {
			IoUtil.close(reader);
		}
		return null;
	}
	
	/**
	 * 按照行处理文件内容
	 * 
	 * @param lineHandler 行处理器
	 * @ IO异常
	 * @since 3.0.9
	 */
//	public void readLines(LineHandler lineHandler) {
//		BufferedReader reader = null;
//		try {
//			reader = FileUtil.getReader(file, charset);
//			IoUtil.readLines(reader, lineHandler);
//		} finally {
//			IoUtil.close(reader);
//		}
//	}
	
	/**
	 * 从文件中读取每一行数据
	 * 
	 * @return 文件中的每行内容的集合
	 * @ IO异常
	 */
	public List<String> readLines()  {
		return readLines(new ArrayList<String>());
	}

	/**
	 * 按照给定的readerHandler读取文件中的数据
	 * 
	 * @param <T> 读取的结果对象类型
	 * @param readerHandler Reader处理类
	 * @return 从文件中read出的数据
	 * @ IO异常
	 */
	public <T> T read(ReaderHandler<T> readerHandler)  {
		BufferedReader reader = null;
		T result = null;
		try {
			reader = FileUtil.getReader(this.file, charset);
			result = readerHandler.handle(reader);
		} catch (IOException e) {
			//throw new IORuntimeException(e);
		} finally {
			IoUtil.close(reader);
		}
		return result;
	}

	/**
	 * 获得一个文件读取器
	 * 
	 * @return BufferedReader对象
	 * @ IO异常
	 */
	public BufferedReader getReader()  {
		return IoUtil.getReader(getInputStream(), this.charset);
	}

	/**
	 * 获得输入流
	 * 
	 * @return 输入流
	 * @ IO异常
	 */
	public BufferedInputStream getInputStream()  {
		try {
			return new BufferedInputStream(new FileInputStream(this.file));
		} catch (IOException e) {
			//throw new IORuntimeException(e);
		}
		return null;
	}
	
	/**
	 * 将文件写入流中
	 * 
	 * @param out 流
	 * @return File
	 * @ IO异常
	 */
	public File writeToStream(OutputStream out)  {
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			IoUtil.copy(in, out);
		}catch (IOException e) {
			//throw new IORuntimeException(e);
		} finally {
			IoUtil.close(in);
		}
		return this.file;
	}

	// -------------------------------------------------------------------------- Interface start
	/**
	 * Reader处理接口
	 * 
	 * @author Luxiaolei
	 *
	 * @param <T> Reader处理返回结果类型
	 */
	public interface ReaderHandler<T> {
		public T handle(BufferedReader reader) throws IOException;
	}
	// -------------------------------------------------------------------------- Interface end
	
	
	//////////////////////
	
	/**
	 * 将String写入文件
	 * 
	 * @param content 写入的内容
	 * @param isAppend 是否追加
	 * @return 目标文件
	 * @ IO异常
	 */
	public File write(String content, boolean isAppend)  {
		BufferedWriter writer = null;
		try {
			writer = getWriter(isAppend);
			writer.write(content);
			writer.flush();
		}catch(IOException e){
		//	throw new IORuntimeException(e);
		}finally {
			IoUtil.close(writer);
		}
		return file;
	}
	
	/**
	 * 将String写入文件，覆盖模式
	 * 
	 * @param content 写入的内容
	 * @return 目标文件
	 * @ IO异常
	 */
	public File write(String content)  {
		return write(content, false);
	}

	/**
	 * 将String写入文件，追加模式
	 * 
	 * @param content 写入的内容
	 * @return 写入的文件
	 * @ IO异常
	 */
	public File append(String content)  {
		return write(content, true);
	}

	/**
	 * 将列表写入文件，覆盖模式
	 * 
	 * @param <T> 集合元素类型
	 * @param list 列表
	 * @return 目标文件
	 * @ IO异常
	 */
	public <T> File writeLines(Collection<T> list)  {
		return writeLines(list, false);
	}

	/**
	 * 将列表写入文件，追加模式
	 * 
	 * @param <T> 集合元素类型
	 * @param list 列表
	 * @return 目标文件
	 * @ IO异常
	 */
	public <T> File appendLines(Collection<T> list)  {
		return writeLines(list, true);
	}

	/**
	 * 将列表写入文件
	 * 
	 * @param <T> 集合元素类型
	 * @param list 列表
	 * @param isAppend 是否追加
	 * @return 目标文件
	 * @ IO异常
	 */
	public <T> File writeLines(Collection<T> list, boolean isAppend)  {
		return writeLines(list, null, isAppend);
	}
	
	/**
	 * 将列表写入文件
	 * 
	 * @param <T> 集合元素类型
	 * @param list 列表
	 * @param lineSeparator 换行符枚举（Windows、Mac或Linux换行符）
	 * @param isAppend 是否追加
	 * @return 目标文件
	 * @ IO异常
	 * @since 3.1.0
	 */
	public <T> File writeLines(Collection<T> list, LineSeparator lineSeparator, boolean isAppend)  {
		try (PrintWriter writer = getPrintWriter(isAppend)){
			for (T t : list) {
				if (null != t) {
					writer.println(t.toString());
					printNewLine(writer, lineSeparator);
					writer.flush();
				}
			}
		}
		return this.file;
	}
	
	/**
	 * 将Map写入文件，每个键值对为一行，一行中键与值之间使用kvSeparator分隔
	 * 
	 * @param Map<String,Object> map
	 * @param kvSeparator 键和值之间的分隔符，如果传入null使用默认分隔符" = "
	 * @param isAppend 是否追加
	 * @return 目标文件
	 * @ IO异常
	 * @since 4.0.5
	 */
	public File writeMap(Map<?, ?> map, String kvSeparator, boolean isAppend)  {
		return writeMap(map, null, kvSeparator, isAppend);
	}
	
	/**
	 * 将Map写入文件，每个键值对为一行，一行中键与值之间使用kvSeparator分隔
	 * 
	 * @param Map<String,Object> map
	 * @param lineSeparator 换行符枚举（Windows、Mac或Linux换行符）
	 * @param kvSeparator 键和值之间的分隔符，如果传入null使用默认分隔符" = "
	 * @param isAppend 是否追加
	 * @return 目标文件
	 * @ IO异常
	 * @since 4.0.5
	 */
	public File writeMap(Map<?, ?> map, LineSeparator lineSeparator, String kvSeparator, boolean isAppend)  {
		if(null == kvSeparator) {
			kvSeparator = " = ";
		}
		try(PrintWriter writer  = getPrintWriter(isAppend)) {
			for (Entry<?, ?> entry : map.entrySet()) {
				if (null != entry) {
					writer.println(StrUtil.format("{}{}{}", entry.getKey(), kvSeparator, entry.getValue()));
					printNewLine(writer, lineSeparator);
					writer.flush();
				}
			}
		}
		return this.file;
	}
	
	/**
	 * 写入数据到文件
	 * 
	 * @param data 数据
	 * @param off 数据开始位置
	 * @param len 数据长度
	 * @return 目标文件
	 * @ IO异常
	 */
	public File write(byte[] data, int off, int len)  {
		return write(data, off, len, false);
	}
	
	/**
	 * 追加数据到文件
	 * 
	 * @param data 数据
	 * @param off 数据开始位置
	 * @param len 数据长度
	 * @return 目标文件
	 * @ IO异常
	 */
	public File append(byte[] data, int off, int len)  {
		return write(data, off, len, true);
	}

	/**
	 * 写入数据到文件
	 * 
	 * @param data 数据
	 * @param off 数据开始位置
	 * @param len 数据长度
	 * @param isAppend 是否追加模式
	 * @return 目标文件
	 * @ IO异常
	 */
	public File write(byte[] data, int off, int len, boolean isAppend)  {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileUtil.touch(file), isAppend);
			out.write(data, off, len);
			out.flush();
		}catch(IOException e){
		//	throw new IORuntimeException(e);
		} finally {
			IoUtil.close(out);
		}
		return file;
	}

	/**
	 * 将流的内容写入文件<br>
	 * 此方法不会关闭输入流
	 * 
	 * @param in 输入流，不关闭
	 * @return dest
	 * @ IO异常
	 */
	public File writeFromStream(InputStream in)  {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileUtil.touch(file));
			IoUtil.copy(in, out);
		}catch (IOException e) {
		//	throw new IORuntimeException(e);
		} finally {
			IoUtil.close(out);
		}
		return file;
	}

	/**
	 * 获得一个输出流对象
	 * 
	 * @return 输出流对象
	 * @ IO异常
	 */
	public BufferedOutputStream getOutputStream()  {
		try {
			return new BufferedOutputStream(new FileOutputStream(FileUtil.touch(file)));
		} catch (IOException e) {
		//	throw new IORuntimeException(e);
		}
		return null;
	}

	/**
	 * 获得一个带缓存的写入对象
	 * 
	 * @param isAppend 是否追加
	 * @return BufferedReader对象
	 * @ IO异常
	 */
	public BufferedWriter getWriter(boolean isAppend)  {
		try {
			return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FileUtil.touch(file), isAppend), charset));
		} catch (Exception e) {
		//	throw new IORuntimeException(e);
		}
		return null;
	}

	/**
	 * 获得一个打印写入对象，可以有print
	 * 
	 * @param isAppend 是否追加
	 * @return 打印对象
	 * @  IO异常
	 */
	public PrintWriter getPrintWriter(boolean isAppend)  {
		return new PrintWriter(getWriter(isAppend));
	}
	
	/**
	 * 检查文件
	 * 
	 * @  IO异常
	 */
	private void checkFile()  {
	//	Assert.notNull(file, "File to write content is null !");
		if(this.file.exists() && false == file.isFile()){
		//	throw new IORuntimeException("File [{}] is not a file !", this.file.getAbsoluteFile());
			return;
		}
	}
	
	/**
	 * 打印新行
	 * @param writer Writer
	 * @param lineSeparator 换行符枚举
	 * @since 4.0.5
	 */
	private void printNewLine(PrintWriter writer, LineSeparator lineSeparator) {
		if(null == lineSeparator) {
			//默认换行符
			writer.println();
		}else {
			//自定义换行符
			writer.println(lineSeparator.getValue());
		}
	}
	
	
	public static boolean delFoled(File dir) {
		if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = delFoled(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
	}

}
