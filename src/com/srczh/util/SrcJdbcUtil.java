package com.srczh.util;

 
import com.srczh.manage.util.Page;
import com.srczh.manage.util.Slog;

import java.io.BufferedReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 
/**
 * @title JDBC工具类
 *      连接数据库
 *      执行SQL
 *      查询对象
 *      查询集合
 */
public class SrcJdbcUtil {
 

	/** 驱动名称 */
    private static String DRIVER_NAME = "";
    /** 数据库链接地址 */
    private static  String URL = "";
    /** 用户名 */
    private static  String USERNAME = "";
    /** 密码 */
    private static  String PASSWORD = "";
 
    /** 定义连接 */
    private static Connection CONN=null;
    /** 定义STMT */
    private static PreparedStatement STMT;
    /** 定义结果集 */
    private static ResultSet RS;
 
    
   
    public SrcJdbcUtil() {
 		super();
 	}
    
    
    public SrcJdbcUtil(Map<String,Object> map) {
    	initJdbc(map);
 	    /** 初始化加载链接 */
        try {
        	if(CONN==null){
        		Class.forName(DRIVER_NAME);
        		Slog.println(URL);
        		CONN = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        	}
        } catch (ClassNotFoundException e) {
            System.err.println("驱动加载失败");
            e.printStackTrace();
        } catch (SQLException e) {
            System.err.println("数据库链接异常");
            e.printStackTrace();
        }
 	}
    


    /** 获取链接 */
    public  Connection getConn() {
    	if(CONN==null){
			try {
				Slog.println(URL);
				CONN = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
        return CONN;
    }

    

    /**测试连接返回是否正常*/
    public int cdbs(){
    	CONN=getConn();
    	if(CONN==null){
    		return 0;
    	}
    	return 1;
    }
 
    /** 关闭链接,释放资源 */
    public static void close() {
        try {
            if (RS != null) {
            	RS.close();
            	RS = null;
            }
            if (STMT != null) {
            	STMT.close();
            	STMT = null;
            }
 
            if (CONN != null) {
                CONN.close();
                CONN = null;
            }
        } catch (SQLException e) {
            Slog.error("资源释放发生异常");
        }
    }
 
    /**
     * 获取指定数据库下所有的表名
     * @param dbNm
     * @return
     */
    public static List<Object> getAllTableName(String sql) {
        List<Object> result = new ArrayList<Object>();
        Statement st = null;
        try {
            st = CONN.createStatement();
           // ResultSet rs = st.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA='" + dbNm + "'");
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                result.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            close();
        }
        return result;
    }
 
    /** 执行SQL返回ResultSet */
    public  ResultSet executeSql(String sql, Object... args) {
        try {
//            Slog.println("准备执行SQL : \n" + sql);
        	if(CONN==null){
        		this.getConn();
        	}
            STMT = CONN.prepareStatement(sql);
            
            if (null != args && args.length != 0) {
                for (int i = 0; i < args.length; i++) {
                	STMT.setObject(i + 1, args[i]);
                }
            }
            //if(sql.toLowerCase().indexOf("select")>-1) {
            	 RS = STMT.executeQuery();
           // }
            
           
        } catch (SQLException e) {
            System.err.println("数据查询异常");
            e.printStackTrace();
        }
        return RS;
    }
    
    /**
     * 执行ddl ,dml 语句
     * @param sql
     * @return
     * @throws SQLException 
     */
    public  int excuteCommd( String sql) throws SQLException {
    	if(CONN==null){
    		this.getConn();
    	}
    	Slog.println(" 执行sql="+sql);
    	
		//Statement  statement =CONN.createStatement();/*创建一个对象用于执行SQL语句*/
		Statement statement=CONN.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY); 
		return statement.executeUpdate(sql);
    }
    
    public boolean excute(String sql) throws SQLException {
    	if(CONN==null){
    		this.getConn();
    	}
    	Slog.println(" 执行sql="+sql);
		Statement statement=CONN.createStatement(); 
		return statement.execute(sql);
    }
    
 
    /**
     * @title 查询数据结果 , 并封装为对象
     */
    public  Map<String, Object> excuteQuery( String sql, Object... args) {
        try {
        	Slog.println(" 执行sql="+sql);
            RS = executeSql(sql, args);
            ResultSetMetaData metaData = RS.getMetaData();
 
            Map<String, Object> resultmap = new HashMap<>();
            if (RS.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    String columnname = metaData.getColumnLabel(i);
                    Object obj = RS.getObject(i);
                    if(obj instanceof oracle.sql.CLOB){
                    	oracle.sql.CLOB clob = (oracle.sql.CLOB)obj; 
                    	resultmap.put(columnname, getClog(clob));
                    }else{
                    	resultmap.put(columnname, obj);
                    }
                }
            }
            //return JSON.parseObject(JSON.toJSONString(resultmap), klass);
            return resultmap;
        } catch (Exception e) {
            Slog.println("数据查询异常");
            e.printStackTrace();
        } finally {
            close();
        }
        return new HashMap<>();
    }
    
    //sqlservrer查询
    public  Map<String, Object> excuteSQLSever( String sql,Page page) {
    	Map<String,Object> rsmap = new HashMap<>();
    	try {
	    	if(CONN==null){
	    		this.getConn();
	    	}
			STMT = CONN.prepareStatement(sql);
	    	//STMT=(PreparedStatement) CONN.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY); 
	        RS = STMT.executeQuery();
	    	  
	        RS.last() ;
	    	int allCount=RS.getRow() ; //获得最大的行数
	    	page.setRsCount(allCount);
	    	rsmap.put("page", page);
	    	
	    	if(page.pageNo<=1) {
	    		//RS.first() ;
	    		RS.beforeFirst();
	    	}else {
	    		RS.absolute((page.getPageNo())*page.getPageNum()) ;
	    	}
	    	List<Map<String, Object>> resultList = new ArrayList<>();
	    	 while(RS.next())  {
	    		 ResultSetMetaData metaData = RS.getMetaData();
	                int columnCount = metaData.getColumnCount();
	                Map<String, Object> resultmap = new HashMap<>();
	                for (int i = 1; i <= columnCount; i++) {
	                	String col = metaData.getColumnName(i);
	                	String val = RS.getString(i);
	                    resultmap.put(col, val);
	                }
	                resultList.add(resultmap);
	    	 }
	    	rsmap.put("list", resultList);
	    	 return rsmap;
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}finally {
    		close();
		}
    	 return new HashMap<>();
    }
    
    
    public static String getClog(oracle.sql.CLOB colb) throws Exception{
    	String content="";
	    BufferedReader a = new BufferedReader(colb.getCharacterStream()); //以字符流的方式读入BufferedReader 
	    String str = ""; 
			while ((str = a.readLine()) != null) {
				content = content.concat(str); 
			}
    	return content;
    }
    
 
    /**
     * 按sql查询数据，不区别分页情况
     * @param sql
     * @param args
     * @return
     */
    public List<Map<String, Object>> excuteQueryToList( String sql, Object... args) {
        try {
            RS = executeSql(sql, args);
            List<Map<String, Object>> resultList = new ArrayList<>();
            while (RS.next()) {
                ResultSetMetaData metaData = RS.getMetaData();
                int columnCount = metaData.getColumnCount();
                Map<String, Object> resultmap = new HashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                	String col = metaData.getColumnName(i);
                	String val = RS.getString(i);
                //	Slog.println(val);
                    resultmap.put(col, val);
                }
                resultList.add(resultmap);
            }
 
          //  return JSON.parseArray(JSON.toJSONString(resultList), klass);
            return resultList;
        } catch (Exception e) {
            System.err.println("数据查询异常");
            e.printStackTrace();
        } finally {
            close();
        }
        return new ArrayList<>();
    }
    
    /**
     * 查询分页数据
     * @param page 需要指定dbType
     * @param sql  自动封装分页sql
     * @param args 过滤参数
     * @return 返回map中包含page以及list
     */
    public  Map<String, Object> excutePageQuery(Map<String,Object> map, String sql, Object... args) {
    	Map<String,Object> rmap=new HashMap<>();


    	Page page=(Page) map.get("page");
    	String csql="";
    	//查询总数
    	if("mysql".equals( map.get("DB_TYPE"))) {
    		//String sqlb =sql.substring(sql.indexOf("*")+1);
    		csql = "select count(1) COUNT from( "+sql+") count";
    	}else if("oracle".equals( map.get("DB_TYPE"))) {
    		csql= "select count(1) COUNT from ("+sql+")";
    	}
    	Map<String,Object> cmap= excuteQuery(csql, args);
    	int count=Integer.parseInt(cmap.get("COUNT")+"");
    	page.setRsCount(count);
    	sql=page.initPageSql(page.getDbType(),sql);
    	List<?> list= excuteQueryToList(sql,args);
    	rmap.put("page",page);
    	rmap.put("list", list);
    	return rmap;
    }
    
	public void initJdbc(Map<String,Object> map){
		//jdbc:oracle:thin:@127.0.0.1:1521:dbname
		//jdbc:mysql://localhost:3306/mydb
		//jdbc:db2://127.0.0.1:50000/dbname
		//jdbc:postgresql://localhost/dbname
		//jdbc:sybase:Tds:localhost:5007/dbname
		//sql2000//jdbc:microsoft:sqlserver://localhost:1433;DatabaseName=dbname
		//sql2005//jdbc:sqlserver://localhost:1433; DatabaseName=dbname
		String url="";
		if("oracle".equals(map.get("DB_TYPE"))){
			url="jdbc:oracle:thin:@"+map.get("DB_SERVER")+":"+map.get("DB_PORT")+":"+map.get("DB_SID");
		}
		if("mysql".equals(map.get("DB_TYPE"))){
			url="jdbc:mysql://"+map.get("DB_SERVER")+":"+map.get("DB_PORT")+"/"+map.get("DB_SID")+"?useUnicode=true&amp;characterEncoding=utf-8&amp;useSSL=false";
		}
		if("sqlserver".equals(map.get("DB_TYPE"))){
			url="jdbc:sqlserver://"+map.get("DB_SERVER")+":"+map.get("DB_PORT")+";DatabaseName="+map.get("DB_SID");
		}
		if("db2".equals(map.get("DB_TYPE"))){
			url="jdbc:db2://"+map.get("DB_SERVER")+":"+map.get("DB_PORT")+"/"+map.get("DB_SID");
		}
		if("postgresql".equals(map.get("DB_TYPE"))){
			url="jdbc:postgresql://"+map.get("DB_SERVER")+":"+map.get("DB_PORT")+"/"+map.get("DB_SID");
		}

		String user=map.get("DB_USER")+"";
		String passwd=map.get("DB_PASSWD")+"";
		this.URL=url;
		this.USERNAME=user;
		this.PASSWORD=passwd;
		initDataType(url);
		//SrcJdbcUtil sdb = new SrcJdbcUtil(url,user,passwd);
	}
	
    //数据库类型
    public void initDataType(String url){
    	if((url.toLowerCase()).indexOf("oracle")>0){
 			DRIVER_NAME="oracle.jdbc.driver.OracleDriver";
 		}
 		if((url.toLowerCase()).indexOf("mysql")>0){
 			DRIVER_NAME="com.mysql.jdbc.Driver";
 		}
 		if((url.toLowerCase()).indexOf("sqlserver")>0){
 		  // driverName="com.microsoft.sqlserver.jdbc.SQLServerDriver";
 			DRIVER_NAME="com.microsoft.sqlserver.jdbc.SQLServerDriver";
 		}
 		if((url.toLowerCase()).indexOf("db2")>0){
 			DRIVER_NAME="com.ibm.db2.jcc.DB2Driver";
 		}
 		if((url.toLowerCase()).indexOf("sybase")>0){
 			DRIVER_NAME="com.sybase.jdbc.SybDriver";
 		}
 		if((url.toLowerCase()).indexOf("PostgreSQL")>0){
 			DRIVER_NAME="org.postgresql.Driver";
 		}
    }
 
}
