package com.srczh.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sql语句创建工具
 * @author zhoujun
 *
 */
public class SrcSqlUtil {

	/**
	 * 组织新增sql创建参数
	 * @param map
	 * @param da map and pd
	 * @return map
	 */
	public static Map<String,Object> initInsertSql(Map<String,Object> map,String da) {
		String colns = "";
		String pval="";
		String vals="";
		String mval="";//mybtais专用
		String kcol="";
		if("mysql".equals(map.get("dbtype"))) {
			List<?> cls=(List<?>) map.get("cls");
			for(int i=0;i<cls.size();i++) {
				Map<String,Object> cmap=(Map) cls.get(i);
				String col=cmap.get("COLUMN_NAME")+"";
				String ckey=cmap.get("COLUMN_KEY")+"";
				if(!"PRI".equals(ckey)) {//非主键
					if(colns==""){
						colns=col;
					} else {
						colns=colns+","+col;
					}
					if(pval==""){
						pval="?";
					}else{
						pval=pval+",?";
					}
					if(vals==""){
						vals=da+".get(\""+col.toLowerCase()+"\")";
					}else {
						vals=vals+","+da+".get(\""+col.toLowerCase()+"\")"; 
					}
					if(mval=="") {
						mval="#{"+col+"}";
					}else {
						mval=mval+","+"#{"+col+"}";
					}
				}else {
					if(kcol=="") {
						kcol=col;
					}else {
						kcol=kcol+","+col;
					}
				}
			}
		}
		else {
			List<?> cls=(List<?>) map.get("cls");
			for(int i=0;i<cls.size();i++) {
				Map<String,Object> cmap=(Map<String,Object>) cls.get(i);
				String col=cmap.get("COLUMN_NAME")+"";
				String ckey=cmap.get("PRI")+"";
				if(!"P".equals(ckey)) {
					if(colns=="") {
						colns=col;
					} else {
						colns=colns+","+col;
					}
					if(pval==""){
						pval="?";
					}else{
						pval=pval+",?";
					}
					if(vals=="") { 
						vals=da+".get(\""+col.toLowerCase()+"\")";
					}else { 
						vals=vals+","+da+".get(\""+col.toLowerCase()+"\")"; 
					}
					if(mval=="") {
						mval="#{"+col+"}";
					}else {
						mval=mval+","+"#{"+col+"}";
					}
				}else {
					if(kcol=="") {
						kcol=col;
					}else {
						kcol=kcol+","+col;
					}
					if("oracle".equals(map.get("dbtype"))) {
						if(mval=="") {
							mval="#{"+col+"}";
						}else {
							mval=mval+","+"#{"+col+"}";
						}
					}
				}
			}
		}
		Map<String,Object> imap = new HashMap<>();
		imap.put("col", colns);
		imap.put("cval", pval);
		imap.put("vals", vals);
		imap.put("kcol", kcol);
		imap.put("mval", mval);
		return imap;
	}
	
	/**
	 * 组织删除sql创建参数
	 * @param map
	 * @return map
	 */
	public static Map<String,Object> initDeleteSql(Map<String,Object> map,String da) {
		String colns = "";
		String vals="";
		String key="";
		List cls=(List) map.get("cls");
		for(int i=0;i<cls.size();i++) {
			Map<String,Object> cmap=(Map<String,Object>) cls.get(i);
			if("mysql".equals(map.get("dbtype"))) {
				String col=cmap.get("COLUMN_NAME")+"";
				String ckey=cmap.get("COLUMN_KEY")+"";
				if("PRI".equals(ckey)) {
					colns = col+" =? ";
					vals=da+".get(\""+col.toLowerCase()+"\")";
					key=col;
					break;
				}
			}
			else {
				String col=cmap.get("COLUMN_NAME")+"";
				String ckey=cmap.get("PRI")+"";
				if("P".equalsIgnoreCase(ckey)) {
					colns = col+" =? ";
					vals=da+".get(\""+col.toLowerCase()+"\")";
					key=col;
					break;
				}
			}
		}
		Map<String,Object> imap = new HashMap<>();
		imap.put("col", colns);
		imap.put("vals", vals);
		imap.put("key", key);
		return imap;
	}
	
	/**
	 * 组织更新sql创建参数
	 * @param map
	 * @return map
	 */
	public static Map<String,Object> initUpdateSql(Map<String,Object> map,String da) {
		String where="",wval="";
		String sets = "",sval="",set="";
		String msets="";
		String key="";
		List<?> cls=(List<?>) map.get("cls");
		for(int i=0;i<cls.size();i++) {
			Map<String,Object> cmap=(Map<String,Object>) cls.get(i);
			if("mysql".equals(map.get("dbtype"))) {
				String col=cmap.get("COLUMN_NAME")+"";
				String ckey=cmap.get("COLUMN_KEY")+"";		
				if("PRI".equals(ckey)) {
					where = col+" =? ";
					key=col;
					wval=da+".get(\""+col+"\")";
				}else {
					if(sets=="") {sets=col+"=?"; sval=da+".get(\""+col+"\")";}
					else {sets=sets+","+col+"=?"; sval=sval+","+da+".get(\""+col+"\")";}
					if(set=="") {set=col;}else {set=set+","+col;}
					if(msets=="") {msets=col+"="+"#{"+col+"}";}else {msets=msets+","+col+"="+"#{"+col+"}";}
				}
			}
			else{
				String col=cmap.get("COLUMN_NAME")+"";
				String ckey=cmap.get("PRI")+"";		
				if("P".equalsIgnoreCase(ckey)) {
					where = col+" =? ";
					wval=da+".get(\""+col.toLowerCase()+"\")";
				}else {
					if(sets=="") {sets=col.toLowerCase()+"=?"; sval=da+".get(\""+col.toLowerCase()+"\")";}
					else {sets=sets+","+col.toLowerCase()+"=?"; sval=sval+","+da+".get(\""+col.toLowerCase()+"\")";}
					if(msets=="") {msets=col+"="+"#{"+col.toLowerCase()+"}";}else {msets=msets+col+"="+"#{"+col.toLowerCase()+"}";}
				}
			}
		}
		Map<String,Object> imap = new HashMap<>();
		imap.put("sets", sets);
		imap.put("set", set);
		imap.put("sval", sval);
		imap.put("where", where);
		imap.put("wval", wval);
		imap.put("key", key);
		imap.put("msets", msets);
		return imap;
	}
	
	/**
	 * 组织查询sql创建参数
	 * 封装主键列到pkcols，值到pkvals
	 * @param map
	 * @return map
	 */
	public static Map<String,Object> initSelectSql(Map<String,Object> map,String da) {
		List<?> cls=(List<?>) map.get("cls");
		String cols="";
		String pkcolns = "";
		String pkvals="",key="";
		for(int i=0;i<cls.size();i++) {
			Map<String,Object> cmap=(Map<String,Object>) cls.get(i);
			String col=cmap.get("COLUMN_NAME")+"";
			if(cols=="") {
				cols=col;
			}else {
				cols=cols+","+col;
			}
			if("mysql".equals(map.get("dbtype"))) {
				String ckey=cmap.get("COLUMN_KEY")+"";		
				if("PRI".equals(ckey)) {
					pkcolns = col+" =? ";
					pkvals=da+".get(\""+col+"\")";
					if(key=="")key=col;
					break;
				}
			}
			else  {
				String ckey=cmap.get("PRI")+"";		
				if("P".equalsIgnoreCase(ckey)) {
					pkcolns = col+" =? ";
					pkvals=da+".get(\""+col.toLowerCase()+"\")";
					if(key=="")key=col;
					break;
				}
			}
		}
		Map<String,Object> imap = new HashMap<>();
		imap.put("pkcolns", pkcolns);
		imap.put("pkvals", pkvals);
		imap.put("key", key);
		imap.put("cols", cols);
		return imap;
	}

}