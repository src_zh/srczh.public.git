package com.srczh.manage;

/**
 * SrcView 展示对象
 * @author zhoujun
 */
public class SrcView {
	
	private String url;
	private String name;
	private Object data;
//	private String bind;
	private String type;
//	private String path;

	public SrcView(){}
	
	public SrcView(String name){
		this.name=name;
	}
	
	public String getUrl(){
		return url;
	}
	public void setUrl(String url){
		this.url = url;
	}
	public Object getData(){
		return data;
	}
	public void setData(Object data){
		this.data = data;
	}
//	public String getBind() {
//		return bind;
//	}
//	public void setBind(String bind){
//		this.bind = bind;
//	}
	public String getType(){
		return type;
	}
	public void setType(String type){
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

//	public String getPath() {
//		return path;
//	}
//
//	public void setPath(String path) {
//		this.path = path;
//	}
//	
	
}
