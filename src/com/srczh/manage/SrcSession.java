package com.srczh.manage;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.srczh.manage.util.Slog;

public class SrcSession {
	private static SrcSession instance;
	Map<String,HttpSession> sessionMap;
	
	 private SrcSession() {  
         sessionMap = new HashMap<String,HttpSession>();  
     }  

     public static SrcSession getInstance() {  
         if (instance == null) {  
             instance = new SrcSession();  
         }  
         return instance;  
     }  

     public synchronized void addSession(HttpSession session) {  
         if (session != null) {  
        	 Slog.println("add session="+session.getId());
        	 sessionMap.put(session.getId(), session);  
         }  
     }  

     public synchronized void delSession(HttpSession session) {  
         if (session != null) {  
        	 sessionMap.remove(session.getId());  
         }  
     }  

     public synchronized HttpSession getSession(String sessionID) {  
         if (sessionID == null) {  
             return null;  
         }  
         return sessionMap.get(sessionID);  
     }  

     
     public int getNum() {
    	 return sessionMap.size();
     }
	
}
