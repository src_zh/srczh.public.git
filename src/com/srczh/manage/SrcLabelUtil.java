package com.srczh.manage;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import com.srczh.manage.util.DateUtil;
import com.srczh.manage.util.Page;
import com.srczh.manage.util.SrcJson;

/**
 * src标签工具类
 * @author zhoujun
 */
public class SrcLabelUtil {

	public static Object sess;
	public static Object apps;
	
	/**清除html注释代码*/
	public static String initNotes(String code){
		 Pattern r = Pattern.compile(SrcConfig.SRC_NOTES);
		 Matcher m = r.matcher(code);
	       while(m.find()) {
	    	   String bqt=m.group();
	    	   code=code.replace(bqt, "");
	       }
	    return code;
	}
	
	
	/**
	 * 执行srcList标签
	 * @param htm
	 * @param data
	 * @return
	 */
	public static String initSrcList(String code,Object obj){
		//判断是否有src_list标签
		 Pattern r = Pattern.compile(SrcConfig.SRC_LIST);
		 Matcher m = r.matcher(code);
	       while(m.find()) {
	    	   String bqt=m.group();
	    	   String bq= bqt.substring(0,bqt.indexOf(">")+1);
	    	   //标签体需要保持
	    	   if(bq.indexOf(" keep ")>-1 || bq.indexOf(" keep>")>-1) {
	    		  String nbqt=bq.replace(" keep", "");
	    		   code=code.replace(bq, nbqt);
	    		   continue;
	    	   }
	    	   String bqtop=m.group(1).trim();
	    	   String bqtb=(m.group(2)).trim(); 
 
//	    	   if("".equals(bqtb)){bqtb=bqt.substring(bqt.indexOf(">")+1,bqt.lastIndexOf("<"));}
	    	   StringBuffer sb=new StringBuffer();
	    	   //标签体不为空
	    	   if(!"".equals(bqtop)&&!"".equals(bqtb)){
	    		   Object nobj=null;
	    		   
	    		//判断是否是application中的值
	   			 if(!"".equals(bqtop)&&"@".equals(bqtop.substring(0, 1))){
	   				bqtop = bqtop.substring(1);
	   				ServletContext as =(ServletContext) apps;
	   				if(as!=null) {
		   				if(bqtop.indexOf(".")>0){
		   					String amtop=bqtop.substring(0, bqtop.indexOf("."));
		   					String bmtop=bqtop.substring(bqtop.indexOf("."));
			   				obj=((ServletContext)as).getAttribute(amtop);
			   				nobj=SrcLabelUtil.getBindData2(obj, bmtop,"",0);
		   				}else{
		   					if(as!=null) {
		   						nobj=((ServletContext)as).getAttribute(bqtop);
		   					}
		   				}
	   				}
	    		   
	    		   //是绑定session
	   			 }else if(!"".equals(bqtop)&&"$".equals(bqtop.substring(0,1))){
	    			   //去掉session标识符$
	    			   bqtop=bqtop.substring(1);
	    			   HttpSession os =(HttpSession) sess;
	    			   if(os!=null) {
		    			   if(bqtop.indexOf(".")>-1){
		    				   String amtop=bqtop.substring(0, bqtop.indexOf("."));
			   				   String bmtop=bqtop.substring(bqtop.indexOf("."));
		    				   obj=((HttpSession)os).getAttribute(amtop);
		    				   nobj=SrcLabelUtil.getBindData2(obj, bmtop,"",0); 
		    			   }else{
		    				   if(os!=null) {
		    					   nobj=((HttpSession)os).getAttribute(bqtop);
		    				   }
		    			   }
	    			   }
	    			   
	    			//request绑定 得到对象
	    		   }else{
		    			// nobj=SrcLabelUtil.toSrcBind(jsp, obj, "", 0);
	    			   if(bqtop.indexOf(".")>-1){
	    				   String a=bqtop.substring(0,bqtop.indexOf("."));
	    				   String b=bqtop.substring(bqtop.indexOf(".")+1);
	    				   if(!"".equals(a)) {
	    					   nobj=SrcLabelUtil.getBindData(obj, a,"", 0); 
	    					   nobj=SrcLabelUtil.getBindData(nobj, b,"'", 0); 
	    				   }else {
	    					   nobj=SrcLabelUtil.getBindData(obj, b,"", 0);  
	    				   }
	    			   }else {
	    				   nobj=SrcLabelUtil.getBindData(obj, bqtop,"",0); 
	    			   }
	    		   }
	   			 
		   		  if(nobj instanceof List){
	  				   List<?> ols= (List<Object>) nobj;
	  				   for(int i=0;i<ols.size();i++) {
	  					   
	  					   //判难断是否存在下级标签ifelse
	  					   String tmp=bqtb;
	  					   tmp=SrcLabelUtil.initSrcIfelse(tmp,ols.get(i),SrcConfig.SRC_IFELSE,i);
	  					   tmp=SrcLabelUtil.initSrcIfelse(tmp,ols.get(i),SrcConfig.SRC_IF,i);
	  					   tmp=toSrcBind(tmp, ols.get(i),"", i);
	  					   tmp=out(tmp,ols.get(i),0);
	  					   tmp=toSrcFormat(tmp,ols.get(i),0);
	  					   tmp=toSrcWhen(tmp,ols.get(i),0);
	  					   sb.append(tmp);
	  				   }
	  			   }else if(nobj!=null&& nobj.getClass().isArray()){
	  				   for(int i=0;i<Array.getLength(nobj);i++) {
	  					   
	  					   //后面解决遍历中加入条件是针对主对象还是遍历后对象
	  					   //解决方案 将主对象及遍历对象一同传入标签解释方法
	  					   Object zobj=Array.get(nobj, i);

	  					   //判难断是否存在下级标签ifelse
	  					   String tmp=bqtb;
	  					   tmp=initSrcIfelse(tmp,nobj,SrcConfig.SRC_IFELSE,i);
	  					   tmp=initSrcIfelse(tmp,nobj,SrcConfig.SRC_IF,i);
	  					   tmp=toSrcBind(tmp, zobj,"", i);
	  					   tmp=out(tmp,zobj,i);
	  					   tmp=toSrcFormat(tmp,zobj,i);
	  					   tmp=toSrcWhen(tmp,zobj,i);
	  					   sb.append(tmp);
	  				   }
	  			 }else if(nobj!=null&& nobj instanceof Map){
	  				   Map<String,Object> mobj = (Map) nobj;
	  				   int i=0;
	  				   String tmp="";
	  				   for(Object key:mobj.keySet()) {
	  					   Object mobjs = mobj.get(key);
	  					   i++;
	  					   tmp=initSrcIfelse(bqtb, mobjs,SrcConfig.SRC_IFELSE,i);
	  					   tmp=initSrcIfelse(bqtb, mobjs,SrcConfig.SRC_IF,i);
	  					   //System.out.println(i+"==="+key);
	  					   tmp=toSrcBind(bqtb, mobj,"'", i);
	  					   sb.append(tmp);
	  				   }
	  			   }
	    	   }
	    	   code=code.replace(bqt, sb+"");
	      }
	      return code;
	}

	/**可以判断字符中是否有ifelse 或单独if
	 * 先判断表达式ture或市false 取if还是else中内容
	 * 在获取内容中绑定数据
	 */
	public static String initSrcIfelse(String htm,Object data,String ifes,int i){
		Pattern re = null;
		re=Pattern.compile(ifes);
		Matcher m = re.matcher(htm);
		StringBuffer sb=new StringBuffer("");
		String bqt="",mtop="",mbod="";
		while(m.find()) {
			bqt=m.group();
			String bq=bqt.substring(0,bqt.indexOf(">")+1);
			if(bq.indexOf(" keep ")>-1 || bq.indexOf(" keep>")>-1) {
				String nbqt=bq.replace(" keep", "");
				htm=htm.replace(bq, nbqt);
				sb.append(htm);
				continue;
			}
			String ielse="",ielse2="";
			mtop=m.group(1).trim();
			mbod=m.group(2);
			if(ifes.indexOf("src_else")>0 && bqt.indexOf("<src_else>")>0){
				ielse2=m.group(3)==null?"":m.group(3);
			}

			
			Object o=null;
				//java执行js判断
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine se = manager.getEngineByName("js");
				try {
					//表达式是否有绑定值
					//判断条件属性数据类型
					String mtops="";
					String arcs="'";
					if(mtop.indexOf("(length)")>0||mtop.indexOf(".length")>=0||mtop.indexOf("(index)")>0||mtop.indexOf(".index")>=0) {
						arcs="";
					}
					mtops=compareBind(mtop,data,"'",i);
					
					//System.out.println(" if("+mtops+")");
					//特定输出
					mtops=out(mtops,data,i);
					
					//表达式为真 取if内容
					o=se.eval(mtops);
					
					//表达式为真
					int a= m.start()>0?m.start()-1:m.start();
					int e=m.end();
					String ts=htm.substring(0,a);
					if((o instanceof Integer && (Integer)o>0) ||(o instanceof String && !"".equals(o) && !"false".equals(o.toString().trim()))||
							(o instanceof Boolean && (Boolean)o)){
//						System.out.println("z="+htm.length()+" a="+a+" e="+e);
						
						//sb已有值 存在多个ifelse
						if("".equals(sb)||sb.length()==0) {
							sb.append(ts+mbod+htm.substring(e));
						}else {
							int start=sb.toString().indexOf(bqt);
							int end=start+bqt.length();
							sb=sb.replace(start, end, mbod);
						}
						
					//表达式为假	
					}else{
//						//sb已有值 存在多个ifelse
						if("".equals(sb.toString())||sb.length()==0) {
							sb.append(ts);
							//如果有else部分 ，取else部分内容
							if(ifes.indexOf("src_else")>0){
								//特定输出
								//ielse2=out(ielse2,data,i);
								sb.append(ielse2+htm.substring(e));
							}else {
								//特定输出
								//htm=out(htm,data,i);
								sb.append(htm.substring(e));
							}
						}else {
							int start=sb.toString().indexOf(bqt);
							int end=start+bqt.length();
							if(ifes.indexOf("src_else")>0){
								//特定输出
								//ielse2=out(ielse2,data,i);
								sb=sb.replace(start, end, ielse2);
							}else {
								sb=sb.replace(start, end, "");
							}
						}
					}
				} catch (ScriptException e) {
					e.printStackTrace();
				}
			}
		if(bqt==""){
			sb.append(htm);
		}else {
//			if(sb.indexOf("<src_if ")>-1) {
//				sb.append(initSrcIfelse(sb.toString(),data,SrcConfig.SRC_IFELSE,0));
//				sb.append(initSrcIfelse(sb.toString(),data,SrcConfig.SRC_IF,0));
//			}
		}
		return sb.toString();
	}


	
	/**
	 * 比较运算绑定参数
	 * @param src
	 * @param data
	 * @param arcs
	 * @param i
	 * @return
	 */
	public static String compareBind(String src,Object data,String arcs,int i) {
		Pattern re =Pattern.compile(SrcConfig.SRC_BIND);
 		Matcher m = re.matcher(src);
 		Object odata=null;
 		while (m.find()) {
 			 String sh=m.group();
 			 String mtop=m.group(1).trim();
 			 
 			//判断是否是application中的值
 			 if(!"".equals(mtop)&&"@".equals(mtop.substring(0, 1))){
 				mtop = mtop.substring(1);
 				ServletContext as =(ServletContext) apps;
 				if(mtop.indexOf(".")>0){
 					String amtop=mtop.substring(0, mtop.indexOf("."));
 					String bmtop=mtop.substring(mtop.indexOf("."));
 					if(as!=null) {
 	 				    odata=((ServletContext)as).getAttribute(amtop);
 	 					if(odata!=null){
 	 						odata=SrcLabelUtil.getBindData(odata,bmtop.trim(),arcs,i);
 	 					}
 					}
 				}else if(mtop.indexOf("(")>0 || mtop.indexOf(")")>0){
 					String ak=mtop.substring(0,mtop.indexOf("("));
 	 				String bk=mtop.substring(mtop.indexOf("("));
 	 				if(!"".equals(ak)) {
 	 					odata=((ServletContext)as).getAttribute(ak);
 	 					odata=SrcLabelUtil.getBindData(odata,bk.trim(),arcs,i);
 	 				}
 				}else{
 					if(as!=null) {
 						odata=((ServletContext)as).getAttribute(mtop);
 					}
 				}
 			 
 			 //判断是否是session中的值
 			 }else if(!"".equals(mtop)&&"$".equals(mtop.substring(0, 1))){
 				mtop=mtop.substring(1);
 				
 				HttpSession os =(HttpSession) sess;
 				if(mtop.indexOf(".")>0){
 					String amtop=mtop.substring(0, mtop.indexOf("."));
 					String bmtop=mtop.substring(mtop.indexOf("."));
 					if(os!=null) {
 						odata=((HttpSession)os).getAttribute(amtop);
	 					if(odata!=null){
	 						odata=SrcLabelUtil.getBindData(odata,bmtop.trim(),arcs,i);
	 					}
 					}
 				}else if(mtop.indexOf("(")>0 || mtop.indexOf(")")>0) {
 					String ak=mtop.substring(0,mtop.indexOf("("));
 	 				String bk=mtop.substring(mtop.indexOf("("));
 	 				if(!"".equals(ak)) {
 	 					if(os!=null) {
 	 						odata=((HttpSession)os).getAttribute(ak);
 	 						odata=SrcLabelUtil.getBindData(odata,bk.trim(),arcs,i);
 	 					}
 	 				}
 				}else{
 					if(os!=null) {
 						odata=((HttpSession)os).getAttribute(mtop);
 					}
 				}
 			//request中取值
 			}else{
 				
 				if(mtop.indexOf(".")>=0){
 					if(mtop==".index" || ".index".equals(mtop)) {
 						odata= i;
 					}else {
 	 					String amtop=mtop.substring(0, mtop.indexOf("."));
 	 					String bmtop=mtop.substring(mtop.indexOf("."));
 	 					
 	 					if(!"".equals(amtop)) {
 	 						odata=SrcLabelUtil.getBindData(data,amtop.trim(),arcs,i);
 	 						odata=SrcLabelUtil.getBindData(odata,bmtop.trim(),arcs,i);
 	 					}else {
 	 						odata=SrcLabelUtil.getBindData(data,bmtop.trim(),arcs,i);
 	 					}
 					}
 					
 				}else if(mtop.indexOf("(")==0 || mtop.indexOf(")")>0) {
 					String ak=mtop.substring(0,mtop.indexOf("("));
 	 				String bk=mtop.substring(mtop.indexOf("("));
 	 				if(!"".equals(ak)) {
 	 					odata=SrcLabelUtil.getBindData(data,ak.trim(),arcs,i);
 	 					odata=SrcLabelUtil.getBindData(odata,bk.trim(),arcs,i);
 	 				}else {
 	 					odata=SrcLabelUtil.getBindData(data,bk.trim(),arcs,i);
 	 				}
 				}else{
 					if(data instanceof String) {
 							odata=data;
 					}else {
 						odata=SrcLabelUtil.getBindData(data,mtop.trim(),arcs,i);
 					}
 				}
 			}
 			
 			//odata 为下级属性值
			if(odata!=null){
				//if条件绑定赋值
				if(odata==null ||"".equals(odata)) {
					 src=src.replace(sh, "String("+(odata)+")");
				}else if(odata instanceof Map||odata instanceof List) {
					src=src.replace(sh, "String('"+odata.toString()+"')"); //odata有值 
				}else if(odata.getClass().isArray()){
					src=src.replace(sh, "String('"+SrcJson.ArrayToStr(odata)+"')");
				}else if(odata.getClass().isPrimitive() || odata instanceof String|| odata instanceof StringBuffer ||  odata instanceof Boolean) {
						src=src.replace(sh, "String('"+odata+"')");
				}else if(odata instanceof Integer ||odata instanceof Float|| odata instanceof Double || odata instanceof BigDecimal) {
					String result= new BigDecimal(odata.toString()).toString();
					src=src.replace(sh, "String("+result+")");
				}else if(odata instanceof Date) {
					java.text.DateFormat sf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					src=src.replace(sh, "String('"+sf.format(odata)+"')");
				}else {
					try {
						src=src.replace(sh, "String('"+SrcJson.BeantoJson(odata)+"')");
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else{
				src=src.replace(sh, "String()");
			}
 		 }
 		 return src;
	}
	
	
	
	
	
    /**
     * 是否有绑定参数值
     * src有可能数组下标，是.子项
     * arcs 标识值是否为null情况
     * data 可能是字符、数组、list、map
     */
    public static String toSrcBind(String src,Object data,String arcs,int i){
    	Pattern re =Pattern.compile(SrcConfig.SRC_BIND);
		Matcher m = re.matcher(src);
		Object odata=null;
		while (m.find()) {
			 String sh=m.group();
			 String mtop=m.group(1).trim();
			 
			//判断是否是application中的值
			 if(!"".equals(mtop)&&"@".equals(mtop.substring(0, 1))){
				mtop = mtop.substring(1);
				ServletContext as =(ServletContext) apps;
				if(mtop.indexOf(".")>0){
					String amtop=mtop.substring(0, mtop.indexOf("."));
					String bmtop=mtop.substring(mtop.indexOf("."));
					if(as!=null) {
	 				    odata=((ServletContext)as).getAttribute(amtop);
	 					if(odata!=null){
	 						odata=getBindData2(odata,bmtop.trim(),arcs,i);
	 					}
					}
				}else if(mtop.indexOf("(")>0 || mtop.indexOf(")")>0){
					String ak=mtop.substring(0,mtop.indexOf("("));
	 				String bk=mtop.substring(mtop.indexOf("("));
	 				if(!"".equals(ak)) {
	 					odata=((ServletContext)as).getAttribute(ak);
	 					odata=getBindData2(odata,bk.trim(),arcs,i);
	 				}
				}else{
					if(as!=null) {
						odata=((ServletContext)as).getAttribute(mtop);
					}
				}
			 
			//判断是否是session中的值
			 }else if(!"".equals(mtop)&&"$".equals(mtop.substring(0, 1))){
				mtop=mtop.substring(1);
				
				HttpSession os =(HttpSession) sess;
				if(mtop.indexOf(".")>0){
					String amtop=mtop.substring(0, mtop.indexOf("."));
					String bmtop=mtop.substring(mtop.indexOf("."));
					if(os!=null) {
						odata=((HttpSession)os).getAttribute(amtop);
	 					if(odata!=null){
	 						odata=getBindData2(odata,bmtop.trim(),arcs,i);
	 					}
					}
				}else if(mtop.indexOf("(")>0 || mtop.indexOf(")")>0) {
					String ak=mtop.substring(0,mtop.indexOf("("));
	 				String bk=mtop.substring(mtop.indexOf("("));
	 				if(!"".equals(ak)) {
	 					if(os!=null) {
	 						odata=((HttpSession)os).getAttribute(ak);
	 						odata=getBindData2(odata,bk.trim(),arcs,i);
	 					}
	 				}
				}else{
					if(os!=null) {
						odata=((HttpSession)os).getAttribute(mtop);
					}
				}
			//request中取值
			}else{
				
				if(mtop.indexOf(".")>=0){
					if(mtop==".index" || ".index".equals(mtop)) {
						odata= i+"";
					}else {
	 					String amtop=mtop.substring(0, mtop.indexOf("."));
	 					String bmtop=mtop.substring(mtop.indexOf("."));
	 					
	 					if(!"".equals(amtop)) {
	 						odata=getBindData2(data,amtop.trim(),arcs,i);
	 						odata=getBindData2(odata,bmtop.trim(),arcs,i);
	 					}else {
	 						odata=getBindData2(data,bmtop.trim(),arcs,i);
	 					}
					}
					
				}else if(mtop.indexOf("(")==0 || mtop.indexOf(")")>0) {
					String ak=mtop.substring(0,mtop.indexOf("("));
	 				String bk=mtop.substring(mtop.indexOf("("));
	 				if(!"".equals(ak)) {
	 					odata=getBindData2(data,ak.trim(),arcs,i);
	 					odata=getBindData2(odata,bk.trim(),arcs,i);
	 				}else {
	 					odata=getBindData2(data,bk.trim(),arcs,i);
	 				}
				}else{
					if(data instanceof String) {
						odata=data;
					}else {
						odata=getBindData2(data,mtop.trim(),arcs,i);
					}
				}
			}
			
			//odata 为下级属性值
			if(odata!=null){
				
//				String tname=odata.getClass().getName();
//				Slog.println("tname="+tname);
				
				//if条件绑定赋值
				 if(odata==null ||"".equals(odata)) {
					 src=src.replace(sh, odata+"");
				}else if(odata instanceof Map||odata instanceof List) {
					src=src.replace(sh, odata.toString()); //odata有值 
				}else if(odata.getClass().isArray()){
					src=src.replace(sh, SrcJson.ArrayToStr(odata));
				}else if(odata.getClass().isPrimitive() || odata instanceof String|| odata instanceof StringBuffer || odata instanceof Integer ||  odata instanceof Boolean) {
					src=src.replace(sh, odata+"");
					
				}else if(odata instanceof Float|| odata instanceof Double || odata instanceof BigDecimal) {
					String result= new BigDecimal(odata.toString()).toString();
					src=src.replace(sh, result);
				}else if(odata instanceof Date) {
					java.text.DateFormat sf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					src=src.replace(sh, sf.format(odata));
					
				}else {
					try {
						src=src.replace(sh, SrcJson.BeantoJson(odata).toString());
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else{
				if("".equals(arcs)){
					src=src.replace(sh, "");
				}else{
					src=src.replace(sh, arcs+odata+arcs);
				}
			}
		 }
		 return src;
    }
     
   /**
    * 获取标签体整体结构
    */
    public static List<String> changqbt(String source){
    	List<String> result = new ArrayList<String>();
    	if(source==null||"".equals(source)){
    		result.add("");
    		result.add("");
    		result.add("");
    		return null;
    	}
    	String bqtop="";
    	String bqbod="";
    	String bqfoot=source.substring(source.lastIndexOf("<")-1);
    	source=source.substring(0,source.lastIndexOf("<")-1);
    	Pattern re =Pattern.compile(SrcConfig.SRC_BQ);
		Matcher m = re.matcher(source);
		int cn=0;
		 while (m.find()&&cn==0) {
			 cn++;
			 bqtop=m.group();
			 bqbod =source.substring( m.end());
	       }
    	result.add(bqtop);
    	result.add(bqbod);
    	result.add(bqfoot);
    	return result;
    }
    
  
     /**
      * 指定特殊输出
      * @param src
      * @param data
      * @param i
      * @return
      */
     public static String out(String src,Object data,int i){
    	 Pattern re =Pattern.compile(SrcConfig.SRC_OUT);
  		 Matcher m = re.matcher(src);
  		 String rs="";
  		 while (m.find()) {
  			 String bqt=m.group(0);
  			 String type=m.group(1).trim();
  			 String bq=bqt.substring(0,bqt.indexOf(">")+1);
  			 if(bq.indexOf(" keep ")>-1 || bq.indexOf(" keep>")>-1) {
  				String nbqt=bq.replace(" keep", "");
  				src=src.replace(bq, nbqt);
  				 continue;
  			 }
  			 String mtop=toSrcBind(m.group(2).trim(), data, "",i);

  			 //html转意
  			 if("html".equalsIgnoreCase(type)) {
  				 StringBuffer buffer = new StringBuffer();
  				 for (int n = 0; n < mtop.length(); n++) {
  					 char c = mtop.charAt(n);
  					 switch (c) {
  					 case '<':
  						 buffer.append("&lt;");
  						 break;
  					 case '>':
  						 buffer.append("&gt;");
  						 break;
  					 case '&':
  						 buffer.append("&amp;");
  						 break;
  					 case '"':
  						 buffer.append("&quot;");
  						 break;
  					 default:
  						 buffer.append(c);
  					 }
  				 }
  				 rs = buffer.toString();
  			 }
  			 //转unicode
  			 else if( "unicode".equalsIgnoreCase(type) ) {
  				 StringBuffer buffer = new StringBuffer();
  				 for (int n = 0; n < mtop.length(); n++) {
  					 buffer.append("\\u" + Integer.toString(mtop.charAt(n), 16));
  				 }
  				 rs = buffer.toString();
  				 //反转unicode
  			 }else if("notunicode".equalsIgnoreCase(type)) {
  				 StringBuffer sb = new StringBuffer();
  				 String[] hex = mtop.split("\\\\u");
  				 for (int n = 1; n < hex.length; n++) {
  					 int index = Integer.parseInt(hex[n], 16);
  					 sb.append((char) index);
  				 }
  				 rs = sb.toString();
  				 //字符脱敏
  			 }else if("sensitive".equalsIgnoreCase(type)) {
  				 int ls= mtop.length();
  				 int js=Math.round(ls/2);
  				 if(js>=ls) {
  					 js=js--;
  				 }else if(js==0) {
  					 js=1;
  				 }
  				 String jms=mtop.substring(0,js);
  				 mtop=mtop.replace(jms, "***");
  				 rs = mtop;
  				//代码运算
  			 }else if("operation".equalsIgnoreCase(type)) {
  				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine se = manager.getEngineByName("js");
				try {
					rs=se.eval(mtop).toString();
				} catch (ScriptException e) {
					e.printStackTrace();
				}
  			 }
  			 src=src.replace(bqt, rs);
  		 }
  		 
  		 return src;
     }
     
     
 
     /**
      * 保留标签
      * @param src
      * @return
      */
     public static String keepSrcBind(String src) {
    		Pattern re =Pattern.compile(SrcConfig.SRC_KEEPBIND);
     		Matcher m = re.matcher(src);
     		while (m.find()) {
     			String bqt=m.group();
     			String nbqt=bqt.replace("{![", "{[");
     			src=src.replace(bqt, nbqt);
     		}
     	return src;
     }
     /**
      * 特殊属性绑定
      * @param obj
      * @param da
      * @param sign
      * @param index
      * @return
      */
     public static Object getBindData(Object obj,String da,String sign,int index){
     	da=da.trim();
     	StringBuffer sb=new StringBuffer();
     	
 		if(obj instanceof Map){
 			//如果没有下级
 			if(da.indexOf(".")<0 && da.indexOf("(")<0){
 				return ((Map)obj).get(da)==null?"":((Map)obj).get(da);
 			}
 			//有点
 			if(da.indexOf(".")>=0) {
 				String tmp=da.substring(da.indexOf(".")+1);
 				return ((Map)obj).get(tmp);
 			}
 			//括号中
 			if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				if("key".equals(bk.trim())){
 					int n=0;
					Set<String> keySet = ((Map)obj).keySet();
		     		for(Iterator<String> it = keySet.iterator();it.hasNext();) {
		     			String key=it.next();
		     			if(index==0) {
 			     			if(sb.length()>0) {
 			     				sb.append(","+key);
 			     			}else {
 			     				sb.append(sign+key+sign);
 			     			}
		     			}else {
		     				n++;
		     				if(n==index) {
 			     				sb.append(sign+key+sign);
 			     				return sb;
 			     			}
		     			}
		     		}
 					return sb;
 				}
 				else if("val".equals(bk)) {
 						Set<String> keySet = ((Map)obj).keySet();
 						int n=0;
 						for(Iterator<String> it = keySet.iterator();it.hasNext();) {
 			     			String key=it.next();
 			     			if(index==0) {
	 			     			if(sb.length()>0) {
	 			     				sb.append(","+((Map)obj).get(key));
	 			     			}else {
	 			     				sb.append(sign+((Map)obj).get(key)+sign);
	 			     			}
 			     			}else {
 			     				n++;
 			     				if(n==index) {
 	 			     				sb.append(sign+((Map)obj).get(key)+sign);
 	 			     				return sb;
 	 			     			}
 			     			}
 			     		}
 			     	return sb;
 				}
 				else if("length".equals(bk.trim())) {
 					return ((Map)obj).size();
 				}else if("string".equals(bk.trim())) {
 	 				return SrcJson.mapToJson((Map)obj).toString();
 				}else {
 					return ((Map)obj).get(bk.trim());
 				}
 			}
 			
 		}else if(obj instanceof List) {
 			
			if(".length".equals(da)) {
				return ((List<Object>)obj).size();
			}else if(".index".equals(da)) {
				return index;
			}
			//括号内
 			else if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				if("length".equals(bk.trim())) {
 					return ((List<Object>)obj).size();
 				}else if("index".equals(bk.trim())) {
 					return index+1;
 				}else if("string".equals(bk.trim())) {
 					return SrcJson.ListToStr((List<Object>)obj);
 				//下标
 				}else {
 	 				try{
 	 					int num=Integer.parseInt(bk.trim());
 	 					if(num>=0) {
 	 						Object crs=((List<Object>)obj).get(num);
 	 						return SrcJson.toString(crs);
 	 					}
 	 				}catch(Exception e){return "null";}
 				}
 			}else {
 				return ((List<Object>)obj);
 			}
 		}
 		//数组
 		else if(obj!=null && obj.getClass().isArray()) {
 			
 			if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				
 				if("length".equals(bk.trim())) {
 					return ((Object[])obj).length;
 				}else if("index" .equals(bk.trim())) {
 		 			return index+1;
 				}else if("string".equals(bk.trim())) {	
 					return SrcJson.ArrayToStr(obj).toString();
 				//下标
 				}else{
 	 				try{
 	 					int num=Integer.parseInt(bk.trim());
 	 					if(num>=0) {
 	 						return  ((Object[])obj)[num];
 	 					}
 	 				}catch(Exception e){return "null";}
 				}
 			}else if(da.indexOf(".")==0) {
 				String bk=da.substring(da.indexOf(".")+1);
 				if("length".equals(bk.trim())) {
 					return ((Object[])obj).length;
 				}else if("index" .equals(bk.trim())) {
 		 			return index+1;
 				}
 			}else {
 				sb.append(SrcJson.ArrayToStr(obj));
 			}
 		//基本数据类型
 		}else if(obj!=null && obj.getClass().isPrimitive() || obj instanceof String || obj instanceof Number|| obj instanceof Boolean  ){
 			if(da.indexOf("(")>=0) {
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 	 			if("string".equals(bk.trim())) {	
 					return obj;
 	 			}
 	 			else if("length".equals(bk.trim())) {	
 					return (obj+"").length();
 	 			}
 	 			else if("index".equals(bk)) {	
 					return index+1;
 	 			}
 			} else {
 				return obj;
 			}

 		}else if(obj instanceof Page) {
 			if(da.indexOf(".")>=0){
 				String tmp = da.substring(da.indexOf(".")+1);
 				Page page = (Page) obj;
 				if("html".equals(tmp.trim())) {
 					return page.getHtml();
 				}else if("pageNo".equals(tmp.trim())) {
 					return page.getPageNo();
 				}else if("pageNum".equals(tmp.trim())) {
 					return page.getPageNum();
 				}else if("pageCount".equals(tmp.trim())) {
 					int pc=page.getPageCount();
 					return pc;
 				}else if("rsCount".equals(tmp.trim())) {
 					return page.getRsCount();
 				}else if("dbType".equals(tmp.trim())) {
 					return page.getDbType();
 				}else if("url".equals(tmp.trim())) {
 					return page.getUrl();
 				}else if("showNum".equals(tmp.trim())) {
 					return page.getShowNum();
 				}else if("startTag".equals(tmp.trim())) {
 					return page.getStartTag();
 				}
 				Map<String,Object> map =SrcJson.JsonToMap(SrcJson.BeantoJson(page).toString());
 				return map.get(tmp.trim());
 			}
 			
 		//实体bean先转换
 		}else {
 			if(da.indexOf(".")>=0){
 				String tmp = da.substring(da.indexOf(".")+1);
 				Map<String,Object> map =SrcJson.JsonToMap(SrcJson.BeantoJson(obj).toString());
 				return map.get(tmp.trim());
 			}
 			else if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				if("string" .equals(bk.trim())) {
 	 				return SrcJson.toString(obj);
 	 			}
 				else if("index".equals(bk.trim())) {
 					return index+1;
 				}
 				else if(!"".equals(bk.trim())) {//是属性
 					Map<String,Object> map =SrcJson.JsonToMap(SrcJson.BeantoJson(obj).toString());
 					return map.get(bk.trim());
 				}
 			}else {
 				String objjs=SrcJson.BeantoJson(obj).toString();
 				//System.out.println("js="+objjs);
 				Map<String,Object> map =SrcJson.JsonToMap(objjs);
 				return map.get(da);
 				//return map;
 			}
 		}
 		return sb;
     }    
     
     /**
      * 特殊属性绑定
      * @param obj
      * @param da
      * @param sign
      * @param index
      * @return
      */
     public static Object getBindData2(Object obj,String da,String sign,int index){
     	da=da.trim();
     	StringBuffer sb=new StringBuffer();
     	
 		if(obj instanceof Map){
 			//如果没有下级
 			if(da.indexOf(".")<0 && da.indexOf("(")<0){
 				return ((Map)obj).get(da)==null?"":((Map)obj).get(da);
 			}
 			//有点
 			if(da.indexOf(".")>=0) {
 				String tmp=da.substring(da.indexOf(".")+1);
 				return ((Map)obj).get(tmp);
 			}
 			//括号中
 			if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				if("key".equals(bk.trim())){
 					int n=0;
					Set<String> keySet = ((Map)obj).keySet();
		     		for(Iterator<String> it = keySet.iterator();it.hasNext();) {
		     			String key=it.next();
		     			if(index==0) {
 			     			if(sb.length()>0) {
 			     				sb.append(","+key);
 			     			}else {
 			     				sb.append(key);
 			     			}
		     			}else {
		     				n++;
		     				if(n==index) {
 			     				sb.append(key);
 			     				return sb;
 			     			}
		     			}
		     		}
 					return sb;
 				}
 				else if("val".equals(bk)) {
 						Set<String> keySet = ((Map)obj).keySet();
 						int n=0;
 						for(Iterator<String> it = keySet.iterator();it.hasNext();) {
 			     			String key=it.next();
 			     			if(index==0) {
	 			     			if(sb.length()>0) {
	 			     				sb.append(","+((Map)obj).get(key));
	 			     			}else {
	 			     				sb.append(((Map)obj).get(key));
	 			     			}
 			     			}else {
 			     				n++;
 			     				if(n==index) {
 	 			     				sb.append(((Map)obj).get(key));
 	 			     				return sb;
 	 			     			}
 			     			}
 			     		}
 			     	return sb;
 				}
 				else if("length".equals(bk.trim())) {
 					return ((Map)obj).size();
 				}else if("string".equals(bk.trim())) {
 	 				return SrcJson.mapToJson((Map)obj).toString();
 				}else {
 					return ((Map)obj).get(bk.trim());
 				}
 			}
 			
 		}else if(obj instanceof List) {
 			
			if(".length".equals(da)) {
				return ((List<Object>)obj).size();
			}else if(".index".equals(da)) {
				return index;
			}
			//括号内
 			else if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				if("length".equals(bk.trim())) {
 					return ((List<Object>)obj).size();
 				}else if("index".equals(bk.trim())) {
 					return index+1;
 				}else if("string".equals(bk.trim())) {
 					return SrcJson.ListToStr((List<Object>)obj);
 				//下标
 				}else {
 	 				try{
 	 					int num=Integer.parseInt(bk.trim());
 	 					if(num>=0) {
 	 						Object crs=((List<Object>)obj).get(num);
 	 						return SrcJson.toString(crs);
 	 					}
 	 				}catch(Exception e){return "null";}
 				}
 			}else {
 				return ((List<Object>)obj);
 			}
 		}
 		//数组
 		else if(obj!=null && obj.getClass().isArray()) {
 			
 			if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				
 				if("length".equals(bk.trim())) {
 					return ((Object[])obj).length;
 				}else if("index" .equals(bk.trim())) {
 		 			return index+1;
 				}else if("string".equals(bk.trim())) {	
 					return SrcJson.ArrayToStr(obj).toString();
 				//下标
 				}else{
 	 				try{
 	 					int num=Integer.parseInt(bk.trim());
 	 					if(num>=0) {
 	 						return  ((Object[])obj)[num];
 	 					}
 	 				}catch(Exception e){return "null";}
 				}
 			}else if(da.indexOf(".")==0) {
 				String bk=da.substring(da.indexOf(".")+1);
 				if("length".equals(bk.trim())) {
 					return ((Object[])obj).length;
 				}else if("index" .equals(bk.trim())) {
 		 			return index+1;
 				}
 			}else {
 				sb.append(SrcJson.ArrayToStr(obj));
 			}
 		//基本数据类型
 		}else if(obj!=null && obj.getClass().isPrimitive() || obj instanceof String || obj instanceof Number|| obj instanceof Boolean  ){
 			if(da.indexOf("(")>=0) {
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 	 			if("string".equals(bk.trim())) {
 					return obj;
 	 			}
 	 			else if("length".equals(bk.trim())) {	
 					return (obj+"").length();
 	 			}
 	 			else if("index".equals(bk)) {	
 					return index+1;
 	 			}
 			} else {
 				return obj;
 			}

 		}else if(obj instanceof Page) {
 			if(da.indexOf(".")>=0){
 				String tmp = da.substring(da.indexOf(".")+1);
 				Page page = (Page) obj;
 				if("html".equals(tmp.trim())) {
 					return page.getHtml();
 				}else if("pageNo".equals(tmp.trim())) {
 					return page.getPageNo();
 				}else if("pageNum".equals(tmp.trim())) {
 					return page.getPageNum();
 				}else if("pageCount".equals(tmp.trim())) {
 					int pc=page.getPageCount();
 					return pc;
 				}else if("rsCount".equals(tmp.trim())) {
 					return page.getRsCount();
 				}else if("dbType".equals(tmp.trim())) {
 					return page.getDbType();
 				}else if("url".equals(tmp.trim())) {
 					return page.getUrl();
 				}else if("showNum".equals(tmp.trim())) {
 					return page.getShowNum();
 				}else if("startTag".equals(tmp.trim())) {
 					return page.getStartTag();
 				}
 				Map<String,Object> map =SrcJson.JsonToMap(SrcJson.BeantoJson(page).toString());
 				return map.get(tmp.trim());
 			}
 			
 		//实体bean先转换
 		}else {
 			if(da.indexOf(".")>=0){
 				String tmp = da.substring(da.indexOf(".")+1);
 				Map<String,Object> map =SrcJson.JsonToMap(SrcJson.BeantoJson(obj).toString());
 				return map.get(tmp.trim());
 			}
 			else if(da.indexOf("(")>=0 && da.indexOf(")")>0){
 				String ak=da.substring(0,da.indexOf("("));
 				String bk=da.substring(da.indexOf("(")+1,da.indexOf(")"));
 				if("string" .equals(bk.trim())) {
 	 				return SrcJson.toString(obj);
 	 			}
 				else if("index".equals(bk.trim())) {
 					return index+1;
 				}
 				else if(!"".equals(bk.trim())) {//是属性
 					Map<String,Object> map =SrcJson.JsonToMap(SrcJson.BeantoJson(obj).toString());
 					return map.get(bk.trim());
 				}
 			}else {
 				String objjs=SrcJson.BeantoJson(obj).toString();
 				//System.out.println("js="+objjs);
 				Map<String,Object> map =SrcJson.JsonToMap(objjs);
 				return map.get(da);
 				//return map;
 			}
 		}
 		return sb;
     }
     
     

     /**
      * 对参数格式化 可以是纯数字
      * type："###.##"
      * @param src
      * @param data
      * @param i
      * @return
      */
     public static String toSrcFormat(String strs,Object data,int i){
    	if(strs==null || "".equals(strs.trim())) {return"";}
 		Pattern p=Pattern.compile(SrcConfig.SRC_FMT);
 		
 		if(data==null||"null".equals(data)||"".equals(data) ){return "";}
 		Matcher m=p.matcher(strs);
 		String no="";
 		while(m.find()){
 			String bqt=m.group();
 			String bq=bqt.substring(0,bqt.indexOf(">")+1);
 			if(bq.indexOf(" keep ")>-1 || bq.indexOf(" keep>")>-1) {
 				String nbqt=bq.replace(" keep", "");
  				strs=strs.replace(bq, nbqt);
 				continue;
 			}
 			
 			String mtop=m.group(1).trim();
 			if(mtop.indexOf("\"")>0) {
 				mtop=mtop.substring(0,mtop.indexOf("\""));
 			}
 			String type=m.group(2).trim();

 			if("".equals(mtop)||"".equals(type)){
 				return strs;
 			}
 			//yyyy-MM-dd HH:mm
 			java.text.DateFormat  sf;

 			//if(o==null||"".equals(o)) {return "";}
 			//处理时间格式
 			if(type.indexOf("YY")>-1|| type.indexOf("MM")>-1||  type.indexOf("dd")>-1){
 				sf = new SimpleDateFormat(type);
 				if("now".equalsIgnoreCase(mtop.trim())){
 					sf= new SimpleDateFormat(type);
 					no=sf.format(new Date());
 				}else {
 					try {
 						//String os=getBindData( data, mtop,i)+"";
 						Date d= sf.parse(mtop);
 						no=sf.format(d);
 					} catch (ParseException e) {
 						e.printStackTrace();
 					}
 				}
 				//时间简写
 			}else if("***".equals(type)){
 				if(mtop.indexOf(" ")>1 && mtop.indexOf(":")>1){
 					sf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 				}else{
 					sf= new SimpleDateFormat("yyyy-MM-dd");
 				}
 				long st=0;
 				try {
	 				if("now".equalsIgnoreCase(mtop.trim())){
	 					sf= new SimpleDateFormat(type);
	 					no=sf.format(new Date());
	 				}else {
						Date d= sf.parse(mtop);
						st=d.getTime()/1000;
	 				}
 				} catch (ParseException e) {
 					e.printStackTrace();
 				}
 				no=DateUtil.convertTimeToFormat(st);
 				//数字简写
 			}else if("+++".equals(type)) {

 				Pattern pattern = Pattern.compile("-?[0-9]+\\.?[0-9]*");
 				Matcher isNum = pattern.matcher(mtop);
 				//非数字
 				if (!isNum.matches()) {
 					return "";
 				}else {
 					String dw="",tp="";
 					if(mtop.length()==4) {
 						dw="K+";
 						tp=mtop.substring(0,1);
 					}else if(mtop.length()==5) {
 						dw="W+";
 						tp=mtop.substring(0,1);
 					}else if(mtop.length()==6) {
 						dw="W+";
 						tp=mtop.substring(0,2);
 					}else  if(mtop.length()==7){
 						dw="W+";
 						tp=mtop.substring(0,3);
 					}else  if(mtop.length()==8){
 						dw="KW+";
 						tp=mtop.substring(0,1);
 					}else  if(mtop.length()==9){
 						dw="E+";
 						tp=mtop.substring(0,1);
 					}else  if(mtop.length()>9){
 						dw="..E+";
 						tp=mtop.substring(0,1);
 					}
 					else {
 						tp=mtop;
 					}
 					no=tp+dw;
 				}
 			}
 			//有小数点 按数字格式化
 			else if(type.indexOf("#")>=0){

 				Pattern pattern = Pattern.compile("-?[0-9]+\\.?[0-9]*");
 				Matcher isNum = pattern.matcher(mtop);
 				//非数字
 				if (!isNum.matches()) {
 					return "";
 				}else {
 					NumberFormat numberFormat = NumberFormat.getInstance();
 					DecimalFormat numberDecimalFormat;

 					numberDecimalFormat = (DecimalFormat) numberFormat;
 					//保留小数点后面三位，不足的补零,前面整数部分 每隔四位 ，用 “,” 符合隔开
 					numberDecimalFormat.applyPattern(type);
 					//设置舍入模式 为DOWN,否则默认的是HALF_EVEN
 					numberDecimalFormat.setRoundingMode(RoundingMode.DOWN);
 					//设置 要格式化的数 是正数的时候。前面加前缀
 					//numberDecimalFormat.setPositivePrefix("Prefix  ");
 					Double d= new Double(mtop);
 					no = numberDecimalFormat.format(d);
 					if(d==0) {
 						no="0"+no;
 					}
 					//Slog.println("number="+no);
 				}
 			}
 			strs=strs.replace(bqt, no);
 		}
 		return strs;
     }
     
     /**
      * 处理字典类属性值
      * @param strs
      * @param data
      * @param i
      * @return
      */
     public static String toSrcWhen(String strs,Object data,int i){
    	Pattern p=Pattern.compile(SrcConfig.SRC_WHEN);

  		Matcher m=p.matcher(strs);
  		String no="";
  		while(m.find()){
  			String bqt=m.group();
  			String bq=bqt.substring(0,bqt.indexOf(">")+1);
  			if(bq.indexOf(" keep ")>-1 || bq.indexOf(" keep>")>-1) {
  				String nbqt=bq.replace(" keep", "");
  				strs=strs.replace(bq, nbqt);
  				continue;
  			}
  			String name=m.group(1).trim();
  			String when=m.group(2).trim();
  			if("".equals(name)||"".equals(when)){
  				return strs;
  			}

 			String[] ws=when.split(";");
 			for(int is=0;is<ws.length;is++){
 				if(ws[is]!=null && !"".equals(ws[is].trim())){
 					String[] tws=ws[is].split(":");
 					if(name== tws[0].trim()|| tws[0].trim().equals(name)){
 						no=tws[1];break;
 					}
 				}
 			}
 			strs=strs.replace(bqt, no);
 		}
 		return strs;
     }
     
     public static String toSrcArray(String strs,Object data,int i){
     	Pattern p=Pattern.compile(SrcConfig.SRC_WHEN);

   		Matcher m=p.matcher(strs);
   		String no="";
   		while(m.find()){
   			String ts=m.group(0);
   			String name=m.group(1).trim();
   			String when=m.group(2).trim();
   			if("".equals(name)||"".equals(when)){
   				return strs;
   			}

  			String[] ws=when.split(";");
  			for(int is=0;is<ws.length;is++){
  				if(ws[is]!=null && !"".equals(ws[is].trim())){
  					String[] tws=ws[is].split(":");
  					if(name== tws[0].trim()|| tws[0].trim().equals(name)){
  						no=tws[1];break;
  					}
  				}
  			}
  			strs=strs.replace(ts, no);
  		}
  		return strs;
      }
     
 }