package com.srczh.manage;

import java.io.File;

import org.apache.catalina.Host;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Server;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.AprLifecycleListener;
import org.apache.catalina.startup.Tomcat;
import org.apache.coyote.http11.Http11NioProtocol;

/**
 * SrcServer
 * @author zhoujun
 */
public class SrcServer {
	
	 	private String hostname="localhost";
	   
	 	//设置端口,默认的端口，主要看配置属性
	    private int port=8889;
	    //
	    private String webappDir="ROOT";
	    //设置 连接时的一些参数
	    private int maxPostSize=-1;
	    private int maxThreads=200;
	    private int acceptCount=100;
	    private static String contextPath = "/";

	    public SrcServer(){}
	    
	    /**
	     * http启动
	     * @param local
	     * @param port
	     */
	    public void startHttp(String local,int port){
	        try{
	        	Tomcat tomcat = new Tomcat();
	        	tomcat.setPort(port);
	        	tomcat.setHostname(local);
	            tomcat.setBaseDir(".");
	            Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
	            connector.setPort(port);
	            tomcat.setConnector(connector);
	            tomcat.addWebapp("", System.getProperty("user.dir")+ File.separator+this.webappDir);
	            tomcat.enableNaming();
	            tomcat.start();
	            tomcat.getServer().await();
	        	
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
	    }
	    
	    /**
	     * https启动
	     * @param local
	     * @param port
	     */
	    public void startHttps(String local,int ports,String keyname,String keypath,String keypassword){
	    	
	    	try {
	    		SrcConfig.SSLKEY=keypath;
	    		SrcConfig.SSLPASSWD=keypassword;
	    		
		    	Tomcat tomcat = new Tomcat();
		        tomcat.setPort(this.port);
		        tomcat.setBaseDir(".");
		        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		        connector.setPort(ports);
		        Http11NioProtocol protocol = (Http11NioProtocol)connector.getProtocolHandler();
		        protocol.setKeyAlias(keyname);
		        protocol.setKeystoreFile(keypath);
		        protocol.setKeystorePass(keypassword);
		        protocol.setSSLEnabled(true);
		        tomcat.getService().addConnector(connector);
		        tomcat.getHost().setAutoDeploy(false);
	            tomcat.addWebapp("", System.getProperty("user.dir")+ File.separator+this.webappDir);
	            tomcat.enableNaming();
				tomcat.start();
				tomcat.getServer().await();
            } catch (LifecycleException e) {
            }

	    }
	    

	    private void configHost(Host host) {
	        host.setAppBase(System.getProperty("user.dir"));
	    }

	    private void configServer(Server server) {
	        AprLifecycleListener listener = new AprLifecycleListener();
	        server.addLifecycleListener(listener);
	    }

	    private void configConnector(Connector connector) {
	        connector.setURIEncoding("UTF-8");
	        connector.setMaxPostSize(this.maxPostSize);
	        connector.setAttribute("maxThreads", Integer.valueOf(this.maxThreads));
	        connector.setAttribute("acceptCount", Integer.valueOf(this.acceptCount));
	        connector.setAttribute("disableUploadTimeout", Boolean.valueOf(true));
	        connector.setAttribute("enableLookups", Boolean.valueOf(false));
	    }


	   public static SrcServer getService(){
	       return new SrcServer();
	   }

}
