package com.srczh.manage;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;

import com.srczh.manage.interna.Internation;
import com.srczh.manage.pool.SrcPoolBean;
import com.srczh.manage.pool.SrcPoolManage;
import com.srczh.manage.util.Slog;
import com.srczh.manage.util.SrcUtil;

/**
 * SrcListener ServletContextListener
 * @author zhoujun
 */

@WebListener
public class SrcListener implements ServletContextListener {
	
	

	public Hashtable<String,Object> htParam=new Hashtable<>();
	private Hashtable<String,Object> htShowMsg=new Hashtable<>();

	List<String> daoNames = new ArrayList<String>();
	List<String> actionNames = new ArrayList<String>();
	Map<String, Object> actionMap = new HashMap<>();
//	Map<String,Vector<Object>> dMap = new HashMap<String, Vector<Object>>();
	Map<String, Object> daoImplMap = new HashMap<>();
	

	public void contextDestroyed(ServletContextEvent arg0) {
	}

	//监听器
	public void contextInitialized(ServletContextEvent arg0) {

		setHashtable();
		ServletContext sc = arg0.getServletContext();
		String os=queryHashtable("os.name");				
		String osversion=queryHashtable("os.version");  
		String osmoudel=queryHashtable("sun.arch.data.model");
		String oslg=queryHashtable("user.language");
		String oscity=queryHashtable("user.country");
		String oszone= queryHashtable("user.timezone");
		String serverInfo=sc.getServerInfo(); 
		String runtime=queryHashtable("java.runtime.name");
		String runversion=queryHashtable("java.runtime.version");
		String javahome=queryHashtable("java.home");
		String serverpath=queryHashtable("user.dir")==null?"":queryHashtable("user.dir");
		String hhf=queryHashtable("line.separator");
		String wjf=System.getProperty("file.separator");
		

		String basePath= sc.getRealPath("/");
		//String srczh="SRCZH.COM";

		Map<String,Object> smap=new HashMap<>();
		smap.put("os", os);
		smap.put("osversion", osversion);
		smap.put("osmoudel", osmoudel);
		smap.put("oscity", oscity);
		smap.put("oszone", oszone);
		smap.put("serverInfo", serverInfo);
		smap.put("runtime", runtime);
		smap.put("runversion", runversion);
		smap.put("javahome", javahome);
		smap.put("serverpath", serverpath);
		smap.put("basePath", basePath);
		smap.put("hhf", hhf);
		smap.put("wjf", wjf);

		//src properties
		String config="/WEB-INF/classes/source/src.properties";
		String startMess="";
		String bp=basePath.replace("\\", "/");
		String[] p=bp.split("/");
		String pname=p[p.length-2].toLowerCase();
		String logpath=basePath+"\\log";
		String loglevel="all";
		String control="Action";
		String bao="com."+pname;
		String range="/src/*";
		String domain="localhost";
		String coding="UTF-8";
		String zip="false";
		String encryScript="false";
		String encryScript_file="";
		String sessiontimeout="600";
		String sessiontinvalid="";
		String srctask="";
		String srctasktimes="0,60*60"; //start times, end times
		String parameterShielding="";
		String sslkeys="";
		String sslpass="";
		String authorization="";

		Properties pam = new Properties();
		InputStream in=null;
		try {
			in=sc.getResourceAsStream(config);
			if(in!=null) {
				pam.load(in);
			}
			oslg=pam.getProperty("src.language")==null?"ZH":pam.getProperty("src.language");
			SrcConfig.SRC_LANGUAGE=oslg;
			smap.put("oslanguage", oslg);
			startMess=Internation.out(oslg,Internation.onconfig)+": "+config;
		} catch (IOException e2) {
			startMess=Internation.out(oslg,Internation.noconfig)+": "+config;
		}finally{
			try {
				if(in!=null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logpath=pam.getProperty("log.path")==null?logpath:pam.getProperty("log.path");
		loglevel=pam.getProperty("log.level")==null?loglevel:pam.getProperty("log.level");
		control=pam.getProperty("src.control")==null?control:pam.getProperty("src.control");
		bao=pam.getProperty("src.pakage")==null?bao:pam.getProperty("src.pakage");
		range=pam.getProperty("src.range")==null?range:pam.getProperty("src.range");
		domain=pam.getProperty("src.domain")==null?domain:pam.getProperty("src.domain");
		coding=pam.getProperty("src.coding")==null?coding:pam.getProperty("src.coding");
		zip=pam.getProperty("src.zip")==null?zip:pam.getProperty("src.zip");
		encryScript=pam.getProperty("src.encryScript")==null?encryScript:pam.getProperty("src.encryScript");
		encryScript_file=pam.getProperty("src.encryScript_file")==null?encryScript_file:pam.getProperty("src.encryScript_file");
		sessiontimeout=pam.getProperty("src.session.timeout")==null?sessiontimeout:pam.getProperty("src.session.timeout");
		sessiontinvalid=pam.getProperty("src.session.invalid")==null?sessiontinvalid:pam.getProperty("src.session.invalid");
		srctask=pam.getProperty("src.task")==null?srctask:pam.getProperty("src.task");
		srctasktimes=pam.getProperty("src.task.times")==null?srctasktimes:pam.getProperty("src.task.times");
		parameterShielding=pam.getProperty("src.session.invalid")==null?parameterShielding:pam.getProperty("src.session.invalid");
		
		sslkeys=pam.getProperty("src.ssl.keystoreFile")==null?sslkeys:pam.getProperty("src.ssl.keystoreFile");
		sslpass=pam.getProperty("src.ssl.keystorePass")==null?sslpass:pam.getProperty("src.ssl.keystorePass");
		authorization=pam.getProperty("src.authorization")==null?authorization:pam.getProperty("src.authorization");
		
		String pack=bao+"."+control.trim().toLowerCase();
		smap.put("logpath", logpath.trim());
		smap.put("loglevel", loglevel.trim());
		smap.put("range", range.trim());
		smap.put("domain", domain.trim());
		smap.put("package", pack.trim());
		smap.put("control", control.trim());
		smap.put("coding", coding.trim());
		smap.put("srczip", zip.trim());
		smap.put("encryScript", encryScript.trim());
		smap.put("encryScript_file", encryScript_file);
		smap.put("sessiontimeout", sessiontimeout);
		smap.put("sessiontinvalid", sessiontinvalid);
		smap.put("srctask", srctask);
		smap.put("srctasktimes", srctasktimes);
		smap.put("parameterShielding", parameterShielding);
		smap.put("sslkeys", sslkeys);
		smap.put("sslpass", sslpass);
		smap.put("v", SrcConfig.V);

		//src servlet
		String moniter=pam.getProperty("src.monitor")==null?"/src/monitor":pam.getProperty("src.monitor");
		String moniter_commd=pam.getProperty("src.monitor.password")==null?"123456":pam.getProperty("src.monitor.password");
		ServletRegistration sm = sc.addServlet("SrcMonitor", SrcMonitor.class); 
		sm.setInitParameter("domain", domain.trim()); 
		sm.setInitParameter("package", pack.trim()); 
		sm.setInitParameter("control", control.trim());
		sm.addMapping(moniter); 
		smap.put("moniter", moniter);
		smap.put("moniter_commd", moniter_commd);

		//src servlet
		ServletRegistration sr = sc.addServlet("SrcServlet", SrcServlet.class); 
		sr.setInitParameter("domain", domain.trim()); 
		sr.setInitParameter("package", pack.trim()); 
		sr.setInitParameter("control", control.trim());
		sr.addMapping(range.trim()); 

		//src Filter
		FilterRegistration sf = sc.addFilter("SrcFilter", SrcFilter.class);
		sf.setInitParameter("srcEncoding", coding.trim());
		sf.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST,DispatcherType.ASYNC), false, "/*");
		
		//src logs
		Slog.instance(smap);
		Slog.println(true,SrcConfig.SRCLOG);
        
        //srczh log language
        String at=initAuthorization(authorization);
        smap.put("authorization",at);
        Slog.info(at+" "+Internation.out(oslg, Internation.str));
        Slog.println(startMess);
        String deff=Internation.out(oslg, Internation.def);
        Slog.println(Internation.out(oslg, Internation.logAddr)+": "+(logpath.trim()==null?deff+serverpath:logpath.trim()));
        Slog.println(Internation.out(oslg, Internation.logLevel)+": "+(loglevel.trim()==null?deff:loglevel.trim()));
		
        Slog.println(Internation.out(oslg, Internation.fmemory)+": "+(Runtime.getRuntime().maxMemory()/1024/1024)+"M");
		Slog.println(Internation.out(oslg, Internation.system)+": "+os+" "+osversion+" "+osmoudel);
		Slog.println(Internation.out(oslg, Internation.region)+": "+oszone+" "+oslg+" "+oscity);
		Slog.println(Internation.out(oslg, Internation.server)+": "+serverInfo);
		Slog.println(Internation.out(oslg, Internation.language)+": "+runtime);
		Slog.println(Internation.out(oslg, Internation.environment)+": "+javahome+" "+runversion);
		Slog.println(Internation.out(oslg, Internation.coding)+": "+coding);
		Slog.println(Internation.out(oslg, Internation.AbsolutePath)+": "+basePath);
		Slog.println(Internation.out(oslg, Internation.ControllerAddress)+": "+pack);
		Slog.println(Internation.out(oslg, Internation.Controller)+": "+control);
		Slog.println(Internation.out(oslg, Internation.range)+": "+range);
		Slog.println(Internation.out(oslg, Internation.visit)+": "+domain);
		Slog.println("session "+Internation.out(oslg, Internation.keep)+": "+sessiontimeout);
		//src Dao
		URL url = this.getClass().getClassLoader().getResource(wjf);
		String urls=url.toString();
		urls=urls+SrcIoc.replaceTo(bao);
		if(os.toLowerCase().indexOf("windows")>-1){
			urls=urls.replace("file:/", "");
		}else{
			urls=urls.replace("file:", "");
		}
		try {
			urls=URLDecoder.decode(urls,"utf-8");
			SrcIoc.scanAction(actionNames,control,urls+wjf+control.toLowerCase(),bao);
			smap.put("actions", actionNames);
			SrcIoc.scanDao(daoNames, urls+wjf+"dao",bao);
			SrcIoc.daoInstance(daoImplMap, daoNames);
			smap.put("daoImpl", daoImplMap);
			
			Iterator<?> it = daoImplMap.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String,Object> entry = (Entry<String,Object>) it.next();
				Object key = entry.getKey();
				Slog.println(Internation.out(oslg, Internation.zhuru)+" "+key);
			}
		}catch (Exception e1) {
			e1.printStackTrace();
			return ;
		}
		
		//src zip encryScript
		Slog.println(Internation.out(oslg, Internation.zip)+":"+zip);
		Slog.println(Internation.out(oslg, Internation.jsen)+":"+encryScript);
		
		//JDBC POOL
		String pool="", srcsql="",preloading="";
		pool=pam.getProperty("src.pool")==null?"":pam.getProperty("src.pool");
		if(!"".equals(pool)) {
			Slog.println(Internation.out(oslg, Internation.srcjdbcpool)+" "+pool+" ..");
		}
		
		smap.put("pool", pool);
		String connects="10";
		String maxConns="200";
		String minConns="5";
		String inAddConns="5";
		String waitNum="1000";
		String timeout="500";
		pool="srcPool";
		// srcpool
		if("srcPool".equalsIgnoreCase(pool)){
			connects=pam.getProperty("src.connectionSize")==null?connects:pam.getProperty("src.connectionSize");
			maxConns=pam.getProperty("src.maxConnections")==null?maxConns:pam.getProperty("src.maxConnections");
			minConns=pam.getProperty("src.minConnections")==null?minConns:pam.getProperty("src.minConnections");
			inAddConns=pam.getProperty("src.inConnectionSize")==null?inAddConns:pam.getProperty("src.inConnectionSize");
			waitNum=pam.getProperty("src.waitNum")==null?waitNum:pam.getProperty("src.waitNum");
			timeout=pam.getProperty("src.timeout")==null?timeout:pam.getProperty("src.timeout");
		}

		// druid
		else if("druid".equalsIgnoreCase(pool)){
			//公共方法不提供
		}
		//tomcatJdbc
		else if("tomcatJdbc".equalsIgnoreCase(pool)){
			//公共方法不提供
		}

		List<SrcPoolBean> dbls=new ArrayList<SrcPoolBean>();
		String[] drls=pam.getProperty("jdbc.drives")==null?new String[] {}:pam.getProperty("jdbc.drives").toString().split(",");
		//公共版本不支持 多个数据源
		if(drls.length>0 ) {
			for(int i=0;i<1;i++){
				String jdbcDrive=pam.getProperty("jdbc."+drls[i]+".drive")==null?"":pam.getProperty("jdbc."+drls[i]+".drive");
				String dbUrl=pam.getProperty("jdbc."+drls[i]+".url")==null?"":pam.getProperty("jdbc."+drls[i]+".url");
				String dbUname=pam.getProperty("jdbc."+drls[i]+".username")==null?"":pam.getProperty("jdbc."+drls[i]+".username");
				String dbupwd=pam.getProperty("jdbc."+drls[i]+".password")==null?"":pam.getProperty("jdbc."+drls[i]+".password");
				String testSql=pam.getProperty("jdbc."+drls[i]+".test")==null?"":pam.getProperty("jdbc."+drls[i]+".test");
				
				SrcPoolBean spo=new SrcPoolBean();
				spo.setPool(pool);
				spo.setPoolName(drls[i]);
				spo.setDriver(jdbcDrive);
				spo.setDbUrl(dbUrl);
				spo.setDbUname(dbUname);
				spo.setDbPasswd(dbupwd);
				spo.setTestSql(testSql);
				if("srcPool".equals(pool)) {
					spo.setInitConns(Integer.parseInt(connects));
					spo.setMaxConns(Integer.parseInt(maxConns));
					spo.setMinConns(Integer.parseInt(minConns));
					spo.setInAddConns(Integer.parseInt(inAddConns));
					spo.setRconnMiss(Integer.parseInt(waitNum));
					spo.setTimeout(Integer.parseInt(timeout));
				}
				dbls.add(spo);
			}
		}else {
			//单个数据源初始化
			String drone=pam.getProperty("jdbc.drive")==null?"":pam.getProperty("jdbc.drive")+"";
			if(drone!=null&&!"".equals(drone)) {
				String jdbcDrive=pam.getProperty("jdbc.drive")+"".trim()+""==null?"":pam.getProperty("jdbc.drive")+"".trim();
				String dbUrl=pam.getProperty("jdbc.url")+"".trim()+""==null?"":pam.getProperty("jdbc.url")+"".trim();
				String dbUname=pam.getProperty("jdbc.username")+"".trim()==null?"":pam.getProperty("jdbc.username")+"".trim();
				String dbUpwd=pam.getProperty("jdbc.password")+"".trim()==null?"":pam.getProperty("jdbc.password")+"".trim();
				String testSql=pam.getProperty("jdbc.test")+"".trim()==null?"":pam.getProperty("jdbc.test")+"".trim();
				SrcPoolBean spo=new SrcPoolBean();
				spo.setPool(pool);
				spo.setPoolName(SrcConfig.SRCDFPOOL);
				spo.setDriver(jdbcDrive);
				spo.setDbUrl(dbUrl);
				spo.setDbUname(dbUname);
				spo.setDbPasswd(dbUpwd);
				spo.setTestSql(testSql);
				spo.setInitConns(Integer.parseInt(connects));
				spo.setMaxConns(Integer.parseInt(maxConns));
				spo.setMinConns(Integer.parseInt(minConns));
				spo.setInAddConns(Integer.parseInt(inAddConns));
				spo.setRconnMiss(Integer.parseInt(waitNum));
				spo.setTimeout(Integer.parseInt(timeout));
				dbls.add(spo);
			}
		}
		

		smap.put("srcpoolList", dbls);
		
		if(dbls.size()<=0){
			String mess=Internation.out(oslg, Internation.dbpoolwarning);
			Slog.error(mess);
		}else{
			//实例化数据源
			SrcPoolManage spools = SrcPoolManage.getInstance(dbls);
			//test db
			for(int i=0;i<dbls.size();i++){
				SrcPoolBean db=dbls.get(i);
				String poolName=db.getPoolName();
				
				try {
					Connection conn = spools.getConnection(poolName);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				int[] sta=spools.getConnState(poolName);
				//连接数 空闲数
				Slog.println(poolName+": activity="+sta[1]+" free="+sta[0]);
				if(!"".equals(db.getTestSql())) {
					Slog.println(db.getTestSql()+" = "+spools.testConn(poolName, db.getTestSql()));
				}
			}

			//YSQL
			srcsql=pam.getProperty("jdbc.sql")==null?"":pam.getProperty("jdbc.sql");
			preloading=pam.getProperty("jdbc.sql.preloading")==null?"true":pam.getProperty("jdbc.sql.preloading");
			smap.put("SrcSQLPreLoding", preloading);
			if(!"".equals(srcsql)) {
				String xmlpath=basePath+"WEB-INF"+wjf+"classes"+ wjf+ srcsql;
				SrcDbXml dbxml=new SrcDbXml(xmlpath);
				Map<String,Object> fileMap=dbxml.getSqlFelds();
				String loding=Internation.out(oslg, Internation.loding);
//				Slog.println("SrcSQL"+loding+" "+xmlpath);
				fileMap.remove("prj_fnum");
				Slog.println(fileMap);
				Map<String,Object> sqlMap=dbxml.getSqls(fileMap);
				smap.put("SrcSQL", sqlMap);
			}
		}
		
		//src task
		if(srctask!=""&&!"".equals(srctask)) {
			//公共版不提供该方法
		}
		
		float fTotalMemory=(float)Runtime.getRuntime().totalMemory(); //已使用內存
		float fFreeMemory=(float)Runtime.getRuntime().freeMemory();
		String cmemory=Internation.out(oslg, Internation.cmemory);
		
		//src monitor
		Slog.println("srczh monitor: "+moniter);
		Slog.println(cmemory+":"+(fFreeMemory/1024/1024)+"M");
		SrcConfig.initConfig(smap);
	}

	public void setHashtable(){
		Properties me=System.getProperties();
		Enumeration em=me.propertyNames();
		while(em.hasMoreElements()){
			String strKey=(String)em.nextElement();
			String strValue=me.getProperty(strKey);
			htParam.put(strKey,strValue);
		}
	}
	public void getHashtable(String strQuery){
		Enumeration em=htParam.keys();
		while(em.hasMoreElements()){
			String strKey=(String)em.nextElement();
			String strValue=new String();
			if(strKey.indexOf(strQuery,0)>=0){
				strValue=(String)htParam.get(strKey);
				htShowMsg.put(strKey,strValue);
			}
		}
	}
	public String queryHashtable(String strKey){
		strKey=htParam.get(strKey)==null?"":htParam.get(strKey)+"";
		return strKey;
	}

	public String initAuthorization(String auth) {
		//if(auth==null || "".equals(auth)) {
			return " 公共版 ";
		//}
		//return auth;
	}

}
