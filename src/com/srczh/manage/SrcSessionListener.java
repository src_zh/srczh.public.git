package com.srczh.manage;


import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.srczh.manage.util.Slog;

public class SrcSessionListener  implements HttpSessionListener {  

	private SrcSession myc = SrcSession.getInstance();  

	//新建session
	public void sessionCreated(HttpSessionEvent httpSessionEvent) {  
		HttpSession session = httpSessionEvent.getSession();  
		session.setMaxInactiveInterval(SrcConfig.SESSION_TIME_OUT);
		myc.addSession(session);  
	}

	@Override
	//销毁session
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {  
		HttpSession session = httpSessionEvent.getSession();  
		String ac= SrcConfig.SESSION_INVALID;

		if(!"".equals(ac)) {
			Map<String, Object> weblogin = (Map<String, Object>) session.getAttribute("weblogin");
			if(weblogin!=null && weblogin.size()>0) {
				invalidSession(ac,weblogin);
			}
			Map<String, Object> adminlogin = (Map<String, Object>) session.getAttribute("adminlogin");
			if(adminlogin!=null && adminlogin.size()>0) {
				invalidSession(ac,adminlogin);
			}
			Map<String, Object> applogin = (Map<String, Object>) session.getAttribute("applogin");
			if(applogin!=null && applogin.size()>0) {
				invalidSession(ac,applogin);
			}
		}
		myc.delSession(session);  
		Slog.println("del session="+session.getId());
	}

	public void invalidSession(String ac,Map<String,Object> sessionMap) {
		String dao=ac.substring(0,ac.lastIndexOf("."));
		String fun=ac.substring(ac.lastIndexOf(".")+1);
		Class<?> src;
		try {
			src = Class.forName(dao);
			Constructor<?> srccon = src.getConstructor(new Class<?>[] {});
			Object osrc = srccon.newInstance(new Object[] {});
			Method srcmds = src.getDeclaredMethod(fun, new Class[]{Map.class});
			srcmds.invoke(osrc, new Object[]{sessionMap}); 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
