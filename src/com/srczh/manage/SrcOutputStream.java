package com.srczh.manage;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

public class SrcOutputStream extends ServletOutputStream{
private ServletOutputStream outputStream;
	
	public SrcOutputStream(ServletOutputStream outputStream) {
		this.outputStream = outputStream;
	}
 
	@Override
	public void write(int b) throws IOException {
		outputStream.write(b);
	}
 
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		super.write(b, off, len);
	}
 
	@Override
	public void write(byte[] b) throws IOException {
		super.write(b);
	}

	@Override
	public boolean isReady() {
		// TODO 自动生成的方法存根
		return false;
	}

	@Override
	public void setWriteListener(WriteListener listener) {
		// TODO 自动生成的方法存根
		
	}

}
