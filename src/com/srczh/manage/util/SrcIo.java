package com.srczh.manage.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;


import com.srczh.manage.SrcConfig;



@SuppressWarnings("all")
public class SrcIo {

	 String path="";

	private static RandomAccessFile aFile = null;  
	private FileChannel inChannel = null;  
//	private final ByteBuffer buf = ByteBuffer.allocate(1024);  
	
	//单例饿汉式:线程安全，调用效率高，但是不能延时加载
    private static class SrcIoInstance{
        private static final SrcIo instance=new SrcIo();
    }
     

    public static SrcIo getInstance(){
        return SrcIoInstance.instance;
    }
	public SrcIo(){

	}
	
	/**获取文件目录列表实例化**/
	public SrcIo(String path){
		this.path=path;
	}
	

	/**文件拷贝,读取,写入实例化,原路径,新路径**/
	public SrcIo(String apath,String path) throws FileNotFoundException{
		this.path=path;
	}

	/**
	 *  获取项目文件夹目录列表或文件列表
	 * @param fileDir
	 * @return
	 */
	public List<Object> getFelds(String fileDir) {  
		//图片扩展名
		String[] fileTypes = new String[]{"gif", "jpg", "jpeg", "png", "bmp"};
		File currentPathFile = new File(fileDir);  
		List<Object> fileList = new ArrayList<>();
		if(currentPathFile.listFiles() != null) {
			for (File file : currentPathFile.listFiles()) {
				Hashtable<String, Object> hash = new Hashtable<String, Object>();
				String fileName = file.getName();
				if(file.isDirectory()) {
					hash.put("is_dir", true);
					hash.put("has_file", (file.listFiles() != null));
					hash.put("filesize", 0L);
					hash.put("is_photo", false);
					hash.put("filetype", "");
				} else if(file.isFile()){
					String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
					hash.put("is_dir", false);
					hash.put("has_file", false);
					hash.put("filesize", file.length());
					hash.put("is_photo", Arrays.<String>asList(fileTypes).contains(fileExt));
					hash.put("filetype", fileExt);
				}
				hash.put("filename", fileName);
				hash.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file.lastModified()));
				fileList.add(hash);
			}
		}
		
//		List<String> fileList = new ArrayList<String>();  
//		File file = new File(fileDir);  
//		File[] files = file.listFiles();
//		for (File f : files) {
//			if(f.isFile()){
//				fileList.add(f.getName());
//			}
//		}
		return fileList;
	}
	
	/**获取项目文件属性列表
	 * @param fileDir 总路径
	 * @param root    根目录
	 * @param type   类型
	 * @param lv    获取文件夹层级
	 * @return
	 */
	public Map<String,Object> getFolds(String fileDir,String root,String type,int lv) {  
		List<FileBean> fileList = new ArrayList<FileBean>();
		Map<String,Object> lmap = new HashMap<>();
		File file = new File(fileDir);
		fileDir=fileDir.replace("/", SrcConfig.SRC_WJF);
		if(!file.isDirectory()) {
			//file.mkdirs();
			return new HashMap<>();
		}
		File[] files = file.listFiles();// 获取目录下的所有文件或文件夹  
		// 遍历，目录下的所有文件  
		//type==all  记录文件以及文件夹
		//type==fold 记录文件夹
		//type==file 只记录文件
//		int nums=files.length;
//		lmap.put("prj_fnum", nums);
		for(File f : files) {
			FileBean fb = new FileBean();
			String ps=f.getAbsolutePath();
			String roop=path.substring(0,path.lastIndexOf("/")+1);
			roop=roop.replace("/", "\\");
			roop=roop.replace("\\\\", "\\");
			String pps=ps.replace(roop, "");
			pps=pps.replace("\\", "/");
			//Slog.println(ps+"---"+pps);
			fb.setFpath(pps);
			if("all".equalsIgnoreCase(type)) {
				fb.setFname(f.getName());

				fb.setFtime(getFileTime(f));
				if(f.isFile()){
					fb.setFtype("file");
					fb.setFsize(initFSize(f.length()));
				}
				//是文件夹
				else if(f.isDirectory()) {
					fb.setFtype("fold");
					if(lv==0) {
						String sfp=path.replace("/", SrcConfig.SRC_WJF);
						String pfiles=f.getPath().replace(sfp+SrcConfig.SRC_WJF, "");
						pfiles=pfiles.replace(SrcConfig.SRC_WJF, "/");
						getFolds(f.getAbsolutePath(),pfiles,type,lv);
					}
				}
				fileList.add(fb);

				if("\\".equals(root.trim())||"/".equals(root.trim())){
					lmap.put("//",fileList);
				}
			}
			else if("file".equalsIgnoreCase(type)) {
				if(f.isDirectory()) {
					if(lv==0) {
						String sfp=path.replace("/", SrcConfig.SRC_WJF);
						String pfiles=f.getAbsolutePath().replace(sfp+SrcConfig.SRC_WJF, "");
						pfiles=pfiles.replace(SrcConfig.SRC_WJF, "/");
						getFolds(f.getAbsolutePath(),pfiles,type,lv);
					}
				}else if(f.isFile()){
					fb.setFname(f.getName());
					fb.setFpath(f.getPath().replace(fileDir, ""));
					fb.setFtime(getFileTime(f));
					fb.setFtype("file");
					fb.setFsize(initFSize(f.length()));
					fileList.add(fb);
				}
				//if(fileList.size()>0){
				if("\\".equals(root.trim())||"/".equals(root.trim())){
					lmap.put("//",fileList);
				}
			}
			else if("fold".equalsIgnoreCase(type)) {
				if(f.isDirectory()) {
					fb.setFname(f.getName());
					fb.setFpath(f.getPath().replace(fileDir, ""));
					fb.setFtime(getFileTime(f));
					fb.setFtype("file");
					fb.setFsize(initFSize(f.length()));
					fileList.add(fb);
					if(lv==0) {
						String sfp=path.replace("/", SrcConfig.SRC_WJF);
						String pfiles=f.getPath().replace(sfp+SrcConfig.SRC_WJF, "");
						pfiles=pfiles.replace(SrcConfig.SRC_WJF, "/");
						getFolds(f.getAbsolutePath(),pfiles,type,lv);
					}
				}
			}
		}
		if(!"\\".equals(root.trim())&&!"/".equals(root.trim())){
			lmap.put(root,fileList);
		}
		return lmap;
	}
	
	/**
	   *  获取项目文件名称列表树
	 * @param fileDir
	 * @param root
	 * @param type
	 * @return
	 */
	public Map<String,Object> getFelds(String root,String type) {  
		return getFelds(path,root,type);
	}
	public Map<String,Object> getFelds(String fileDir,String root,String type) {  
		List<String> fileList = new ArrayList<String>();  
		Map<String,Object> lmap = new HashMap<>();
		File file = new File(fileDir);  
		if(!file.isDirectory()) {
			file.mkdirs();
		}
		File[] files = file.listFiles();// 获取目录下的所有文件或文件夹  
		// 遍历，目录下的所有文件  
		//type==all  记录文件以及文件夹
		//type==fold 记录文件夹
		//type==file 只记录文件
		int nums=files.length;
		lmap.put("prj_fnum", nums);
		for(File f : files) {
			//System.out.println("===="+f.getPath());
			if("all".equalsIgnoreCase(type)) {
				if(f.isFile()){
					fileList.add(f.getName());
				}
				//是文件夹
				if(f.isDirectory()) {
					String sfp=path.replace("/", SrcConfig.SRC_WJF);
					String pfiles=f.getPath().replace(sfp+SrcConfig.SRC_WJF, "");
					pfiles=pfiles.replace(SrcConfig.SRC_WJF, "/");
					getFelds(f.getAbsolutePath(),pfiles,type);
				}
			//	if(fileList.size()>0){
				if("\\".equals(root.trim())||"/".equals(root.trim())){
					lmap.put("//",fileList);
				}
			}
			else if("file".equalsIgnoreCase(type)) {
				if(f.isDirectory()) {
					String sfp=path.replace("/", SrcConfig.SRC_WJF);
					//String sfp=path.replace("\\", "/");
					String pfiles=f.getAbsolutePath().replace(sfp+SrcConfig.SRC_WJF, "");
					pfiles=pfiles.replace(SrcConfig.SRC_WJF, "/");
					getFelds(f.getAbsolutePath(),pfiles,type);
				}else if(f.isFile()){
					fileList.add(f.getName());
				}
				//if(fileList.size()>0){
				if("\\".equals(root.trim())||"/".equals(root.trim())){
					lmap.put("//",fileList);
				}
			}
			else if("fold".equalsIgnoreCase(type)) {
				if(f.isDirectory()) {
					String sfp=path.replace("/", SrcConfig.SRC_WJF);
					String pfiles=f.getPath().replace(sfp+SrcConfig.SRC_WJF, "");
					pfiles=pfiles.replace(SrcConfig.SRC_WJF, "/");
					getFelds(f.getAbsolutePath(),pfiles,type);
				}
			}
		}
		if(!"\\".equals(root.trim())&&!"/".equals(root.trim())){
			lmap.put(root,fileList);
		}
		return lmap;
	}
	
	/**
	 * 查找文件内容
	 * @param ftxt 查询文本
	 * @param ftype 文件类型
	 * @param inCase 大小写
	 * @return
	 */
	public List<Object> findFile(String ftxt,String ftype,Boolean inCase){
		List<Object> rsList = new ArrayList<>();
		List<String> pathlist = new ArrayList<String>();
		getFileList(path,ftype,pathlist);
		String cpath=path.replace("/", "\\");
		for(int k=0;k<pathlist.size();k++) {
			File file = new File(pathlist.get(k));
			if (file.exists()) {
				String s = file.toString();
				
				int count=0;
//				System.out.println("正在读取第文件:" + s);
				/* 读取数据 */
				try {
					BufferedReader br = new BufferedReader( new InputStreamReader(new FileInputStream(new File(s)), "UTF-8"));
					String lineTxt = null;
					String ite="0";
					Map<String,Object> limap = new HashMap<>();
					List<Object> ht=new ArrayList<>();
					while ((lineTxt = br.readLine()) != null) {
						count++;
						//区别大小写
						if (inCase) {
							if (lineTxt.contains(ftxt)) {
								ite="1";
								ht.add(count);
							//	rsList.add("在【" + s + "】文件中找到了" + lineTxt + "在第" + count + "行");
							}
						} else {
							if (lineTxt.toLowerCase().contains(ftxt.toLowerCase())) {
								ite="1";
								ht.add(count);
							}
						}
						limap.put("lino", ht);
					}
					if(ite=="1"){
						limap.put("lipath", s.replace(cpath, "").replace("\\", "/"));
						rsList.add(limap);
					}
					br.close();
				} catch (Exception e) {
					System.err.println("error: " + e);
				}
			}
		}
		return rsList;
	}
	
	
	public boolean saveFile(String filepath,String filetxt){
		Boolean bool = false;
		String temp = "";
        BufferedReader br = null;
        FileOutputStream fos = null;
        PrintWriter pw = null;
		 try {
	            File file = new File(filepath);//文件路径(包括文件名称)
	            fos = new FileOutputStream(file);
	            pw = new PrintWriter(fos);
	            pw.write(filetxt.toCharArray());
	            pw.flush();
	            bool = true;
	        } catch (Exception e) {
	            e.printStackTrace();
	            bool= false;
	        }finally {
	            //不要忘记关闭
	            try {
	                if (pw != null) {
	                    pw.close();
	                }
	                if (fos != null) {
	                    fos.close();
	                }
	                if (br != null) {
	                    br.close();
	                }
	              
	            } catch(IOException ex) {
	                ex.printStackTrace();
	            }
	        }
		 return bool;
	}
	

	

	//文件类型可能情况
	//正式文件名一个或多个, *.文件类型一个或多个, *或*.*
	public void getFileList(String strPath,String ftype,List<String> pathlist ) {
		File dir = new File(strPath);
		File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				String fileName = files[i].getName();
				if (files[i].isDirectory()) { // 判断是文件还是文件夹
					getFileList(files[i].getAbsolutePath(),ftype,pathlist); // 获取文件绝对路径
				} else if(files[i].isFile()) { // 判断文件名是否以.avi结尾
					if("*".equals(ftype) || "*.*".equals(ftype)){
						String strFileName = files[i].getAbsolutePath();
						pathlist.add(strFileName);
						
					}else if(ftype.indexOf("*.")>0){
						
					}else if(fileName.endsWith("."+ftype)){
						String strFileName = files[i].getAbsolutePath();
						pathlist.add(strFileName);
					}
				} else {
					continue;
				}
			}
		}
	}
	
	public String getFileTime(File f) {
		Calendar cal = Calendar.getInstance();  
        long time = f.lastModified();  
        cal.setTimeInMillis(time);    
        //此处toLocalString()方法是不推荐的，但是仍可输出  
        return cal.getTime().toLocaleString();   
	}
	
	
	/**判断是否有文件存在**/
	public static boolean isExists(String path) {
	    if ((new File(path)).exists()) {
            return false;
        }
	    return true;
	}

	/**拷贝文件夹
	 * @throws IOException */
	public static void copyDir(String souPath, String newPath) throws IOException  {
		File file = new File(souPath);
        
		if(file.isFile()) {
				copyFile(souPath ,  newPath);
		}else if (file.isDirectory()){
			String[] filePath = file.list();
            if (!(new File(newPath)).exists()) {
                (new File(newPath)).mkdirs();
            }
	        for (int i = 0; i < filePath.length; i++) {
	            if ((new File(souPath + file.separator + filePath[i])).isDirectory()) {
	                copyDir(souPath  + file.separator  + filePath[i], newPath+file.separator + filePath[i]);
	            }
	            if (new File(souPath  + file.separator + filePath[i]).isFile()) {
	                copyFile(souPath + file.separator + filePath[i],  newPath+file.separator + filePath[i]);
	            }
	        }
		}
	}

	/**拷贝文件*/
	public void doCopy() throws IOException {  
		inChannel = aFile.getChannel();  
		RandomAccessFile bFile = new RandomAccessFile(path, "rw");  
		FileChannel outChannel = bFile.getChannel();  
		inChannel.transferTo(0, inChannel.size(), outChannel);  
		// System.out.println("Copy over");  
	}  
	public static void doCopy(String olfile,String nfile) throws IOException {
		RandomAccessFile aFile = new RandomAccessFile(olfile, "rw");  
		FileChannel inChannel = aFile.getChannel();  
		RandomAccessFile bFile = new RandomAccessFile(nfile, "rw");  
		FileChannel outChannel = bFile.getChannel();  
		inChannel.transferTo(0, inChannel.size(), outChannel);  
		// System.out.println("Copy over");  
	}
	/**黏贴文件*/
	public static void copyFile(String oldPath,String newPath) throws IOException{
		//复制文件
		File source=new File(oldPath);
		File dast=new File(newPath);
		if(!dast.exists()) {
			FileChannel inputChannel = null;
			FileChannel outputChannel = null;
			try {
				inputChannel = new FileInputStream(source).getChannel();
				outputChannel = new FileOutputStream(dast).getChannel();
				outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
			} finally {
				if(inputChannel!=null) {
					inputChannel.close();
				}
				if(outputChannel!=null) {
					outputChannel.close();
				}
			}
		}
	}
	/**读取文件*/
	public static StringBuffer doReadFile(String path) throws Exception {  
		StringBuffer sb=new StringBuffer();
		FileInputStream fis = new FileInputStream(new File(path));  
		FileChannel channel = fis.getChannel(); 
		Charset charset = Charset.forName("UTF-8");  
		CharsetDecoder decoder = charset.newDecoder();  
		ByteBuffer buffer = ByteBuffer.allocate(128);  
		CharBuffer charBuffer = CharBuffer.allocate(128);  
		int i = channel.read(buffer);  
		while(i != -1){ 
			buffer.flip(); // 切换到读模式  
			decoder.decode(buffer, charBuffer, false);  
			charBuffer.flip(); // 切换到读模式  
			while(charBuffer.hasRemaining()){  
				char c = charBuffer.get();  
				sb.append(c );
			}  
			charBuffer.clear();  
			buffer.clear();  
			i = channel.read(buffer);  
		}
		channel.close();  
		fis.close(); 
		return sb;
	}
	
	/**
	 * https获取请求内容
	 * @param urls
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public static String doReadHttps(String urls,String sid) {  
		StringBuffer sb=new StringBuffer();
		BufferedReader in =null;
		InputStreamReader isr=null;
		 HttpsURLConnection httpConn =null;
		try {
			SSLContext sslcontext = SSLContext.getInstance("SSL","SunJSSE");  
	        sslcontext.init(null, new TrustManager[]{new MyX509TrustManager()}, new java.security.SecureRandom());  
	        URL serverUrl = new URL(urls);  
	        httpConn = (HttpsURLConnection) serverUrl.openConnection();  
	        httpConn.setSSLSocketFactory(sslcontext.getSocketFactory());  
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			httpConn.addRequestProperty("Authorization", "Bearer srczh.com" );
			httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConn.setRequestProperty("Cookie", "JSESSIONID="+sid);
			//重要超时时间
			httpConn.setConnectTimeout(5000);
			httpConn.setReadTimeout(5000);
	        //必须设置false，否则会自动redirect到重定向后的地址  
			httpConn.setInstanceFollowRedirects(false);  
			httpConn.connect();	
			
			int code=httpConn.getResponseCode();
			if (code==200) {
				isr = new InputStreamReader(httpConn.getInputStream(),SrcConfig.SRC_CODING);
				in = new BufferedReader(isr);
				String line;
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				in.close(); 
			}else {
				//httpConn.
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//httpConn.disconnect();
			if(isr!=null) {
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(in!=null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		}
		return sb.toString();
	}
	
	
	/**
	 * http获取请求内容
	 * @param urls
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public static String doRead(String urls,String sid) {  
		StringBuffer sb=new StringBuffer();
		BufferedReader in =null;
		InputStreamReader isr=null;
		HttpURLConnection httpConn =null;
		try {
			URL httpurl = new URL(urls);
			httpConn = (HttpURLConnection) httpurl.openConnection();
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			httpConn.addRequestProperty("Authorization", "Bearer srczh.com" );
			httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConn.setRequestProperty("Cookie", "JSESSIONID="+sid);
			//重要超时时间
			httpConn.setConnectTimeout(5000);
			httpConn.setReadTimeout(5000);
			int code=httpConn.getResponseCode();
			if (code==200) {
				isr = new InputStreamReader(httpConn.getInputStream(),SrcConfig.SRC_CODING);
				in = new BufferedReader(isr);
				String line;
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				isr.close();
				in.close(); 
			}else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//httpConn.disconnect();
			if(isr!=null) {
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(in!=null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		}
		return sb.toString();
	}
	

	
	public static boolean delAllFile(String path) {  
	       boolean flag = false;  
	       File file = new File(path);  
	       if (!file.exists()) {  
	         return flag;  
	       }  
	       if(file.isFile()) {
	    	   return file.delete();
	       }
	       if (!file.isDirectory()) {  
	         return flag;  
	       }
	       String[] tempList = file.list();  
	       File temp = null;  
	       for (int i = 0; i < tempList.length; i++) { 
	          if (path.endsWith(File.separator)) {  
	             temp = new File(path + tempList[i]);  
	          } else {  
	              temp = new File(path + File.separator + tempList[i]);  
	          }  
	          if (temp.isFile()) {  
	             temp.delete();  
	          }  
	          if (temp.isDirectory()) {  
	             delAllFile(path + "/" + tempList[i]);//先删除文件夹里面的文件  
	             delFolder(path + "/" + tempList[i]);//再删除空文件夹  
	             flag = true;  
	          }  
	       }  
	       return flag;  
	 }  
	
	//删除文件夹  
	public static boolean delFolder(String folderPath) {  
		File f = new File(folderPath);
		if(f.isDirectory()) {
			 delAllFile(folderPath); //删除完里面所有内容  
		}
		return  f.delete();
	}
	
	/**修改文件名*/
	public static boolean editFileName(String lpath,String npath){
		File file=new File(lpath);
		if(file.exists()){
			return file.renameTo(new File(npath));  
		}
		return false;
	}

	
	/**写入文件*/
	public void doWrite(String path,String newData) throws IOException {  
		   aFile = new RandomAccessFile(path, "rw");  
		ByteBuffer buf = ByteBuffer.allocate(newData.length());  
		inChannel = aFile.getChannel();  
		//String newData = "New String to wirte to file... " + System.currentTimeMillis();  
		buf.clear();  
		buf.put(newData.getBytes());  
		buf.flip();  
//		while (buf.remaining() > 0) {
//			buf.put((byte)0);
//		}
		while (buf.hasRemaining()) {
			inChannel.write(buf);  
		}
		inChannel.close();  
		//System.out.println("Write Over");  
	}

	/**
	 * 将文件一行行读取到map，key，val按item分隔
	 * @param path
	 * @param item
	 * @return
	 */
	public Map<String,String> getFiletoMap(String path,String item){
		if(path==null||"".equals(path.trim())) {
			return new HashMap<>();
		}
		if(item==null || "".equals(item)) {
			return new HashMap<>();
		}
		File file = new File(path);
		Map pmap = new HashMap<>();
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));//构造一个BufferedReader类来读取文件

            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
            	if(s!=null && !"".equals(s.trim())) {
                	if(s.indexOf(item)>0) {
                		//String ss[] = s.split(item);
                		String k=s.substring(0,s.indexOf(item));
                		String v = s.substring(s.indexOf(item)+1);
                		pmap.put(k, v.trim());
                	}
            	}
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return pmap;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String path="";
		SrcIo f=new SrcIo(path);
		Map<String,Object> lmap=f.getFelds("/","all");
	//	String js=JSON.toJSONString(lmap);
	//	System.out.println(js);
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	public String zhuanyitext(String txt){
		txt=txt.trim();
		txt=txt.replaceAll(";", "");
		txt=txt.replaceAll("\"", "\\\"");
		txt=txt.replaceAll(",", "\\,");
		return txt;
	}
	
	
	public String initFSize(Long l) {
		if(l==0) {
			return "0KB";
		}
		Long n=l/1024;
		if(n>10240000) {
			return (n/1024)+"MB";
		}else if(n<1){
			return "1KB";
		}else {
			return n+"KB";
		}
	}
	
	
}
