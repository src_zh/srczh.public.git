package com.srczh.manage.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Map;


/**
 * Slog 日志控制类
 * @author zhoujun
 */
public class Slog extends Thread{

	private static Slog log = null;
	public static String logtxt="";
 
    static String logpath;
    static String logfile;
    static String loglevel;
    static String wjf;
    static String hhf;

    private Slog() {
    	super();
    }
    
    private Slog( Map<String,Object> map) {
        createFile(map);
    }
 
    protected static boolean exists(String pathFileName) {
        File mFile = new File(pathFileName);
        return mFile.exists();
    }
    
 
    /**
     * 实例化创建日志文件夹及文件名
     * @param map
     */
    protected static void createFile(Map<String,Object> map) {
    	wjf =  map.get("wjf").toString();
    	hhf =  map.get("hhf").toString();
    	loglevel=map.get("loglevel")+"";
    	logpath=initLogPath(map);
        try {
            File dirFile = new File(logpath);
            if(!dirFile.exists()){
                dirFile.mkdirs();
            }
            logfile= SrcUtil.getDateTime("yyyy-MM-dd")+".log";
            File logFile = new File(dirFile, logfile);
            if(!logFile.exists()){
                logFile.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 修改日志文件，按每日或手动修改
     */
    protected static void changeLogFname(){
    	logfile= SrcUtil.getDateTime("yyyy-MM-dd")+".log";
    }
    
    /**
     * 详细记录输出程序内容
     * @param info
     */
    public static void info(String info) {
    	String log=SrcUtil.getDateTime("yyyy-MM-dd HH:mm:ss") + ": " + info;
    	println(log,true,0);
    }
    public static void info(String info,boolean bs) {
    	String log=SrcUtil.getDateTime("yyyy-MM-dd HH:mm:ss") + ": " + info;
    	println(log,bs,0);
    }
   
    /**
     * 输出异常
     * @param log
     */
    public static void error(String log){
    	log=SrcUtil.getDateTime("yyyy-MM-dd HH:mm:ss") + ": " + log;
    	if("all".equalsIgnoreCase(loglevel) || "devel".equalsIgnoreCase(loglevel) ) {
    		System.err.println(log);
    	}
    	if( "all".equalsIgnoreCase(loglevel) || "prodt".equalsIgnoreCase(loglevel)) {
			writeLogFile(log);
		}
    }
    

    /**
              * 打印输出字符
     * @param txt
     */ 
    public static void println(Object log){
    	println(log+"", false,0);
    }
    /**
     * 带限制长度打印
     * @param log
     * @param size
     */
    public static void println(Object log,int size){
    	println(log+"", false,0);
    }
    /**
     * 指定是否记录存档打印
     * @param log
     * @param bs
     */
    public static void println(Object log,boolean bs){
    	println(log+"", bs,0);
    }
      
    /**
     * 打印打印字符，并且是否临时输出到log日志文件，不受log主参数影响
     * @param log
     * @param bs
     * loglevel:
     * 	#all:打印所有，并记录到日志（包含系统异常）
		#devel：打印所有，不记录指定字符到日志（日志包含系统异常）。
		#prodt：不打印指定输出，及记录日志文件（日志包含异常）
		#stop：不打印所有，只记录系统异常日志
     */
    public static void println(String log,boolean bs,int size){

    	if("all".equalsIgnoreCase(loglevel) || "devel".equalsIgnoreCase(loglevel) ) {
    		//int size=Integer.parseInt(SrcConfig.LOGOPTIMIZE);
    		if(size>0 && log.length()>size) {
    			log = log.substring(0,size)+"...";
    		}
    		System.out.println(log);
    	}
    	if(bs||"all".equalsIgnoreCase(loglevel) || "prodt".equalsIgnoreCase(loglevel)) {
        	writeLogFile(log);
		}
    }
    //不受日志优化参数限制
    public static void println(boolean bs,String log){
    	if(bs) {
    		System.out.println(log);
    	}
    }
    /**
     * 向文件中写入内容
     * @param logs 写入的内容
     * @return
     * @throws IOException
     */
    private static void writeLogFile(String logs){
    	logtxt=logs;
    	new Thread() {												
    	  	public void run() {											
    	         String filein = Slog.logtxt+hhf;//新写入的行，换行
    	         String temp = "";
    	         FileInputStream fis = null;
    	         InputStreamReader isr = null;
    	         BufferedReader br = null;
    	         FileOutputStream fos = null;
    	         PrintWriter pw = null;
    	         try {
    	             File file = new File(logpath+wjf+logfile);//文件路径(包括文件名称)
    	             //将文件读入输入流
    	             fis = new FileInputStream(file);
    	             isr = new InputStreamReader(fis);
    	             br = new BufferedReader(isr);
    	             StringBuffer buffer = new StringBuffer();
    	             //文件原有内容
    	             for(int i=0;(temp =br.readLine())!=null;i++){
    	                 buffer.append(temp);
    	                 // 行与行之间的分隔符 相当于“\n”
    	                 buffer = buffer.append(hhf);
    	             }
    	             buffer.append(filein);
    	             fos = new FileOutputStream(file);
    	             pw = new PrintWriter(fos);
    	             pw.write(buffer.toString().toCharArray());
    	             pw.flush();
    	         } catch (Exception e) {
    	        	 e.printStackTrace();
    	         }finally {
    	             try {
    	                 if (pw != null) {
    	                     pw.close();
    	                 }
    	                 if (fos != null) {
    	                     fos.close();
    	                 }
    	                 if (br != null) {
    	                     br.close();
    	                 }
    	                 if (isr != null) {
    	                     isr.close();
    	                 }
    	                 if (fis != null) {
    	                     fis.close();
    	                 }
    	             } catch(IOException ex) {
    	            	 ex.printStackTrace();
    	             }
    	         }
    	  	}
    	  }.start();
    }
 
    /**
     * 单例启动日志类实例
     * @return log
     */
    public static synchronized Slog instance(Map<String,Object> map) {
        if (log == null) {
        	log = new Slog(map);
        }
        return log;
    }
 
    /*
     * 初始化日志文件路径
     * @return log
     */
    private static String initLogPath(Map<String,Object> map){
		String path="";
		if(!"".equals(map.get("logpath"))){
			return map.get("logpath")+wjf+"src-logs";
		}
		path=map.get("serverpath")+wjf+"src-logs";
		if(path==""){
			path=map.get("javahome")+wjf+"src-logs";
		}
		return path;
	}
}