package com.srczh.manage.util;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * SrcJson 输出JSON工具类
 * @author zhoujun
 */
@SuppressWarnings("unchecked")
public class SrcJson{

	private Map<String, Object> json = new HashMap<>();
	static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");


	public void clear() {
		json.clear();
	}
	/**
	 * 添加元素 <br/>
	 * @param key
	 * @param value
	 */
	public Map<String, Object> put(String key, Object value) {
		json.put(key, value);
		return json;
	}
	
	public Object get(String key) {
		return json.get(key);
	}

	/**
	 *  判断是否数字
	 */
	private static boolean isNoQuote(Object value) {
		return (value instanceof Integer || value instanceof Boolean ||value instanceof BigInteger || value instanceof BigDecimal
				|| value instanceof Double || value instanceof Float
				|| value instanceof Short || value instanceof Long || value instanceof Byte);
	}
	/**
	 *  判断是否字符，增加引号
	 */
	private static boolean isQuote(Object value) {
		return (value instanceof String || value instanceof Character);
	}


	/**
	 * 转换字符串
	 * 返回形如{'apple':'red','lemon':'yellow'}的字符串
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
	//	Set<Entry<String, Object>> set = jsonmap.entrySet();
		//for (Entry<String, Object> entry : set) {
		Iterator<Map.Entry<String, Object>> iterator = json.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Object> entry = iterator.next();
			Object value = entry.getValue();
			sb.append("").append("\""+entry.getKey()+"\"").append(":");
			if (value == null) {
				sb.append("\"null\"");
				//continue;// 对于null值，不进行处理，页面上的js取不到值时也是null
			}
			else if (value instanceof SrcJson) {
				sb.append(value.toString());
			} else if (isNoQuote(value)) {
				sb.append(""+value+"");
			} else if (value instanceof Date) {
				sb.append("\"").append(df.format(value)).append("\"");
			} else if (isQuote(value)) {
				sb.append("\"").append(value).append("\"");
			} else if (value.getClass().isArray()) {
				sb.append(ArrayToStr(value));
			} else if (value instanceof Map) {
				//sb.append(objectToJson((Map<String, Object>) value).toString());
				//sb.append(value.toString());
				SrcJson jmap=BeantoJson((Map)value);
				sb.append(jmap.toString());
			} else if (value instanceof List) {
				sb.append(ListToStr((List) value));
			} else {
				sb.append(BeantoJson(value).toString());
			}
			sb.append(",");
		}
		int len = sb.length();
		if (len > 1) {
			sb.delete(len - 1, len);
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static String toString(Object obj) {
		if(obj instanceof Map) {
			return mapToJson((Map<String,Object>)obj).toString();
		}
		else if(obj instanceof List) {
			return ListToStr((List)obj);
		}
		else if(obj instanceof String|| obj instanceof Integer ||obj instanceof Double || obj instanceof Float || obj instanceof Number) {
			return obj+"";
		}
		else if(obj instanceof Boolean) {
			return obj+"";
		}
		else if(obj instanceof String) {
			return obj+"";
		}
		else if(obj.getClass().isArray()) {
			return ArrayToStr(obj);
		}		
		else if(obj.getClass().isPrimitive()) {
			return obj.toString();
		}else {
			return BeantoJson(obj).toString();
		}
		
	}
	

	/**
	 * 转换数组到json字符串
	 * @param array
	 * @return
	 */
	public static String ArrayToStr(Object array) {
		if (!array.getClass().isArray()) {
			return "[]";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		int len = Array.getLength(array);
		Object v = null;
		for (int i = 0; i < len; i++) {
			v = Array.get(array, i);
			if (v instanceof Date) {
				sb.append("\"").append(df.format(v)).append("\"").append(",");
			} else if (isQuote(v)) {
				sb.append("\"").append(v).append("\"").append(",");
			} else if (isNoQuote(v)) {
				//sb.append(v).append(",");
				sb.append("").append(v).append("").append(",");
			} else {
				sb.append(BeantoJson(v)).append(",");
			}
		}
		len = sb.length();
		if (len > 1)
			sb.delete(len - 1, len);
		sb.append("]");
		return sb.toString();
	}


	/**
	 * 转换List到json字符串
	 * @param list
	 * @return
	 */
	public static String ListToStr(List<Object> list) {
		if (list == null)
			return null;
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		Object value = null;
		for (java.util.Iterator<Object> it = list.iterator(); it.hasNext();) {
			value = it.next();
			if (value instanceof Map) {
				sb.append(BeantoJson((Map) value).toString()).append(",");
			} else if (isNoQuote(value)) {
				//sb.append(value).append(",");
				sb.append("").append(value).append("").append(",");
			} else if (isQuote(value)) {
				sb.append("\"").append(value).append("\"").append(",");
			} else {
				sb.append(BeantoJson(value).toString()).append(",");
			}
		}
		int len = sb.length();
		if (len > 1)
			sb.delete(len - 1, len);
		sb.append("]");
		return sb.toString();
	}


	/**
	 * 转换一个任意对象到一个Json对象。
	 * @param object
	 * @return
	 */
	public static SrcJson BeantoJson(Object bean) {
		SrcJson json = new SrcJson();
		if (bean == null){
			return json;
		}
		if(bean instanceof Map) {
			return mapToJson((Map)bean);
		}
		
		Class<?> cls = bean.getClass();
		Field[] fs = cls.getDeclaredFields();
		Object value = null;
		String fieldName = null;
		Method method = null;
		int len = fs.length;
		for (int i = 0; i < len; i++) {
			fieldName = fs[i].getName();
			try {
				Object ov=getGetter(fieldName);
				method = cls.getMethod(ov+"", (Class[]) null);
				value = method.invoke(bean, (Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			json.put(fieldName, value);
		}
		return json;
	}

	
	/**
	 * 转换一个ConcurrentHashMap对象到json对象 <br/>
	 * @param ConcurrentHashMap
	 * @return
	 */
	public static SrcJson mapToJson(Map<String,Object> map) {
		SrcJson json = new SrcJson();
		if (map == null) {
			return json;
		}
		json.getMap().putAll(map);
		return json;
	}

	/**
	 * 从json字符转到ConcurrentHashMap
	 * @param property
	 * @return
	 */
	public static Map<String,Object> JsonToMap(String string){
		
		if(string==null||string==""){
			return null;
		}
		String st=string.substring(0,1);
		String et=string.substring(string.length()-1);
		if(!"{".equals(st) || !"}".equals(et)){
			return null;
		}
		SrcJson json = new SrcJson();
		//Pattern pattern = Pattern.compile("\"([a-zA-Z0-9\\_\\u4e00-\\u9fa5]{0,})\":\"{1}([a-zA-z0-9\\,\\'\\·\\`\\~\\!\\.\\@\\#\\%\\$\\^\\&\\*\\(\\)\\+\\=\\-\\s\\/\\?\\_\\！\\￥\\%\\…\\（\\）\\——\\+\\|\\{\\}\\【\\】\\‘\\；\\：\\”\\“\\’\\。\\，\\、\\？\\%2D\\:\\u4e00-\\u9fa5\"]{0,})\"{1}[\\,\\}]{1}|" +
        //"\"([a-zA-Z0-9\\_\\u4e00-\\u9fa5]{0,})\":\"{1}([a-zA-z0-9\\,\\'\\·\\`\\~\\!\\.\\@\\#\\%\\$\\^\\&\\*\\(\\)\\+\\=\\-\\s\\/\\?\\_\\！\\￥\\%\\…\\（\\）\\——\\+\\|\\{\\}\\【\\】\\‘\\；\\：\\”\\“\\’\\。\\，\\、\\？\\%2D\\:\\u4e00-\\u9fa5\"]{0,})\"{1}[\\,\\}]{1}"
       //        );
		//Pattern patterns = Pattern.compile("\"([a-zA-z0-9]{0,})\":\"{1}([a-zA-z0-9\\,\\-\\s\\/\\?\\_\\%2D\\:\\u4e00-\\u9fa5\"]{0,})\"{1}[\\,\\}]{1}|\"([a-zA-z0-9]{0,})\":([a-zA-z0-9\\,\\-\\s\\/\\?\\_\\%2D\\:\\u4e00-\\u9fa5\"]{0,})[\\,\\}]{1}");
		String p = "\"([^\"]*)\":([^}]*),?" ;
		Pattern pattern = Pattern.compile(p);
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()){
            if(matcher.group(1)!=null&& matcher.group(1)!="null") {
            	json.put(matcher.group(1), matcher.group(2));
            }else if(matcher.group(3)!=null&& matcher.group(3)!="null"){
            	json.put(matcher.group(3), matcher.group(4));
            }
        }
		return json.json;
		
	}


	private static String getGetter(String property) {
		String proty="get" + property.substring(0, 1).toUpperCase()+ property.substring(1, property.length());
		return proty;
	}
	public Map<String, Object> getMap() {
		return this.json;
	}
	
	
	
	public static void main(String[] asd){
//		User us = new User();
//		us.setUname("zjun");
//		us.setId("10");
//		us.setTel("13312@,.345:67");
//		us.setEmail("gfds&*(%[:]【】gfv,ds");
//		us.setTs(false);
////		us.setUser_img("/to_img.jpg");
//		us.setLength(11);
//		us.setDs(12.3);
//		us.setAddr(new String[] {"中国","广东","深圳"});
//		us.setShencai(new double[] {180.0,60.1,39.2,62.1});
		
//		List ls=new ArrayList();
//		Map<String,Object> map= new ConcurrentHashMap<>();
//		map.put("a", "ad@srczh.com");
//	//	map.put("b", "b");
//	//	map.put("c", "c");
//	//	map.put("d", "d");
//		map.put("e", 1);
//		ls.add(map);
//		Map<String,Object> map2= new ConcurrentHashMap<>();
//		ConcurrentHashMap2.put("a2", "zhoujun@srczh.com");
//		ConcurrentHashMap2.put("b2", "b");
//		ConcurrentHashMap2.put("c2", "c");
//		ConcurrentHashMap2.put("d2", "d");
//		ConcurrentHashMap2.put("e2", 2);
//		ls.add(ConcurrentHashMap2);
//		
//		String[] a=new String[]{"a3","a4"};
//		ConcurrentHashMap a3= new ConcurrentHashMap<>();
//		a3.put("ConcurrentHashMap3", a);
//		ls.add(a3);
		
//		SrcJson sj=new SrcJson();
	//	String js=sj.BeantoJson(us).toString();
	//	System.out.println("js=="+js);
	//	ConcurrentHashMap jmap=sj.JsonToConcurrentHashMap(js);
	//	System.out.println("jmap="+jmap);
		
		//String m2=sj.ListToStr(ls);
		
		//System.out.println(m2);
	}
		
}
