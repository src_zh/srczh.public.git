package com.srczh.manage.util;

import java.util.regex.Matcher;

public class SrcLabel {
	public int no;
	int st;
	int ed;
	Matcher m;
	String group; //内容
	String type;  //标签类型
	

	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public int getSt() {
		return st;
	}
	public void setSt(int st) {
		this.st = st;
	}
	public int getEd() {
		return ed;
	}
	public void setEd(int ed) {
		this.ed = ed;
	}
	public Matcher getM() {
		return m;
	}
	public void setM(Matcher m) {
		this.m = m;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
