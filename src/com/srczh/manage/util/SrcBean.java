package com.srczh.manage.util;

public class SrcBean {
	
	private String bname;
	private String pname;
	private Object pval;
	private int no;
	
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public Object getPval() {
		return pval;
	}
	public void setPval(Object pval) {
		this.pval = pval;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}

	
}
