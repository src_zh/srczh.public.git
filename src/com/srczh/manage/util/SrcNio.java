package com.srczh.manage.util;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import com.srczh.manage.SrcConfig;


public class SrcNio{

	/*
	public static String readFile(final String path, final Charset encoding) throws IOException {
		final byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	*/
	/**
	 * Nio读取文件
	 * @param filename
	 * @return 
	 */
    public static String nioRead(String filename) {
    	Slog.println("Nio:"+filename);
        FileInputStream fin = null;
        StringBuffer sb=new StringBuffer();
        try {
        	Charset charset = Charset.forName("UTF-8");
			CharsetDecoder decoder = charset.newDecoder();
			File sf=new File(filename);
			if(sf==null || !sf.isFile()) {
				return "";
			}
			fin = new FileInputStream(sf);
			FileChannel fileChannel = fin.getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(10240);
			CharBuffer charBuffer = CharBuffer.allocate(10240);
			int byteread = fileChannel.read(byteBuffer);

            while( -1 != byteread) {
            	byteBuffer.flip();
				while (byteBuffer.hasRemaining()) {
					decoder.decode(byteBuffer, charBuffer, false);
					charBuffer.flip();
				}
				// 字符缓冲区位置，用于确定字节数组大小
				int byteSize = byteBuffer.position();
				// 确定出字节数组大小
				byte[] byteLine = new byte[byteSize];
				// position=0 准备读取数据
				byteBuffer.rewind();
				// 从position开始读取byteLine.length个字节
				byteBuffer.get(byteLine);
				// 如果是 剔除 /r 前的 /n 后转换为字符串
				//String line = new String(byteLine, 0, OS.isWindows() ? byteLine.length - 1 : byteLine.length);
				String line = new String(byteLine, 0, byteLine.length);
				sb.append(line);
				byteBuffer.clear();
				charBuffer.clear();
				byteread = fileChannel.read(byteBuffer);
            }
            fileChannel.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb+"";
    }
    
	/**
	 * Nio写入文件
	 * @param file 地址
	 * @param fname 文件名
	 * @param chars 内容
	 */
    public static void nioWrite(String file,String fname,String chars) {
    	
    	FileOutputStream fos = null;
    	FileChannel channel = null;
    	try {
    		File f = new File(file) ;
    		if(!f.exists()) {
    			f.mkdirs();
    		}
    		fos = new FileOutputStream(file+SrcConfig.SRC_WJF+fname);
    		// 1. 获取通道
    		channel = fos.getChannel();
    		// 3. 指定缓冲区
    		byte[] array = chars.getBytes();
    		ByteBuffer buffer = ByteBuffer.wrap(array);
    		// 4. 读取到缓冲区
    		channel.write(buffer);
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		// 7. 关闭
    		try {
    			channel.close();
    			fos.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    }

    /**
     * 实现拷贝复制文件
     * @param copypath  源路径
     * @param newfile  新路径
     */
    public static void readAndWriteNIO(String copypath,String newfile) {
        FileInputStream fin = null;
        FileOutputStream fos = null;
        File sf = new File(copypath);
        //是文件夹
        if(sf.isDirectory()) {
        	File nsf = new File(newfile);
        	if(!nsf.exists()) {
        		nsf.mkdirs();
        	}
        	
        	File [] flist = sf.listFiles();
        	for(File f:flist) {
        		String fp = f.getPath();
        		//String nps=fp.replace(src, "");
        		String fname=f.getName();
        		readAndWriteNIO(f.getPath(),newfile+SrcConfig.SRC_WJF+fname);
        	}
        	
        }else {
	        try {
	        	//读文件通道
	            fin = new FileInputStream(sf);
	            FileChannel inchannel = fin.getChannel();
	
	            int capacity = 1024;// 字节
	            ByteBuffer bf = ByteBuffer.allocate(capacity);
	        //  System.out.println("限制是：" + bf.limit() + "容量是：" + bf.capacity() + "位置是：" + bf.position());
	          
	            //写文件通道
	            fos = new FileOutputStream(new File(newfile));
	            FileChannel outchannel = fos.getChannel();
	
	            while(inchannel.read(bf)!=-1){
	                //将当前位置置为limit，然后设置当前位置为0，也就是从0到limit这块，都写入到同道中
	                bf.flip();
	                while(bf.hasRemaining()){
	                	outchannel.write(bf);
	                }
	                bf.clear();
	            }
	            outchannel.close();
	            inchannel.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (fin != null) {
	                try {
	                    fin.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	            if (fos != null) {
	                try {
	                    fos.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
        }
    }
    
    
    public static void main(String[] ds) {
    	String filep="D:\\srcproject\\templater\\srczh";
    	String car="{CDATE=2021-05-08 16:21:04.0, P_LANGE=java, P_STATE=0, P_IMG=/image/java.png, P_PATH=D:/srcproject";
    	nioWrite(filep,"2.txt",car);
    }
    
}
