package com.srczh.manage.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * SrcCache 全局缓存类
 * @author zhoujun
 */
public class SrcCache {
	
	//单例饿汉式:线程安全，调用效率高，但是不能延时加载
    private static class SrcCacheInstance{
        private static final SrcCache instance=new SrcCache();
    }
     
    private SrcCache(){}
     
    public static SrcCache getInstance(){
        return SrcCacheInstance.instance;
    }
     
	
	private final static  Map<String,Object> cache = new HashMap<>();
	
	
	public void clear() {
		cache.clear();
	}


	public static void put(String key, Map<String,Object> map) {
		cache.put(key, map);
	}
	
	public static Map<String,Object> get(String key) {
		return (Map<String, Object>) cache.get(key);
	}
	
	public static void remove(String key){
	      Iterator<?> entries = cache.entrySet().iterator();
	      Map.Entry<?,?> entry;
	      String name = "";
	      String value = "";
	      while (entries.hasNext()) {
	          entry = (Map.Entry<?,?>) entries.next();
	          name = (String) entry.getKey();
	          if(key.equalsIgnoreCase(name)){
	        	  Slog.println("	Clear Cache: "+name);
	        	  entries.remove(); 
	          }
	      }
	      
	  }
	
}
