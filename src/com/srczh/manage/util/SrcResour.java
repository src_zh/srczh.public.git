package com.srczh.manage.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 加载配置文件类
 * @author zhoujun
 *
 */
public class SrcResour {

	public static Map<String,String> load(String path){
		
		Map<String,String> rs=new HashMap<>();
		try {								
			//JDK1.8以后可以省略第二个参数，默认是UTF-8编码
			List<String> lines = Files.readAllLines(Paths.get(path));
			for (String line : lines) {
				String[] lin=line.split(":");
				String key=lin[0].trim();
				String val=lin[1].trim();
				if(!"".equals(key)){
					rs.put(key, val);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	
	public static void write(String path,Map<String,Object> map) throws Exception{
		StringBuffer sb = new StringBuffer();
		
		FileOutputStream outputStream=new FileOutputStream(new File(path));
		FileChannel fileChannel=outputStream.getChannel();
		CharBuffer charBuffer=CharBuffer.allocate(1024);
		
		Iterator<?> entries = map.entrySet().iterator();
		Map.Entry<?,?> entry;
		String name = "";
		String value = "";
		while (entries.hasNext()) {
			entry = (Map.Entry<?,?>) entries.next();
			name = (String) entry.getKey();
			Object valueObj = entry.getValue();
			if(null == valueObj){
				value = "";
			}else if(valueObj instanceof String[]){
				String[] values = (String[])valueObj;
				for(int i=0;i<values.length;i++){
					value += values[i].trim() + ",";
				}
				value = value.substring(0, value.length()-1);
			}else{
				value = valueObj.toString().trim();
			}
			sb.append(name+"="+value+"\r\n");
		}
		if(sb.length()>0){
			charBuffer.put(sb.toString());
			charBuffer.flip();
			Charset charset=Charset.defaultCharset();
			ByteBuffer byteBuffer=charset.encode(charBuffer);

			//不能确定channel.write()能一次性写入buffer的所有数据
			//所以通过判断是否有余留循环写入
			while(byteBuffer.hasRemaining()){
			    fileChannel.write(byteBuffer);
			}

			fileChannel.close();
			outputStream.close();
		}

	}
}
