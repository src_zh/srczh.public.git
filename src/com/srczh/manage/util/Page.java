package com.srczh.manage.util;

import java.util.Map;

/**
 * Page 分页工具类
 * @author zhoujun
 */
public class Page {

	public  int pageNo=1;    //page当前页  默认1
	public  int pageNum=10;  //page页显示数 默认10
	public  int pageCount;   //page总页数 
	public  int rsCount;   //数据总行数
	
	public String dbType ="oracle";  //数据库类型
	public String url;   //地址
	public String html;  //pageHtml
	public String more;  //page更多数据html
	public int pageType=1;
	
	public  int showNum = 3;   // 分页标签的个数
	public  int startTag = 1;

	
	public Page(){
//		this.pageNo=SrcConfig.PAGENO;
//		this.pageNum=SrcConfig.PAGENUM;
//		this.showNum=SrcConfig.SHOWNUM;
	}

	/**
	 * @param pageNum 每页显示数
	 */
	public Page(int pageNum){
		this.pageNum=pageNum;
	}
	
	/**
	 * @param pageNo 查询起始页号
	 * @param pageNum 查询页数
	 */
	public Page(int pageNo,int pageNum){
		this.pageNo=pageNo;
		this.pageNum=pageNum;
	}
	
	
	/**
	 * @param dbType 数据库类型
	 * @param pageNo 查询起始页号
	 * @param pageNum 查询页数
	 */
	public Page(String dbType,int pageNo,int pageNum){
		this.dbType=dbType;
		this.pageNo=pageNo;
		this.pageNum=pageNum;
	}
	

	
	/**
	 * 得到当前页
	 * 处理分页从0开始的情况
	 * */
	public int getCPageNo() {
		if (pageNo <= 0)
			pageNo = 1;
		if (pageNo > getPageCount())
			pageNo = getPageCount();
		return pageNo;
	}
	
	
	/**
	 * 从系统配置类型文件获取数据库类型
	 * 实现分页查询SQL
	 * */
//	public String initPageSql(String sql){
//		StringBuffer pageSql=new StringBuffer();
//		
//		if(SrcConfig.JDBC_DRIVER.indexOf("oracle")>-1){
//			pageSql.append("select * from (select tmp_tb.*,ROWNUM row_id from (");
//			pageSql.append(sql);
//			pageSql.append(") tmp_tb where ROWNUM<=");
//			pageSql.append(this.pageNo * this.pageNum);
//			pageSql.append(") where row_id>");
//			int stPageNum=(this.pageNo-1)<0?0:(this.pageNo-1);
//			pageSql.append(stPageNum*this.pageNum);
//			
//		}else if(SrcConfig.JDBC_DRIVER.indexOf("mysql")>-1){
//			pageSql.append(sql);
//			int stPageNum=(this.pageNo-1)<0?0:(this.pageNo-1);
//			pageSql.append(" limit " + stPageNum + "," + this.getPageNum());
//		}
//		return pageSql.toString();
//	}
	
	/**
	 * 使用外置jdbc查询时，按指定参数区分数据库类型
	 * 实现分页查询SQL
	 * */
	public  String initPageSql(String dbType,String sql){
		if(!"".equals(sql)){
			//是否有条数限制	
			StringBuffer pageSql = new StringBuffer();
			if(this.pageNo>0){
				if(("oracle").equals(dbType)){
					pageSql.append("select * from (select tmp_tb.*,ROWNUM row_id from (");
					pageSql.append(sql);
					pageSql.append(") tmp_tb where ROWNUM<=");
					pageSql.append(this.pageNo * this.pageNum);
					pageSql.append(") where row_id>");
					int stPageNum=(this.pageNo-1)<0?0:(this.pageNo-1);
					pageSql.append(stPageNum*this.pageNum);
				}else if("mysql".equals(dbType)){
					pageSql.append(sql+" limit "+((this.pageNo-1)*this.pageNum)+" ,"+this.pageNum);
					
				}else if("mssqlserver".equals(dbType)){
					sql = sql.replace("select ", "");
					pageSql.append("select top "+this.pageNum+" "+sql);
				}else if("db2".equals(dbType)){
					pageSql.append(sql+" first "+this.pageNum+"  rows only");
				}
			}else{
				pageSql.append(sql);
			}
			sql=pageSql.toString();
		}
		return sql;
	}
	
	
	public String getHtml() {
		StringBuffer sb = new StringBuffer();
		int pg=this.getPageCount(pageCount);
		if ( pg > 1) {
			sb.append("	<div class='btn-p'>\n");
			
				if (pageNo == 1) {
					if(pageType>0) {
						sb.append("	<a class='btn-0' style=''>共 " + pageCount + " 条</a>\n");
						sb.append("	<a class='btn-0' style=''> " + (pg) + " 页</a>\n");
						sb.append("	<a class='' style='display:none'><input type=\"number\" value=\""+pageNo+"\" id=\"toGoPage\" style=\"width:40px;text-align:center;float:left\" placeholder=\"页码\"/></a>\n");
						sb.append("	<a class='' style=\"display:none\"onclick=\"toTZ();\" >跳转</a>\n");
					}
					sb.append("	<a class='lab-1'>«</a>\n");
					sb.append("	<a class='lab-1'>‹</a>\n");
				} else {
					if(pageType>0) {
						sb.append("	<a class='btn-0' style=''>共 " + pageCount + " 条</a>\n");
						sb.append("	<a class='btn-0' style=''> " + (pg) + " 页</a>\n");
						sb.append("	<a style='display:none'><input type=\"number\" value=\""+pageNo+"\" id=\"toGoPage\" style=\"width:50px;text-align:center;float:left\" placeholder=\"页码\"/></a>\n");
						sb.append("	<a style=\"display:none\" onclick=\"toTZ();\">跳转</a>\n");
					}
					sb.append("	<a class='btn-1' onclick=\"nextPage(this,1)\">«</a>\n");
					sb.append("	<a class='btn-1' onclick=\"nextPage(this," + (pageNo - 1) + ")\">‹</a>\n");
				}


			if (pageNo >= showNum) {
				startTag = pageNo - 1;
			}
			int endTag = startTag + showNum - 1;
			for (int i = startTag; i <= pg && i <= endTag; i++) {
				if (pageNo == i){
					sb.append("<a class='lab-1 cclick'>" + i + "</a>\n");
				}else{
					sb.append("	<a class='btn-1' onclick=\"nextPage(this," + i + ")\">" + i + "</a>\n");
				}
			}
			if (pageNo == pg) {
				sb.append("	<a class='lab-1'>›</a>\n");
				sb.append("	<a class='lab-1'>»</a>\n");
			} else {
				sb.append("	<a class='btn-1' onclick=\"nextPage(this," + (pageNo + 1) + ")\"title='" +(pageNo + 1)+"'>›</a>\n");
				sb.append("	<a class='btn-1' onclick=\"nextPage(this," + pg + ")\"title='" +pg+"'>»</a>\n");
			}
			//sb.append("	<li><font>第" + currentPage + "页</font></li>\n");
			//sb.append("	<li><font>共" + totalPage + "页</font></li>\n");
			if(pageType>1) {
				sb.append("	<a style='display:none'><select class='showpgnum' title='显示条数' style=\"width:55px;float:left;\" onchange=\"changeCount(this.value)\">\n");
				sb.append("	<option value='" + pageNum + "'>" + pageNum + "</option>\n");
				sb.append("	<option value='10'>10</option>\n");
				sb.append("	<option value='20'>20</option>\n");
				sb.append("	<option value='30'>30</option>\n");
				sb.append("	<option value='40'>40</option>\n");
				sb.append("	<option value='50'>50</option>\n");
				sb.append("	<option value='60'>60</option>\n");
				sb.append("	<option value='70'>70</option>\n");
				sb.append("	<option value='80'>80</option>\n");
				sb.append("	<option value='90'>90</option>\n");
				sb.append("	</select>\n");
				sb.append("	</a>\n");
			}
			sb.append("</div>\n");
		}else {
			
		}
		html = sb.toString();
		return html;
	}
	
	
	public String getMore() {
		StringBuffer sb = new StringBuffer();
		int pg=this.getPageCount(pageCount);
		if ( pageNo < pg) {
			int npage=pageNo+1;
			sb.append("	 <a class=\"cu-a\" onclick=\"morePage(this,"+npage+")\"> 查看更多.. </a>");
		}
		more=sb.toString();
		return more;
	}



	/**
	 * 公共处理page
	 * @param map
	 */
	public void initPageMap(Map<String,Object> map){
		if(map.get("page.pageNo")==null){
			map.put("page.pageNo", 1);
		}
		if(map.get("page.pageNum")==null){
			map.put("page.pageNum", 10);
		}
	}

	/**
	 * 获取PageSize
	 * @param map
	 * @return
	 */
	public int getPageSize(Map<String,Object> map){
		String ps=map.get("page.rsSize")==null?"0":map.get("page.rsSize")+"";
		int rsSize=Integer.parseInt(ps);
		String po=map.get("page.pageNum")==null?"0":map.get("page.pageNum")+"";
		int pageNum=Integer.parseInt(po);
		int n=rsSize/pageNum;
		if(rsSize%pageNum>0){
			n++;
		}
		return n;
	}
	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getPageCount() {
		int n=rsCount/pageNum;
		if(rsCount%pageNum>0){
			n++;
		}
		if(n==0){n++;}
		return n;
	}

	/**得到总页数*/
	public int getPageCount(int rsSize){
		int n=rsSize/pageNum;
		if(rsSize%pageNum>0){
			n++;
		}
		return n;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getRsCount() {
		return rsCount;
	}

	public void setRsCount(int rsCount) {
		this.rsCount = rsCount;
		pageCount=rsCount/pageNum;
		if(rsCount%pageNum>0){
			pageCount++;
		}
		if(pageCount==0){
			pageCount++;
		}
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public int getShowNum() {
		return showNum;
	}

	public void setShowNum(int showNum) {
		this.showNum = showNum;
	}

	public int getStartTag() {
		return startTag;
	}
	public void setStartTag(int startTag) {
		this.startTag = startTag;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public void setMore(String more) {
		this.more = more;
	}

	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public int getPageType() {
		return pageType;
	}

	public void setPageType(int pageType) {
		this.pageType = pageType;
	}

}
