package com.srczh.manage.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.srczh.manage.SrcConfig;
import com.srczh.manage.interna.Internation;

/**
 * SrcUtil 工具类
 * @author zhoujun
 */
@SuppressWarnings("unchecked")
public class SrcUtil {

	
	/**
	 * 绑定传参到map
	 * @param map
	 * @return
	 */
	public static Map<String,Object> changeParamMap(Map<?,?> parametermap){
		 String prams=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.parameter);
	      // 返回值ConcurrentHashMap
	      Map<String,Object> returnmap = new HashMap<>();
	      Iterator<?> entries = parametermap.entrySet().iterator();
	      Map.Entry<?,?> entry;
	      String name = "";
	      String value = "";
	      if(entries.hasNext()){
	    	 Slog.println("	"+prams+"-->");
	      }
	      while (entries.hasNext()) {
	          entry = (Map.Entry<?,?>) entries.next();
	          name = (String) entry.getKey();
	          Object valueObj = entry.getValue();
	          if(null == valueObj){
	              value = "";
	          }else if(valueObj instanceof String[]){
	              String[] values = (String[])valueObj;
	              for(int i=0;i<values.length;i++){
	                  value += values[i].trim() + ",";
	              }
	              value = value.substring(0, value.length()-1);
	          }else{
	              value = valueObj.toString().trim();
	          }
	          //封装前,对value进行过滤
	         // value= valFilters(value+"");
	          Slog.println("	   "+name+"="+(value.length()>100?value.substring(0,100)+"...":value));
	          returnmap.put(name, value);
	          value="";
	          if(!entries.hasNext()){
	        	  Slog.println("	"+prams+"<--");
	          }
	      }
	      return  returnmap;
	  }

	
	/**
	 * 对字符值进行过滤
	 * @param chs
	 * @return
	 */
	public static String valFilters(String chs){
		String vaid=SrcConfig.PAMAMETERSHIELDING;
		if(chs!=null && !"".equals(chs) && !"".equals(vaid)){
			String [] vids=vaid.split(",");
			for(int i=0;i<vids.length;i++){
				String sv=vids[i].trim();
				if(!"".equals(sv)){
					chs=chs.replace(sv, "");
				}
			}
		}
		return chs;
	}
	
	
	
	/**
	 * 取list参数到全局map
	 * @param ls
	 * @param key
	 * @param val
	 * @return
	 */
	public static Map<Object,Object> getConfig(List<?> ls,String key,String val){
		Map<Object,Object> rmap = new HashMap<>();
		for(int i=0;i<ls.size();i++){
			Map<String,Object> tm=(Map) ls.get(i);
			rmap.put(tm.get(key), tm.get(val));
		}
		return rmap;
	}
	
	
	/**
	 * 将List 返回Json 
	 * @param ls
	 * @return
	 */
	public static String getJsonToList(List<Object> ls) {
		if (ls == null || ls.size() <= 0) {
			return "[]";
		}
		String json = "";
		for (int i = 0; i < ls.size(); i++) {
			Map<String,Object> tmp = (Map) ls.get(i);
			if (i + 1 == ls.size()) {
				json += "{" + changeMapToJson(tmp,"") + "}";
			} else {
				json += "{" + changeMapToJson(tmp,"") + "},";
			}
		}
		return "[" + json + "]";
	}
	
	/**
	 * 从实体类转换到ConcurrentHashMap
	 * @param obj
	 * @return
	 */
	public static Map<String, Object> objToMap(Object obj) {
        Map<String, Object> map = new HashMap<>();
        if (obj == null) {
            return map;
        }
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                map.put(field.getName(), field.get(obj));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
	
	
	/**
     * Map转成实体对象
     * @param Map   实体属性Map
     * @param clas 实体对象
     * @return
     */
    public static Object MapToObj(Map<String, Object> map, Class<?> clas) {
        if (map == null) {
            return null;
        }
        Object obj = null;
        try {
            obj = clas.newInstance();
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                    continue;
                }
                field.setAccessible(true);
                field.set(obj, map.get(field.getName()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }



	
	/** 将ConcurrentHashMap数据转json字符串**/
	public static String changeMapToJson(Map<String,Object> map,String txt){
		StringBuffer sb = new StringBuffer();
        Set<String> key = map.keySet();
        Iterator<String> iter = key.iterator();
        txt=txt==""?"":txt+"_";
        int i=0;
        while (iter.hasNext()) {
        	i++;
            String field = iter.next();
            String tmp= map.get(field)==null?"":map.get(field)+"";
            if(i==map.size()){
            	sb.append("\""+txt+field + "\":\"" + tmp+"\"");
            }else{
            	sb.append("\""+txt+field + "\":\"" + tmp+"\",");
            }
        }
		return "{"+sb+"}";
	}
	
	
	/***
	 * 字符串格式化工具
	 * @param type 说明: html=html标签转义；unicode=转unicode字符；notunicode：翻转unicode；sensitive=脱敏字符；number=数值转义；time=时间转义
	 * @param mtop 需转义字符
	 * @return
	 */
	public static String formout(String type,String mtop) {
		String rs=""; 
		if("html".equalsIgnoreCase(type)) {
			StringBuffer buffer = new StringBuffer();
			for (int n = 0; n < mtop.length(); n++) {
				char c = mtop.charAt(n);
				switch (c) {
				case '<':
					buffer.append("&lt;");
					break;
				case '>':
					buffer.append("&gt;");
					break;
				case '&':
					buffer.append("&amp;");
					break;
				case '"':
					buffer.append("&quot;");
					break;
				default:
					buffer.append(c);
				}
			}
			rs = buffer.toString();
		}
		//转unicode
		else if( "unicode".equalsIgnoreCase(type) ) {
			StringBuffer buffer = new StringBuffer();
			for (int n = 0; n < mtop.length(); n++) {
				buffer.append("\\u" + Integer.toString(mtop.charAt(n), 16));
			}
			rs = buffer.toString();
			//反转unicode
		}else if("notunicode".equalsIgnoreCase(type)) {
			StringBuffer sb = new StringBuffer();
			String[] hex = mtop.split("\\\\u");
			for (int n = 1; n < hex.length; n++) {
				int index = Integer.parseInt(hex[n], 16);
				sb.append((char) index);
			}
			rs = sb.toString();
			//字符脱敏
		}else if("sensitive".equalsIgnoreCase(type)) {
			int ls= mtop.length();
			int js=Math.round(ls/2);
			if(js>=ls) {
				js=js--;
			}else if(js==0) {
				js=1;
			}
			String jms=mtop.substring(0,js);
			mtop=mtop.replace(jms, "***");
			rs = mtop;	
		}else if("number".equalsIgnoreCase(type)) {
			Pattern pattern = Pattern.compile("-?[0-9]+\\.?[0-9]*");
				Matcher isNum = pattern.matcher(mtop);
				//非数字
				if (!isNum.matches()) {
					return "";
				}else {
					String dw="",tp="";
					if(mtop.length()==4) {
						dw="K+";
						tp=mtop.substring(0,1);
					}else if(mtop.length()==5) {
						dw="W+";
						tp=mtop.substring(0,1);
					}else if(mtop.length()==6) {
						dw="W+";
						tp=mtop.substring(0,2);
					}else  if(mtop.length()==7){
						dw="W+";
						tp=mtop.substring(0,3);
					}else  if(mtop.length()==8){
						dw="KW+";
						tp=mtop.substring(0,1);
					}else  if(mtop.length()==9){
						dw="E+";
						tp=mtop.substring(0,1);
					}else  if(mtop.length()>9){
						dw="..E+";
						tp=mtop.substring(0,1);
					}
					else {
						tp=mtop;
					}
					rs=tp+dw;
				}
		}else if("time".equalsIgnoreCase(type)) {
			java.text.DateFormat  sf;
			if(mtop.indexOf(" ")>1 && mtop.indexOf(":")>1){
					sf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				}else{
					sf= new SimpleDateFormat("yyyy-MM-dd");
				}
				long st=0;
				try {
				Date d= sf.parse(mtop);
				st=d.getTime()/1000;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			rs=DateUtil.convertTimeToFormat(st);
		}
		return rs;
	}
	
	
//	/** 将list数据转ConcurrentHashMap**/
//	public static ConcurrentHashMap<?, ?> changeListToConcurrentHashMap(List<Object> list){
//        Map<String, Object> map = new ConcurrentHashMap<>();  
//        if (list != null) {  
//            for (int i = 0; i < list.size(); i++) {  
//            	map = (Map<String, Object>) list.get(i);  
//            }  
//        }  
//        return map; 
//	}
	
	
	
	/**使用正则解析页面内容
	 * 返回list
	 * **/
	public static List<String> getSubUtilToList(String soap,String rgex){  
        List<String> list = new ArrayList<String>();  
        Pattern pattern = Pattern.compile(rgex);// 匹配的模式  
        Matcher m = pattern.matcher(soap);  
        while (m.find()) {  
            int i = 1;  
            list.add(m.group(i));  
            i++;  
        }  
        return list;  
    }
	
	/**使用正则解析页面内容
	 * 返回字符串
	 * **/
	 public static String getSubUtilSimple(String soap,String rgex){  
	        Pattern pattern = Pattern.compile(rgex);// 匹配的模式  
	        Matcher m = pattern.matcher(soap);  
	        while(m.find()){  
	            return m.group(1);  
	        }  
	        return "";  
	 }
	 
		/**
		 * 形参任意日期格式
		 * yyyy-MM-dd | HH:mm:ss
		 * @param ys
		 * @return
		 */
		public static String getDateTime(String ys){
			SimpleDateFormat df = new SimpleDateFormat(ys);//设置日期格式""
			return df.format(new Date());
		}
		/**
		 * 带日期形参任意日期格式
		 * @param date
		 * @param ys
		 * @return
		 */
		public static String getDateTime(Date date ,String ys){
			SimpleDateFormat df = new SimpleDateFormat(ys);//设置日期格式""
			return df.format(date);
		}
		
		/**
		 * 返回当前数字比例下一个随机整数
		 * @param num
		 * @return
		 */
		public static int nextInt(int num){
			String sn=(Math.random()*num)+"";
			sn=sn.substring(0,sn.indexOf("."));
			int it=Integer.parseInt(sn);
		//	System.out.println(sn);
			return it;
		}
		
		/**
		 * 返回一个随机浮点数
		 * @return
		 */
		public static float nextFloat(){
			String dt=Math.random()+"";
			float lt=Float.parseFloat(dt);
		//	System.out.println(lt);
			return lt;
		}
		
		
		/**
		 * //按数量取任意字符
		 * @param num
		 * @return
		 */
		public static String randomString(int num,String type) {
			String sc="";
			for(int i=0;i<num;i++) {
				sc+=randomChar(type);
			}
			return sc;
		}
		
		
		/**
		 * //取一个任意字符
		 * @return
		 */
		private static char randomChar(String type) {
			Random r = new Random();
			String s = "";
			 if("num".equals(type)) {
				s = "0123456789";
			}else if("uchar".equals(type)) {
				s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			}else if("lchar".equals(type)) {
				s = "abcdefghijklmnopqrstuvwxyz";
			}else {
				s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
			}
			return s.charAt(r.nextInt(s.length()));
		}
		
		
		/**
		 * //按文件类型取代码名称
		 * @param ftype
		 * @return
		 */
		public static String fileSuff(String ftype) {
			if("js".equalsIgnoreCase(ftype)) {
				return "javascript";
			}else if("xml".equalsIgnoreCase(ftype)||"json".equalsIgnoreCase(ftype)) {
				return "application/xml";
			
			}else if("aspx".equalsIgnoreCase(ftype)||"asp".equalsIgnoreCase(ftype)) {
				return "application/x-aspx";
			}else if("php".equalsIgnoreCase(ftype)) {
				return "application/x-httpd-php";
			}else if("java".equalsIgnoreCase(ftype)) {
				return "text/x-java";
			}else if("jsp".equalsIgnoreCase(ftype)) {
				return "application/x-jsp";
			
			}else if("html".equalsIgnoreCase(ftype)||"htm".equalsIgnoreCase(ftype)) {
				return "text/x-haml";
			}else if("css".equalsIgnoreCase(ftype)) {
				return "text/css";
			
			}else if("sql".equalsIgnoreCase(ftype)) {
				return "text/x-sql";
			
			}else if("cpp".equalsIgnoreCase(ftype)||"h".equalsIgnoreCase(ftype)) {
				return "text/x-objectivec";
			}
			return "text/x-textile";
		}
		 
		public static String grantAuthorization(String author) {
			if(author==null|| "".equals(author)) {
				return "";
			}
			author = author.toUpperCase();
			int len=author.length();
			//第1个字符随机
			//找到第二个与第一个相同字符或截取长度
			//中间为加密对象长度
			//取加密对象*2
			//在最后取加密长度*1
			//匹配两段加密
			if(len<=20) {
				String sta=SrcUtil.randomString(1,"uchar");
				String staa=numtochar(len,3);
				String stb = "";
				String stbb=SrcUtil.randomString(len,"uchar");
				for(int i=len;i>0;i--) {
					String ds=author.substring(i-1,i);
					if(ds=="."||".".equals(ds)) {ds="/";}
					stb=stb+ds;
					stb=stb+stbb.substring(i-1,i);
				}
				String ns = sta+staa+stb;
				int cs=(ns+stbb).length();
				String css = SrcUtil.randomString(64-cs,"uchar");

				return ns+css+stbb;
			}
			return "";
		}
		
		//解析授权码
		public static String analysisAuthorization(String achar) {
			if(achar.length()==64) {
				String nums = achar.substring(1,4);
				int len = chartonum(nums);
				String asc=achar.substring(3,len*2+3);
				String ac="",tc="";
				for(int i=asc.length();i>0;i=i-2) {
					String ds=asc.substring(i-2,i-1);
					if(ds=="/"||"/".equals(ds)) {ds=".";}
					ac+=ds;
					tc+=asc.substring(i-1,i);
				}
				
				String ec=achar.substring(achar.length()-tc.length());
				if(ec==tc||ec.equals(tc)) {
					return ac;
				}
			}
			return "";
		}
		
		public static String numtochar(int num,int len) {
			String lens=num+"";
			String cs="";
			for(int i=0;i<lens.length();i++) {
				String n=lens.substring(i,i+1);
				if(n.equals("1")||n=="1") {cs+="q";}
				if(n.equals("2")||n=="2") {cs+="w";}
				if(n.equals("3")||n=="3") {cs+="e";}
				if(n.equals("4")||n=="4") {cs+="r";}
				if(n.equals("5")||n=="5") {cs+="t";}
				if(n.equals("6")||n=="6") {cs+="y";}
				if(n.equals("7")||n=="7") {cs+="u";}
				if(n.equals("8")||n=="8") {cs+="i";}
				if(n.equals("9")||n=="9") {cs+="o";}
				if(n.equals("0")||n=="0") {cs+="p";}
			}
			//保持长度
			if(cs.length()<len) {
				for(int i=0;i<(len-cs.length());i++) {
					cs="p"+cs;
				}
			}
			return cs.toUpperCase();
		}
		
		public static int chartonum(String car){
			String cs="";
			car=car.toLowerCase();
			for(int i=0;i<car.length();i++) {
				String n = car.substring(i,i+1);
				if(n.equals("q")||n=="q") {cs+="1";}
				if(n.equals("w")||n=="w") {cs+="2";}
				if(n.equals("e")||n=="e") {cs+="3";}
				if(n.equals("r")||n=="r") {cs+="4";}
				if(n.equals("t")||n=="t") {cs+="5";}
				if(n.equals("y")||n=="y") {cs+="6";}
				if(n.equals("u")||n=="u") {cs+="7";}
				if(n.equals("i")||n=="i") {cs+="8";}
				if(n.equals("o")||n=="o") {cs+="9";}
				if(n.equals("p")||n=="p") {cs+="0";}
			}
			return Integer.parseInt(cs);
		}
		
		
		
		
		
		public static void main(String[] qas) {
//			String sc=randomString(10);
//			System.out.println(sc);
////				for(int i =0;i<10;i++) {
//					char s=randomChar();
//					System.out.println(s);
//					int n = nextInt(10000);
//					System.out.println(n);
////				}
		}		
		
		
		
		
}
