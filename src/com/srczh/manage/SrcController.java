package com.srczh.manage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.srczh.manage.interna.Internation;
import com.srczh.manage.util.Slog;
import com.srczh.manage.util.SrcJson;
import com.srczh.util.ZipTools;


/**
 * SrcAction Action超类
 * @author zhoujun
 */
@SuppressWarnings("all")
public class SrcController {

	//private static HttpServletRequest request=null;
	//private static HttpServletResponse response=null;
	private static ThreadLocal<HttpServletRequest> request=new ThreadLocal<>();
	private static ThreadLocal<HttpServletResponse> response=new ThreadLocal<>();
	private static ThreadLocal<Map<String,Object>> prameMap=new ThreadLocal<>();
	
	private String respontxt=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.responsetxt);
	private String loginfail=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.loginfail);
	private String profail=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.profail);
	

	private void init(Map<String,Object> map) {
		request.set((HttpServletRequest) map.get("src_request"));
		response.set((HttpServletResponse) map.get("src_response"));
		Map<String,Object> daoImplMap = (Map<String, Object>) map.get("src_daomap");
		String pkg= (String) map.get("src_pakg");
		map.remove("src_request");
		map.remove("src_response");
		map.remove("src_pakg");
		map.remove("src_daomap");

		Map<String,Object> asm=new HashMap<>();
		prameMap.set(changeMap(map,asm));
		prameMap.get().remove("src_shili");
		prameMap.get().remove("src_action");
		prameMap.get().remove("src_method");

		String action =pkg+"."+map.get("src_action");
		Class<?> ac=null;
		Object cpath=null;

		try {
			ac = Class.forName(action);
			Constructor<?> con = ac.getConstructor(new Class<?>[] {});
			Object instance = con.newInstance(new Object[] {});

			//Action object
			Field fields[] = instance.getClass().getDeclaredFields();
			String name = "";
			String value = "";

			for (Field field : fields) {
				Class<?> ft=field.getType();
				String pname=field.getName();

				//bading list
				if(java.util.List.class.isAssignableFrom(ft)){
					
					ParameterizedType listGenericType = (ParameterizedType) field.getGenericType();
					Type[] listActual = listGenericType.getActualTypeArguments();
					if (!(listActual[0] instanceof ParameterizedType)) {
					}
					Class<?> fx=(Class) listActual[0];
					Constructor<?> fcon = fx.getConstructor();
					List als=new ArrayList();
					String kname="";

					for(Object key:asm.keySet()) {
						name =key.toString();
						value = asm.get(name).toString();
						Object cinstance= fcon.newInstance();

						String spn=name.split("_")[0];
						if(pname.equalsIgnoreCase(spn)){
							String [] fval=value.split(",");
							for(int n=0;n<fval.length;n++){
								String[] tv= fval[n].split(":");
								Field fu = cinstance.getClass().getDeclaredField(tv[0]);
								fu.setAccessible(true);
								fu.set(cinstance, tv[1]);
							}
							als.add(cinstance);
						}
					}

					if(als.size()>0){
						field.setAccessible(true);
						field.set(instance,als);
						prameMap.get().put(pname, als);
					}
					als=new ArrayList<>();

				//bading Object
				}else{
					Object finstance =null;
					if(asm.get(pname)!=null) {
						if(finstance==null){
							Constructor<?> fcon = ft.getConstructor();
							finstance= fcon.newInstance();
						}
						
						String[] vals=asm.get(pname).toString().split(",");
						for(int i=0;i<vals.length;i++) {
							String[] val=vals[i].split(":");
							if(!"".equals(val[0].trim())) {
								Field fu = finstance.getClass().getDeclaredField(val[0].trim());
								fu.setAccessible(true);
								String ftype=fu.getGenericType().toString();
								if("string".equalsIgnoreCase(ftype)){
									fu.set(finstance, val[1]);
								}else if("int".equalsIgnoreCase(ftype)){
									fu.set(finstance, Integer.parseInt(val[1]));
								}else if("float".equalsIgnoreCase(ftype)){
									fu.set(finstance, Float.parseFloat(val[1]));
								}else if("double".equalsIgnoreCase(ftype)){
									fu.set(finstance, Double.parseDouble(val[1]));
								}else if("long".equalsIgnoreCase(ftype)){
									fu.set(finstance, Long.parseLong(val[1]));
								}else{
									fu.set(finstance, val[1]);
								}
							}
						}
					}
	
					if(finstance!=null){
						field.setAccessible(true);
						field.set(instance,finstance);
						prameMap.get().put(pname, finstance);
					}
					finstance=null;
				}

				//src daoimpl
				String fns= ft.getCanonicalName();
				fns=fns.substring(fns.lastIndexOf(".")+1);
				Object sobj=daoImplMap.get(fns+"Impl");
				if(sobj!=null){
					field.setAccessible(true);
					field.set(instance,sobj);
				}
			}

			Method mds = ac.getMethod(map.get("src_method")+"",  new Class[]{});
			if(instance!=null){
				cpath= (Object) mds.invoke(instance, new Object[]{});
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/404.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		}catch (NoSuchMethodException  e) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/404.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		}catch (SecurityException e) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/404.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		} catch (InstantiationException e ) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/error.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		} catch (IllegalArgumentException e ) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/error.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		}catch (IllegalAccessException e ) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/error.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		}
		catch (InvocationTargetException e) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/error.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		}
		catch (NoSuchFieldException e) {
			e.printStackTrace();
			RequestDispatcher rd = request.get().getRequestDispatcher("/error.html");
			try {
				rd.include(request.get(), response.get());
			} catch (ServletException | IOException e1) {
				e1.printStackTrace();
			}
			return;
		}

		//srcAction retuen
		if(cpath!=null){
			try {
				//return SrcView
				if(cpath instanceof SrcView){
					SrcView sv=(SrcView)cpath;
					
					//doc
					if("doc".equalsIgnoreCase(sv.getType())&&sv.getData()!=null) {
						response.get().setHeader("Content-file", "srcview/doc;charset=UTF-8");
						request.get().setAttribute("src_bind", sv.getData());
						RequestDispatcher rd = request.get().getRequestDispatcher(sv.getUrl());
						rd.include(request.get(), response.get());

					//excel
					}else if("csv".equalsIgnoreCase(sv.getType())&&sv.getData()!=null) {
						if(sv.getData()==null) {
							return;
						}
						Map<String,Object> emap=(Map) sv.getData();
						String head=emap.get("xls_head")==null?"":emap.get("xls_head").toString();
						List<?> bodys=emap.get("xls_body")==null?new ArrayList<Object>():(ArrayList<Object>) emap.get("xls_body");
						if(!"".equals(head)) {
							StringBuffer sb=new StringBuffer();
							sb.append(head+SrcConfig.SRC_HHF);
							for(int i=0;i<bodys.size();i++) {
								sb.append(bodys.get(i)+SrcConfig.SRC_HHF);
							}
							response.get().setHeader("Content-file", "srcview/csv;charset=UTF-8");
							ServletOutputStream out=response.get().getOutputStream();
							OutputStreamWriter ow=new OutputStreamWriter(out,SrcConfig.SRC_CODING);
							ow.write(sb+"");
							ow.flush();
							ow.close();
							return;
						}
					} else {
						if(sv.getData()!=null){
//							SrcJson dmap = new SrcJson();
//							dmap.put("url", sv.getUrl());
//							dmap.put("data", sv.getData());
//							dmap.put("type", sv.getType()==null?"srcview":sv.getType());
//							dmap.put("bind", sv.getBind());

							response.get().setHeader("Content-type", "text/html;charset=UTF-8");
							request.get().setAttribute("src_bind", sv.getData());
						}

						if(sv.getUrl()!=null){
							RequestDispatcher rd = request.get().getRequestDispatcher(sv.getUrl());
							response.get().setHeader("Content-type", "text/html;charset=UTF-8");
							rd.include(request.get(), response.get());
							Slog.info("["+respontxt+"]："+sv.getUrl());
						}
					}

				//return array
				}else if(cpath.getClass().isArray()){
					Object[] path=(Object[]) cpath;
					String surl=path[0]+"";
					//return http
					if(surl.indexOf("http")==0){
						response.get().sendRedirect(surl+"");
						return;
					}
					if(path.length>1) {
						request.get().setAttribute("src_bind", path[1]);
					}
					RequestDispatcher rd = request.get().getRequestDispatcher(surl);
					rd.include(request.get(), response.get());
					Slog.info("["+respontxt+"]："+path[0]);
				}else{
					if(cpath.toString().indexOf("http")==0){
						response.get().sendRedirect(cpath+"");
						return;
					}
					RequestDispatcher rd = request.get().getRequestDispatcher(cpath.toString());
					rd.include(request.get(), response.get());
					Slog.info("["+respontxt+"]："+cpath);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServletException e) {
				e.printStackTrace();
			}
			cpath=null;
		}
	}
	
	/**
	 * 转换 ConcurrentHashMap
	 * @param parametermap
	 * @return
	 */
	private Map<String,Object> changeMap(Map<String,Object> pamap,Map<String,Object> cmap){
		Map<String,Object> returnMap = new HashMap<>();
		Iterator<?> entries = pamap.entrySet().iterator();
		Map.Entry<String,Object> entry;
		String name = "";
		String value = "";

		while (entries.hasNext()) {
			entry = (Map.Entry) entries.next();
			name = (String) entry.getKey();
			Object valueObj = entry.getValue();

			//bading Object
			if(name.indexOf(".")>1){
				//List uls[1].name
				if(name.indexOf("[")>1&&name.indexOf("]")>1){
					String tme= name.substring(0,name.indexOf("."));
					String pval = name.substring(name.indexOf(".")+1);
					String pname=tme.substring(0,name.indexOf("["));
					String no=tme.substring(tme.indexOf("[")+1,tme.indexOf("]"));
					
					//chang setup ConcurrentHashMap
					if(cmap.get(pname+"_"+no)==null){
						cmap.put(pname+"_"+no, pval+":"+valueObj);
					}else{
						String opv=cmap.get(pname+"_"+no).toString();
						//uls_1=id:1,name:aaa
						//uls_2=id:2,name:bbb
						cmap.put(pname+"_"+no, pval+":"+valueObj+","+opv);
					}
					//  ls.add(cmap);

				}else{
					String pname= name.substring(0,name.indexOf("."));
					String pval =  name.substring(name.indexOf(".")+1);
					
					//组织一个对象放一个ConcurrentHashMap中
					if(cmap.get(pname)==null){
						cmap.put(pname, pval+":"+valueObj);
					}else{
						//有ConcurrentHashMap保存
						String opv=cmap.get(pname).toString();
						//user=id:1,name:aaa
						//user=id:2,name:bbb
						cmap.put(pname, pval+":"+valueObj+","+opv);
					}
				}
				continue;
			}

			if(null == valueObj){
				value = "";
			}else if(valueObj instanceof String[]){
				String[] values = (String[])valueObj;
				for(int i=0;i<values.length;i++){
					if(value==""){value=values[i].trim();}
					else{
						value = value+","+values[i].trim();
					}
				}
				value = value.substring(0, value.length()-1);
			}else{
				value = valueObj.toString().trim();
			}
			returnMap.put(name, value);
			value="";
		}
		return  returnMap;
	}


	
	/**
	 * 下载文件
	 * @param path
	 * @throws IOException
	 */
	protected void srcDownFile(String path) throws IOException {
		Slog.println("	[down]-->"+path);
		File f = new File(path);
		if(!f.exists()) {
			String fpath = path.substring(0,path.lastIndexOf("."));
			String ftype = path.substring(path.lastIndexOf(".")+1);
			if(ftype.equalsIgnoreCase("zip")) {
				try {
					ZipTools.zipProj(fpath, "zip");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		String filename=URLEncoder.encode(f.getName(),SrcConfig.SRC_CODING); 
		if(f.exists()){
			 FileInputStream fis = null;
	         BufferedInputStream bis = null;
	         response.get().setContentType("application/octet-stream");
	         response.get().setCharacterEncoding(SrcConfig.SRC_CODING);  
	         response.get().setHeader("Content-Disposition","attachment; filename="+filename+""); 
	        //response.get()..setHeader("Content-file","src-down");
			//this.getResponse().setHeader("Content-Disposition", "attachment;filename=" +fname+""); 
	         response.get().setHeader("Content-file", "srcview/file;charset=UTF-8");
	         
	         try {
				byte[] buffer = new byte[4096];
	            fis = new FileInputStream(path);
	            bis = new BufferedInputStream(fis);
	            OutputStream os = response.get().getOutputStream();
	            int i = bis.read(buffer);
	            while (i != -1) {
	                os.write(buffer, 0, i);
	                i = bis.read(buffer);
	            }
	         } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
		}
	}

	/**
	 * 绑定输出对象
	 * @param name 对象命名
	 * @param val 对象值
	 */
	protected void srcBind(String name,Object val) {
		this.request.get().setAttribute(name, val);
	}
	/**
	 * 绑定输出ConcurrentHashMap
	 * @param ConcurrentHashMap
	 */
	protected void srcBind(Map<String,Object> map) {
		if(map==null) {
			return ;
		}
		Iterator<?> entries = map.entrySet().iterator();
		Map.Entry<String,Object> entry;
		String name = "";
		while (entries.hasNext()) {
			entry = (Map.Entry) entries.next();
			name = (String) entry.getKey();
			Object valueObj = entry.getValue();
			this.request.get().setAttribute(name, valueObj);
		}
	}
	/**
	 * 获取绑定对象
	 * @param name
	 * @return
	 */
	protected Object getBind(String name) {
		return this.request.get().getAttribute(name);
	}

	/**
	 * 获取session对象
	 * @param session
	 * @return
	 */
	protected Object getSession(String session){
		return this.getRequest().getSession().getAttribute(session);
	}
	/**
	 * 获取session操作
	 * @param session
	 * @return
	 */
	protected Object getSession(){
		return this.getRequest().getSession();
	}
	/**
	 * 设置session对象
	 * @param name
	 * @param val
	 */
	protected void setSession(String name,Object object){
		this.getRequest().getSession().setAttribute(name, object);
	}
	/**
	 * 删除session对象
	 * @param session
	 */
	protected void removeSession(String session){
		this.getRequest().getSession().removeAttribute(session);
	}
	/**
	 * 获取全局对象数据
	 * @param context
	 * @return
	 */
	public Object getSrcContext(String context) {
		return this.getRequest().getSession().getServletContext().getAttribute(context);
	}
	/**
	 * 设置全局对象数据
	 * @param context
	 * @param object
	 * @return
	 */
	public void setSrcContext(String context,Object object) {
		this.getRequest().getSession().getServletContext().setAttribute(context,object);
	}
	/**
	 * 删除全局对象数据
	 * @param context
	 */
	protected void removeContext(String context){
		this.getRequest().getSession().getServletContext().removeAttribute(context);
	}

	/**
	 * 公共返回
	 * @param chars
	 */
	protected void write(String chars){
		String retuntxt=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.retuntxt);
		if(chars!=null){
			Slog.info("["+retuntxt+"]:"+chars);
		}
		try {
			this.getResponse().setHeader("Content-type", "text/html;charset=UTF-8");
			this.getResponse().getWriter().write(chars);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}



	/**
	 * 公用登录检查
	 * @param logintype
	 * @return
	 */
	public Map<String,Object> checkLogin(String logintype){
		Map<String,Object> umap=(Map<String, Object>) this.getRequest().getSession().getAttribute (logintype);
		return umap;
	}
	/**
	 * 登录失效,返回指定页面
	 * @param ontype
	 */
	public void redo(String url){
		String purl=this.request.get().getRequestURL().toString();
		String local=this.getRequest().getLocalAddr();
		String port=this.getRequest().getServerPort()+"";
		if(purl.indexOf(port)>0) {
			purl=purl.substring(purl.indexOf(port)+port.length());
		}else {
			purl=purl.substring(purl.indexOf(local)+local.length());
		}
		this.getRequest().getSession().setAttribute("purl", purl);
		
		String bf="";
		bf+="if(window.parent.document.body){";
		bf+="	if(window.parent.parent.document.body){";
				bf+="		if(window.parent.parent.parent.document.body){";
						bf+="			window.parent.parent.parent.location='"+url+"?s';";
						bf+="		}else{";
								bf+="			window.parent.parent.location='"+url+"?s';";
								bf+="		}";
								bf+="	}else{";
										bf+="		window.parent.location='"+url+"?s';";
										bf+="	}";
										bf+="}else{";
												bf+="	window.location='"+url+"?s';";
												bf+="}";
		
		try {
//			this.response.get()..sendRedirect(todo);
			this.write("<script>"+bf+"</script>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		srcBind("message","您需要登录！");
		return;
	}
	/**
	 * 系统失败返回
	 * @param erros
	 */
	private void doErrors(String erros){
		Map<String,Object> rmap=new HashMap<>();
		rmap.put("src_type", "system");
		rmap.put("src_state", -1);
		rmap.put("src_message", profail+": "+erros);
		this.write(SrcJson.mapToJson(rmap).toString());
	}

	
	public Map<String, Object> getPramMap() {
		return prameMap.get();
	}
//	public void setPramMap(Map<String, Object> prameMap) {
//		this.prameMap = prameMap;
//	}
	public HttpServletRequest getRequest() {
		return request.get();
	}

	public HttpServletResponse getResponse() {
		return response.get();
	}
    /**
     * 用于封装响应错误信息
     * @param message
     * @return
     */
    private String initError(String message) {
    	SrcJson obj=new SrcJson();
    	obj.put("state", 0);
        obj.put("error", 1);
        obj.put("message", message);
        return obj.toString();
    }

    /**防止重复操作
     	* 操作登记
     * */
    public void initComm(String comm) {
    	if(comm!=null&&!"".equals(comm)) {
    		HttpSession se =(HttpSession) this.getSession();
    		String sessid = se.getId();
    		this.setSession(comm, sessid+"-"+comm);
    	}
    }
    /**防止重复操作
     * 	防重机制：操作之前需要登记
               *       检查操作是否登记，否则错误操作
     * */
    protected boolean checkComm(String comm) {
    	if(comm!=null&&!"".equals(comm)) {
    		String tokens=this.getSession(comm)==null?"":this.getSession(comm)+"";
    		if(!"".equals(tokens)) {
    			this.removeSession(comm);
    			return true;
    		}
    	}
    	return false;
    }   
}
