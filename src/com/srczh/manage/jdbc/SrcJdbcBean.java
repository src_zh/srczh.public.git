package com.srczh.manage.jdbc;

public class SrcJdbcBean {
	
	String jdbcName;
	String jdbcDrive;
	String dbUrl;
	String dbUname;
	String dbUpwd;
	String test;
	
	public String getJdbcName() {
		return jdbcName;
	}
	public void setJdbcName(String jdbcName) {
		this.jdbcName = jdbcName;
	}

	public String getJdbcDrive() {
		return jdbcDrive;
	}
	public void setJdbcDrive(String jdbcDrive) {
		this.jdbcDrive = jdbcDrive;
	}
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getDbUname() {
		return dbUname;
	}
	public void setDbUname(String dbUname) {
		this.dbUname = dbUname;
	}
	public String getDbUpwd() {
		return dbUpwd;
	}
	public void setDbUpwd(String dbUpwd) {
		this.dbUpwd = dbUpwd;
	}
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
	
	
	
}
