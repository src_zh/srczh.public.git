package com.srczh.manage.jdbc;


import java.util.Map;

import com.srczh.manage.SrcConfig;

/**
 * SrcJdbc jdbc辅助类
 * @author zhoujun
 */
public class SrcJdbc {
	
	/**插入类型*/
	public final static int INSERT=1;
	/**修改类型*/
	public final static int UPDATE=2;
	/**删除类型*/
	public final static int DELETE=3;
	/**数据库系统语句*/
	public final static int SYSTEM=4;
	
	/**
	 * 数据表对象名称
	 * 多数据源情况下， Object= 数据源名.表名
	 */
	public String Object;
	/**
	 * 数据源名称
	 * 默认数据源 Defualt
	 */
	public String Pool=SrcConfig.SRCDFPOOL; 
	/**
	 * 数据查询 Where 条件对象
	 */
	public String Where;
	/**
	 * 数据查询 Like 条件对象
	 */
	public String Like;
	/**
	 * 数据查询 In 条件对象
	 */
	public String In;
	/**
	 * 数据修改 Set 对象
	 */
	public String Set;
	/**
	 * 数据查询 Select 列名称对象
	 */
	public String Fields;
	/**
	 * 数据查询 Ordey 排序对象
	 */
	public String Order;
	/**
	 * 数据查询 Group 分组对象
	 */
	public String Group;
	
	/**
	 * 数据库存储过程对象
	 */
	public String Call;
	//public String Clob;
	/**
	 * 数据库查询 Join 链接对象
	 */
	public String Join;
	
	/**
	 * 数据库查询返回
	 */
	public String Return;
	
	/**
	 * SrcJdbc查询定义使用预SQL
	 */
	public String Ysql;//使用预sql
	/**
	 * SrcJdbc查询使用预SQL参数数组 
	 */
	public Object[] SqlPrame;//预sql查询 需要这个参数数组

	/**
	 * SrcJdbc 使用查询缓存
	 * */
	public Boolean Cache=false;
	/**
	 * SrcJdbc SQL 执行类型
	 * */
	public int DDL;
	
	/**
	 * SrcJdbc 使用 Map<String,Object> 对象
	 * */
	public Map<String,Object> map;
	
	/**
	 * SrcJdbc 定义查询返回条数
	 *  默认为所有条
	 */
	public int Number =0;
	
	public SrcJdbc() {
		super();
	}
	
	public String getSet() {
		return Set;
	}
	public void setSet(String set) {
		Set = set;
	}
	public String getObject() {
		return Object;
	}
	public void setObject(String object) {
		Object = object;
	}
	public String getWhere() {
		return Where;
	}
	public void setWhere(String where) {
		Where = where;
	}
	public String getFields() {
		return Fields;
	}
	public void setFields(String fields) {
		Fields = fields;
	}
	public String getOrder() {
		return Order;
	}

	public void setOrder(String order) {
		Order = order;
	}

	public String getGroup() {
		return Group;
	}

	public void setGroup(String group) {
		Group = group;
	}

	public String getCall() {
		return Call;
	}

	public void setCall(String call) {
		Call = call;
	}

	public int getNumber() {
		return Number;
	}

	public String getLike() {
		return Like;
	}

	public void setLike(String like) {
		Like = like;
	}

	public String getIn() {
		return In;
	}

	public void setIn(String in) {
		In = in;
	}

	public void setNumber(int number) {
		Number = number;
	}

//	public String getClob() {
//		return Clob;
//	}
//
//	public void setClob(String clob) {
//		Clob = clob;
//	}


	public Map<String,Object> getMap() {
		return map;
	}

	public String getPool() {
		return Pool;
	}

	public void setPool(String pool) {
		Pool = pool;
	}

	public void setMap(Map<String,Object> map) {
		this.map = map;
	}

	public Boolean getCache() {
		return Cache;
	}

	public void setCache(Boolean cache) {
		Cache = cache;
	}

	public int getDDL() {
		return DDL;
	}

	public void setDDL(int dDL) {
		DDL = dDL;
	}

	public String getJoin() {
		return Join;
	}

	public void setJoin(String join) {
		Join = join;
	}

	public String getYsql() {
		return Ysql;
	}

	public void setYsql(String ysql) {
		Ysql = ysql;
	}

	public Object[] getSqlPrame() {
		return SqlPrame;
	}

	public void setSqlPrame(Object[] sqlPrame) {
		SqlPrame = sqlPrame;
	}

	public String getReturn() {
		return Return;
	}

	public void setReturn(String returns) {
		Return = returns;
	}

}
