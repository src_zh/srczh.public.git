package com.srczh.manage;


import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.srczh.manage.interna.Internation;
import com.srczh.manage.util.Slog;
import com.srczh.manage.util.SrcUtil;

/**
 * SrcServlet 重写HttpServlet
 * @author zhoujun
 */

@MultipartConfig
public class SrcServlet extends HttpServlet {
	private static final long serialVersionUID = -2153241422283931981L;
	private static final String INCLUDE_REQUEST_URI_ATTRIBUTE = "javax.servlet.include.request_uri";
	private static final String INCLUDE_SERVLET_PATH_ATTRIBUTE = "javax.servlet.include.servlet_path";
	private static final String INCLUDE_CONTEXT_PATH_ATTRIBUTE = "javax.servlet.include.context_path";
	private static final String INCLUDE_PATH_INFO_ATTRIBUTE = "javax.servlet.include.path_info";
	private static final String INCLUDE_QUERY_STRING_ATTRIBUTE = "javax.servlet.include.query_string";
	private static final String FORWARD_REQUEST_URI_ATTRIBUTE = "javax.servlet.forward.request_uri";
	private static final String FORWARD_CONTEXT_PATH_ATTRIBUTE = "javax.servlet.forward.context_path";
	private static final String FORWARD_SERVLET_PATH_ATTRIBUTE = "javax.servlet.forward.servlet_path";
	private static final String FORWARD_PATH_INFO_ATTRIBUTE = "javax.servlet.forward.path_info";

	private String requesttxt=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.requesttxt);
	private String plclass=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.plclass);
	private String plaction=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.plaction);
	private String profail=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.profail);

	private  static String host=SrcConfig.DOMAIN;
	private  static String pkg=SrcConfig.PACKAGE;
	private  static String acn=changeAction(SrcConfig.CONTROL);
	private  static Object daomap=SrcConfig.DAOMAP;



	public SrcServlet() {
		super();
	}

	public void init() throws ServletException {
	}


	public void todo(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset="+SrcConfig.SRC_CODING);
		request.setCharacterEncoding(SrcConfig.SRC_CODING);

		//解决跨域session不一致
		//response.setHeader("Access-Control-Allow-Credentials", "true");
		//response.setHeader("Access-Control-Allow-Origin" , "*" );
		//response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


		String bpath = request.getContextPath();  //工程名
		String cpath = request.getServletPath();  //Servlet名称
		String inc=request.getDispatcherType().name();
		String pam = request.getPathInfo()==null?"":request.getPathInfo();		  //请求参数


		if("INCLUDE".equalsIgnoreCase(inc)){
			pam=request.getAttribute(INCLUDE_REQUEST_URI_ATTRIBUTE)+"" ;
			String base=request.getAttribute(INCLUDE_SERVLET_PATH_ATTRIBUTE)+"" ;
			pam=pam.replace(base,"");
		}else{
			//	pam=pam.replace("/favicon.ico",	"");
		}

		if("/".equals(pam)||"".equals(pam)){
			response.sendRedirect("/");
		}
		// 获取请求是从哪里来的
		String referer = request.getHeader("referer");
		StringBuffer URL = request.getRequestURL();
		String ah =SrcConfig.GRANT;
		String dm = SrcConfig.DOMAIN.toLowerCase();
		String sname=request.getServerName();
		if(sname.equalsIgnoreCase(dm)) {
			response.getWriter().write("对不起，请访问配置地址  "+dm); return;
		}


		//初始化action，method，pam
		Slog.println(SrcConfig.SRC_HHF);
		Slog.info("["+requesttxt+"]: "+""+pam); 

		String[]as=new String[]{""};
		if(pam!=null){
			as= pam.split("/");
		}
		Map map = SrcUtil.changeParamMap(request.getParameterMap());
		String action="",method="",methodType="";
		if(as.length==1){
			action=changeAction("index");
			method="index";

		}else if(as.length==2){

			String path=as[1];
			if(path.indexOf(".")>-1){
				path=path.substring(0, path.indexOf("."));
			}
			action=changeAction(path);
			method="index";

		}else if(as.length==3){
			action=changeAction(as[1]);
			method=as[2];
			if(method.indexOf(".")>-1){
				method=method.substring(0, method.indexOf("."));
			}

			if(method.indexOf("_")>0){
				String[] tmp=method.split("_");
				method=tmp[0];
				methodType=tmp[1];
			}

			//带有参数
		}else  if(as.length==4){
			action=changeAction(as[1]);
			method=as[2];

			//方法中存在特殊符号
			if(method.indexOf(".")>-1){
				method=method.substring(0, method.indexOf("."));
			}
			map.put("ID", as[3]);
		}

		Slog.println("	"+plclass+": "+pkg+"."+action+acn,false);
		Slog.println("	"+plaction+": "+method,false);


		//回到默认页
		if(action==null||"".equals(action)){
			//重定向
			//response.sendRedirect("/index.html");
			//return;
			////response.sendRedirect("/");
			//内部跳转
			RequestDispatcher rd = request.getRequestDispatcher("/index.html");
			rd.include(request, response);return;
		}
		if(method==null||"".equals(method)){
			RequestDispatcher rd = request.getRequestDispatcher("/index.html");
			//内部跳转
			rd.include(request, response);return;
		}

		//需要跳转
		if(!"".equals(cpath.trim())){
			map.put("src_pakg", pkg);
			map.put("src_shili",action);
			map.put("src_action",action+acn);
			map.put("src_method", method);
			map.put("src_request",request);
			map.put("src_response",response);
			map.put("src_daomap", daomap);
			if(methodType!=""){map.put("methodType", methodType);}
			try {
				Class<?> src =Class.forName("com.srczh.manage.Src"+acn);
				Constructor<?> srccon = src.getConstructor(new Class<?>[] {});
				Object osrc = srccon.newInstance(new Object[] {});
				Method srcmds = src.getDeclaredMethod("init", new Class[]{Map.class});
				srcmds.setAccessible(true);
				srcmds.invoke(osrc, new Object[]{map});
//				if(map.get("includeid")!=null) {
//					request.setAttribute("includeid", map.get("includeid"));
//				}
			} catch (Exception e) {
				e.printStackTrace();
				Slog.error(profail+": "+e.getMessage()+"");
			}
		}
	}
	


	/**初始化Action***/
	public static String changeAction(String action){
		if(action==null ||action=="") return "";
		action = action.replace("/", "");
		String up = action.substring(0,1);
		action = action.substring(1,action.length());
		return up.toUpperCase()+action.toLowerCase();
	}

}
