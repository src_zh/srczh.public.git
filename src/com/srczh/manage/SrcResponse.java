package com.srczh.manage;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class SrcResponse {
	

	    private HttpServletResponse _getHttpServletResponse() {
	        try {
				return (HttpServletResponse) super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return null;
	    }

	    /**
	     * The default behavior of this method is to call addCookie(Cookie cookie)
	     * on the wrapped response object.
	     */
	    
	    public void addCookie(Cookie cookie) {
	        this._getHttpServletResponse().addCookie(cookie);
	    }

	    /**
	     * The default behavior of this method is to call containsHeader(String
	     * name) on the wrapped response object.
	     */
	    
	    public boolean containsHeader(String name) {
	        return this._getHttpServletResponse().containsHeader(name);
	    }

	    /**
	     * The default behavior of this method is to call encodeURL(String url) on
	     * the wrapped response object.
	     */
	    
	    public String encodeURL(String url) {
	        return this._getHttpServletResponse().encodeURL(url);
	    }

	    /**
	     * The default behavior of this method is to return encodeRedirectURL(String
	     * url) on the wrapped response object.
	     */
	    
	    public String encodeRedirectURL(String url) {
	        return this._getHttpServletResponse().encodeRedirectURL(url);
	    }

	    /**
	     * The default behavior of this method is to call encodeUrl(String url) on
	     * the wrapped response object.
	     *
	     * @deprecated As of Version 3.0 of the Java Servlet API
	     */
	    
	    @Deprecated
	    public String encodeUrl(String url) {
	        return this._getHttpServletResponse().encodeUrl(url);
	    }

	    /**
	     * The default behavior of this method is to return encodeRedirectUrl(String
	     * url) on the wrapped response object.
	     *
	     * @deprecated As of Version 3.0 of the Java Servlet API
	     */
	    
	    @Deprecated
	    public String encodeRedirectUrl(String url) {
	        return this._getHttpServletResponse().encodeRedirectUrl(url);
	    }

	    /**
	     * The default behavior of this method is to call sendError(int sc, String
	     * msg) on the wrapped response object.
	     */
	    
	    public void sendError(int sc, String msg) throws IOException {
	        this._getHttpServletResponse().sendError(sc, msg);
	    }

	    /**
	     * The default behavior of this method is to call sendError(int sc) on the
	     * wrapped response object.
	     */
	    
	    public void sendError(int sc) throws IOException {
	        this._getHttpServletResponse().sendError(sc);
	    }

	    /**
	     * The default behavior of this method is to return sendRedirect(String
	     * location) on the wrapped response object.
	     */
	    
	    public void sendRedirect(String location) throws IOException {
	        this._getHttpServletResponse().sendRedirect(location);
	    }

	    /**
	     * The default behavior of this method is to call setDateHeader(String name,
	     * long date) on the wrapped response object.
	     */
	    
	    public void setDateHeader(String name, long date) {
	        this._getHttpServletResponse().setDateHeader(name, date);
	    }

	    /**
	     * The default behavior of this method is to call addDateHeader(String name,
	     * long date) on the wrapped response object.
	     */
	    
	    public void addDateHeader(String name, long date) {
	        this._getHttpServletResponse().addDateHeader(name, date);
	    }

	    /**
	     * The default behavior of this method is to return setHeader(String name,
	     * String value) on the wrapped response object.
	     */
	    
	    public void setHeader(String name, String value) {
	        this._getHttpServletResponse().setHeader(name, value);
	    }

	    /**
	     * The default behavior of this method is to return addHeader(String name,
	     * String value) on the wrapped response object.
	     */
	    
	    public void addHeader(String name, String value) {
	        this._getHttpServletResponse().addHeader(name, value);
	    }

	    /**
	     * The default behavior of this method is to call setIntHeader(String name,
	     * int value) on the wrapped response object.
	     */
	    
	    public void setIntHeader(String name, int value) {
	        this._getHttpServletResponse().setIntHeader(name, value);
	    }

	    /**
	     * The default behavior of this method is to call addIntHeader(String name,
	     * int value) on the wrapped response object.
	     */
	    
	    public void addIntHeader(String name, int value) {
	        this._getHttpServletResponse().addIntHeader(name, value);
	    }

	    /**
	     * The default behavior of this method is to call setStatus(int sc) on the
	     * wrapped response object.
	     */
	    
	    public void setStatus(int sc) {
	        this._getHttpServletResponse().setStatus(sc);
	    }

	    /**
	     * The default behavior of this method is to call setStatus(int sc, String
	     * sm) on the wrapped response object.
	     *
	     * @deprecated As of Version 3.0 of the Java Servlet API
	     */
	    
	    @Deprecated
	    public void setStatus(int sc, String sm) {
	        this._getHttpServletResponse().setStatus(sc, sm);
	    }

	    /**
	     * {@inheritDoc}
	     * <p>
	     * The default implementation is to call
	     * {@link HttpServletResponse#getStatus()}
	     * on the wrapped {@link HttpServletResponse}.
	     *
	     * @since Servlet 3.0
	     */
	    
	    public int getStatus() {
	        return this._getHttpServletResponse().getStatus();
	    }

	    /**
	     * {@inheritDoc}
	     * <p>
	     * The default implementation is to call
	     * {@link HttpServletResponse#getHeader(String)}
	     * on the wrapped {@link HttpServletResponse}.
	     *
	     * @since Servlet 3.0
	     */
	    
	    public String getHeader(String name) {
	        return this._getHttpServletResponse().getHeader(name);
	    }

	    /**
	     * {@inheritDoc}
	     * <p>
	     * The default implementation is to call
	     * {@link HttpServletResponse#getHeaders(String)}
	     * on the wrapped {@link HttpServletResponse}.
	     *
	     * @since Servlet 3.0
	     */
	    
	    public Collection<String> getHeaders(String name) {
	        return this._getHttpServletResponse().getHeaders(name);
	    }

	    /**
	     * {@inheritDoc}
	     * <p>
	     * The default implementation is to call
	     * {@link HttpServletResponse#getHeaderNames()}
	     * on the wrapped {@link HttpServletResponse}.
	     *
	     * @since Servlet 3.0
	     */
	    
	    public Collection<String> getHeaderNames() {
	        return this._getHttpServletResponse().getHeaderNames();
	    }

}
