package com.srczh.manage.pool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import com.srczh.manage.SrcConfig;
import com.srczh.manage.interna.Internation;
import com.srczh.manage.util.Slog;

/**
 * 数据源管理器
 * @author zhoujun
 *
 */
public class SrcPoolManage {

	private static SrcPoolManage pts = null;
	private static List<SrcPoolBean> jdbcList=null;
	static String ct=SrcConfig.SRC_LANGUAGE;
	/** 
	 * 加载的驱动器名称集合 
	 */  
	private Set<String> drivers = new HashSet<String>(); 

	/**
	 * 数据库连接池字典
	 * 为每个节点创建一个连接池（可配置多个节点）
	 */
	public static Map<String, Object> pools = new HashMap<>();



	private SrcPoolManage(List<SrcPoolBean> jdbcList) {
		this.jdbcList=jdbcList;
		createPool();
	}
	/**
	 * 获得单例
	 */
	public synchronized static SrcPoolManage getInstance(List listBean) {
		if (pts == null) {
			pts = new SrcPoolManage(listBean);
		}
		return pts;
	}
	/**
	 * 装载JDBC驱动程序，并创建连接池
	 */
	private void createPool() {

		for(int i=0;i<jdbcList.size();i++) {
			SrcPoolBean jdbcbean= (SrcPoolBean) jdbcList.get(i);
			String pool = jdbcbean.getPool();
			String poolname=jdbcbean.getPoolName();
			try {
				if("srcPool".equalsIgnoreCase(pool)){
					//确保同一驱动只能加载一次
					if(!drivers.contains(jdbcbean.getDriver())){
						Class.forName(jdbcbean.getDriver());
						drivers.add(jdbcbean.getDriver());
					}

					//创建连接池
					SrcJdbcPoolInter cpool = SrcJdbcPool.CreateSrcPool(jdbcbean);
					if (cpool != null) {
						jdbcbean.setDs(cpool);

						pools.put(poolname, jdbcbean);
						cpool.checkPool();
						Slog.println(poolname + Internation.out(ct, Internation.poolSucc));
					} else {
						Slog.println(poolname + Internation.out(ct, Internation.poolfail));
					}


				}else if("druid".equalsIgnoreCase(pool)){
					//公共版本不支持

				}else if("tomcatJdbc".equalsIgnoreCase(pool)){
					//公共版本不支持

				}

			} catch (ClassNotFoundException e) {
				Slog.error(Internation.out(ct, Internation.notjdbcDerver) + jdbcbean.getDriver() + "");
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}



	/**
	 * 从指定连接池中获取可用连接
	 * @param poolName要获取连接的连接池名称
	 * @throws SQLException 
	 * @return连接池中的一个可用连接或null
	 */
	public static Connection getConnection(String poolName) throws SQLException {
		SrcPoolBean jb = (SrcPoolBean) pools.get(poolName);
		String pool = jb.getPool();

		if("srcPool".equalsIgnoreCase(pool)){
			SrcJdbcPoolInter spool =  (SrcJdbcPoolInter) jb.getDs();
			//获取当前线程的连接
			return spool.getCurrentConnecton();

		}else if("druid".equalsIgnoreCase(pool)){
			// SrcPool_druid druid = new SrcPool_druid();
			DataSource cn=  (DataSource) jb.getDs();
			return cn.getConnection();

		}else if("tomcatJdbc".equalsIgnoreCase(pool)){
			//SrcPool_tomcat tomcat = new SrcPool_tomcat();
			// return tomcat.getConnection(pools, poolName);
			DataSource cn=  (DataSource) jb.getDs();
			return cn.getConnection();
		}
		return null;
	}


	/**
	 * 取当前数据库类型
	 * @param poolName
	 * @return
	 */
	public static String getDbType(String poolName) {
		String driver="";
		SrcPoolBean jb = (SrcPoolBean) pools.get(poolName);
		driver=jb.getDriver();
		if(driver.indexOf("oracle")>-1) {
			return "oracle";
		}else if(driver.indexOf("mysql")>-1) {
			return "mysql";
		}else if(driver.indexOf("sqlserver")>-1) {
			return "sqlserver";
		}
		return driver;
	}


	/**
	 * 返回当前连接池状态
	 * @param poolName
	 * @return
	 */
	public static int[] getConnState(String poolName) {
		//String pool = SrcConfig.POOL;
		SrcPoolBean jb = (SrcPoolBean) pools.get(poolName);
		String pool = jb.getPool();
		if("srcPool".equalsIgnoreCase(pool)){
			SrcJdbcPoolInter spool = (SrcJdbcPoolInter) jb.getDs();
			//	String state=Internation.out(ct, Internation.free)+":"+spool.getFreeNum()+" "+Internation.out(ct, Internation.activity)+":"+spool.getActiveNum();
			//获取当前线程的连接
			int[] cns=new int[]{spool.getFreeNum(),spool.getActiveNum()};
			return cns;

		}else if("druid".equalsIgnoreCase(pool)){

			//	return new int[]{jb.getInitConns(),jb.getMaxConns()};
		}else if("tomcatJdbc".equalsIgnoreCase(pool)){

			//	return new int[]{jb.getInitConns(),jb.getMaxConns()};

		}
		return new int[]{jb.getInitConns(),jb.getMaxConns()};
	}



	/**
	 * 回收指定连接池的连接
	 * @param poolName连接池名称
	 * @param conn要回收的连接
	 */
	public static void closeConnection(String poolName, Connection conn) throws SQLException {
		String pool = SrcConfig.POOL;
		if("srcPool".equalsIgnoreCase(pool)){
			SrcPoolBean spb = (SrcPoolBean) pools.get(poolName);
			SrcJdbcPoolInter spool =(SrcJdbcPoolInter) spb.getDs();
			if (spool != null) {
				try {
					spool.closeConn(conn);
				} catch (SQLException e) {
					Slog.error("回收"+poolName+"池中的连接失败。");
					throw new SQLException(e);
				}
			}else{
				Slog.error("找不到"+poolName+"连接池，无法回收");
			}
		}
	}


	/**
	 * 重置连接池
	 */
	public static void destroy(String poolName){

		String pool = SrcConfig.POOL;
		if("srcPool".equalsIgnoreCase(pool)){
			SrcPoolBean spb = (SrcPoolBean) pools.get(poolName);
			SrcJdbcPoolInter spool =  (SrcJdbcPoolInter)spb.getDs();
			spool.destroy();
		}
	}

	/**
	 * 关闭所有连接，撤销驱动器的注册
	 */
	public void destroy() {
		String pool = SrcConfig.POOL;
		if("srcPool".equalsIgnoreCase(pool)){
			
			for (Map.Entry<String, Object> poolEntry : pools.entrySet()) {
				SrcPoolBean spb = (SrcPoolBean) poolEntry.getValue();
				SrcJdbcPoolInter spool = (SrcJdbcPoolInter) spb.getDs();
				spool.destroy();
			}
		}
		Slog.info(Internation.out(ct, Internation.closeConn)+" All");
	}

	public Boolean testConn(String poolName, String sql) {
		String pool = SrcConfig.POOL;
		if("srcPool".equalsIgnoreCase(pool)){
			SrcPoolBean spb = (SrcPoolBean) pools.get(poolName);
			SrcJdbcPoolInter spool =  (SrcJdbcPoolInter) spb.getDs();
			return spool.testConn(spool.getCurrentConnecton(), sql);
		}
		return true;
	}
}
