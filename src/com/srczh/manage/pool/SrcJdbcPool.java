package com.srczh.manage.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.srczh.manage.SrcConfig;
import com.srczh.manage.interna.Internation;
import com.srczh.manage.util.Slog;

/**
 * 连接池实现类
 * @author zhoujun
 *
 */
class SrcJdbcPool implements SrcJdbcPoolInter {

	//i18n
	static String ct=SrcConfig.SRC_LANGUAGE;
	static String dbbing=Internation.out(ct, Internation.dbbing);
	String dbpoolerror=Internation.out(ct, Internation.dbpoolerror);
	String dbpoolcolsefaild=Internation.out(ct, Internation.dbpoolcolsefaild);
	
	private SrcPoolBean poolBean=null;

	//连接池可用状态
	private Boolean isActive = true;

	// 空闲连接池 
	private Hashtable<Integer,Connection> freeConnections = new Hashtable<Integer,Connection>();  
	// 活动连接池。活动连接数 <= 允许最大连接数(maxConnections)
	private Hashtable<Integer,Connection> activeConnections = new Hashtable<Integer,Connection>();
	//当前线程获得的连接
	private ThreadLocal<Connection> currentConnection= new ThreadLocal<Connection>();


	private SrcJdbcPool(){
		super();
	}

	public static SrcJdbcPool CreateSrcPool(SrcPoolBean poolBean) {
		SrcJdbcPool srcpool=new SrcJdbcPool();
		srcpool.poolBean = poolBean;

		//始使化时根据配置中的初始连接数创建指定数量的连接
		for (int i = 0; i < srcpool.poolBean.getInitConns(); i++) {
			try {
				Connection conn = srcpool.NewConnection();
				srcpool.freeConnections.put(conn.hashCode(), conn);
			} catch (SQLException | ClassNotFoundException e) {
				Slog.error(dbbing+"  " + e.getMessage());   
				return null;
			}
		}

		srcpool.isActive = true;
		return srcpool;
	}



	/**
	 * 检测连接是否有效
	 * @param 数据库连接对象
	 * @return Boolean
	 */
	private Boolean isValidConnection(Connection conn) throws SQLException{
		try {
			if(conn==null || conn.isClosed()){
			//	this.freeConnections.remove(conn);
				return false;
			}
		} catch (SQLException e) {
			throw new SQLException(e);
		}
		return true;
	}

	/**
	 * 创建一个新的连接
	 * @return 数据库连接对象
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private Connection NewConnection() throws ClassNotFoundException,SQLException {
//		System.out.println("创建空闲连接");
		Connection conn = null;
		try {
			if (this.poolBean != null) {
				conn = DriverManager.getConnection(this.poolBean.getDbUrl(),
				this.poolBean.getDbUname(),this.poolBean.getDbPasswd());
			}
		} catch (SQLException e) {
			throw new SQLException(e);
		}
		return conn;
	}


	private synchronized Connection getConnection() {
		Connection conn = null;
		int acnum=this.getActiveNum();
		// 分支1：当前使用的连接没有达到最大连接数  
		if (acnum < this.poolBean.getMaxConns()) {
			// 基本点3、在连接池没有达到最大连接数之前，如果有可用的空闲连接就直接使用空闲连接，如果没有，就创建新的连接。
			if (this.getFreeNum() > 0) {
				// 分支1.1：如果空闲池中有连接，就从空闲池中直接获取
//				Slog.info(Internation.out(ct, Internation.free)+"："+this.getFreeNum());
//				System.out.println("kaishi:"+freeConnections.size());
//				conn = this.freeConnections.pollFirst();
				
				  int key = (int) this.freeConnections.keySet().toArray()[0];
				  conn = this.freeConnections.get(key);
	              this.freeConnections.remove(key);
//				System.out.println("jieshu:"+freeConnections.size());

				//连接闲置久了也会超时 因此空闲池中的有效连接会越来越少，需要另一个进程进行扫描监测，不断保持一定数量的可用连接。
				//在下面定义了checkFreepools的TimerTask类，在checkPool()方法中进行调用。

				//由于数据库连接闲置久了会超时关闭因此需要连接池采用机制保证每次请求的连接都是有效可用的。
				try {
					if(this.isValidConnection(conn)){
			//			System.out.println("空闲连接可用:"+this.freeConnections.size());
						this.activeConnections.put(conn.hashCode(), conn);
						currentConnection.set(conn);
					}else{
			//			System.out.println("空闲连接不可用，重入");
						conn = getConnection();//同步方法是可重入锁
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				// 如果空闲池中无可用连接就创建新的连接
				try {
		//			System.out.println("空闲连接不可用，新建");
					conn = this.NewConnection();
					//this.activeConnections.add(conn);
					//currentConnection.set(conn);
					//Slog.info(Internation.out(ct, Internation.free)+"：0，"+Internation.out(ct, Internation.estab)+"：1");
					
					this.activeConnections.put(conn.hashCode(), conn);
					
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				}
			}
		// 当前已到达最大连接数  
		} else {
			// 基本点4、当连接池中的活动连接数达到最大连接数，新的请求进入等待状态，直到有连接被释放。
			long startTime = System.currentTimeMillis();
			Slog.info(Internation.out(ct, Internation.activity)+"："+acnum+"，"+Internation.out(ct, Internation.wait)+"："+this.poolBean.getRconnMiss());

			//进入等待状态。等待被notify(),notifyALL()唤醒或者超时自动苏醒  
			try{
				this.wait(this.poolBean.getRconnMiss());  
			}catch(InterruptedException e) {  
				e.printStackTrace();
			}

			//若线程超时前被唤醒并成功获取连接，就不会走到return null。
			//若线程超时前没有获取连接，则返回null。
			//如果timeout设置为0，就无限重连。
//			if(this.propertyBean.getTimeout()!=0){
            if(System.currentTimeMillis() - startTime > this.poolBean.getTimeout()){  
                return null;  
			}
            conn = this.getConnection();
		}
		return conn;
	}


	@Override
	public synchronized Connection getCurrentConnecton() {
		Connection conn=currentConnection.get();
		try {
			if(! isValidConnection(conn)){
				conn=this.getConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}


	@Override
	public synchronized void closeConn(Connection conn) throws SQLException {

		//Slog.info(Thread.currentThread().getName()+" "+ Internation.out(ct, Internation.closeConn)+"：activeConnections.remove:"+conn);
//		Slog.println("当前连接被回收:"+isValidConnection(conn));
		this.activeConnections.remove(conn.hashCode());
		this.currentConnection.remove();
		//活动连接池删除的连接，相应的加到空闲连接池中
//		conn.close();
		try {
			if(isValidConnection(conn)){
				freeConnections.put(conn.hashCode(), conn);
			}else{
				 Connection newconn = this.NewConnection();
	             freeConnections.put(newconn.hashCode(), newconn);
			}
		} catch ( SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		//唤醒getConnection()中等待的线程
		this.notifyAll();
	}

	@Override
	public synchronized void destroy() {
		for(int key:this.freeConnections.keySet()){
            Connection conn = this.freeConnections.get(key);
            try {
                if (this.isValidConnection(conn)) { 
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        for(int key:this.activeConnections.keySet()){
            Connection conn = this.activeConnections.get(key);
            try {
                if (this.isValidConnection(conn)) { 
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        this.isActive = false;
        this.freeConnections.clear();
        this.activeConnections.clear();
	}

	@Override
	public boolean isActive() {
		return this.isActive;
	}


	@Override
	public void checkPool() {

		final String poolname=this.poolBean.getPoolName();
		final String free=Internation.out(ct, Internation.free);
		final String acti=Internation.out(ct, Internation.activity);

		ScheduledExecutorService ses=Executors.newScheduledThreadPool(2);

//		//功能一：开启一个定时器线程输出状态
//		ses.scheduleAtFixedRate(new TimerTask() {
//			@Override
//			public void run() {
//				Slog.println(poolname +" "+acti+"："+getActiveNum()+" "+free+"："+getFreeNum());  
//			}
//		}, 1, 1, TimeUnit.MINUTES);

		//功能二：开启一个定时器线程，监测并维持空闲池中的最小连接数
		ses.scheduleAtFixedRate(new checkFreepools(this), 1, 3, TimeUnit.MINUTES);
	}

	@Override
	public synchronized int getActiveNum() {
		return this.activeConnections.size();
	}

	@Override
	public synchronized int getFreeNum() {
		return this.freeConnections.size();
	}

	//基本点6、连接池内部要保证指定最小连接数量的空闲连接
	class checkFreepools extends TimerTask {
		private SrcJdbcPool conpool = null;
		String creart=Internation.out(ct, Internation.estab);
		public checkFreepools(SrcJdbcPool cpool) {
			this.conpool = cpool;
		}

		@Override
		public void run() {
			if (this.conpool != null && this.conpool.isActive()) {
			//	System.out.println("	空闲连接数:"+conpool.getFreeNum()+"活动连接数:"+conpool.getActiveNum()+"  最小维持连接数:"+conpool.poolBean.getMinConns());
				//空闲加上活动
				int poolstotalnum = conpool.getFreeNum() + conpool.getActiveNum();
	
	            int subnum = conpool.poolBean.getMinConns()  - poolstotalnum;
	             if (subnum > 0) {
	             //       System.out.println(conpool.poolBean.getPoolName()+ "维持空闲池中的最小连接数，补充" + subnum + "个连接");
	                    for (int i = 0; i < subnum; i++) {
	                        try {
	                            Connection newconn = conpool.NewConnection();
	                            conpool.freeConnections.put(newconn.hashCode(), newconn);
	                        } catch (ClassNotFoundException | SQLException e) {
	                            e.printStackTrace();
	                        }
	                    }
	                }
				
				
				
				
				/*	//最小连接- 所有连接
			//	int subnum = conpool.poolBean.getMinConns() - poolstotalnum;

				//空闲连接 小于等于 最小连接
				if (conpool.getFreeNum() <= conpool.poolBean.getMinConns()) {
					
					Slog.println(conpool.poolBean.getPoolName()+ " "+creart+" ：" + conpool.poolBean.getMinConns() + "");
					for (int i = 0; i < conpool.poolBean.getInAddConns(); i++) {
						try {
							conpool.freeConnections.add(conpool.NewConnection());
						} catch (ClassNotFoundException | SQLException e) {
							e.printStackTrace();
						}
					}
				}
				
				//活动连接大于最大值 清除实行
				if(poolstotalnum >= conpool.poolBean.getMaxConns()){
					int acnum=conpool.activeConnections.size();
					for(int i=0;i<=acnum;i++){
						try {
							conpool.closeConn(this.conpool.activeConnections.get(i));
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}*/
			}
		}
	}

	@Override
	public boolean testConn(Connection conn,String sql) {
		PreparedStatement pstmt;              
		try {
			pstmt = (PreparedStatement)conn.prepareStatement(sql);
			pstmt.executeQuery(); 
		} catch (SQLException e) {
			Slog.error(""+this.poolBean.getPoolName()+" 连接测试失败："+e.getMessage());
			e.printStackTrace();
			return false;
		} 
		return true;
	}

}
