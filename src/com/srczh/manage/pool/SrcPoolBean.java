package com.srczh.manage.pool;


public class SrcPoolBean {
	
	//连接池
	private String pool;
	//数据源名称
    private String poolName;
    
    private Object ds;

	//数据连接驱动
    private String driver;
    //数据连接地址串
    private String dbUrl;
    //数据连接用户名
    private String dbUname;
    //数据连接密码
    private String dbPasswd;
    
    private String testSql;
    
    //连接池最大连接数
    private int maxConns ;
    //连接池最小连接数
    private int minConns;
    //连接池初始连接数
    private int initConns;
    //追加池数量
    private int inAddConns;
    //重连间隔毫秒
    private int rconnMiss ;
    
    //连接超时时间 ，单位毫秒，0永不超时
    private int timeout ;
    
    //druid使用 心跳检查
    private boolean  testWhileIdle;
	//#空闲连接关闭时间
    private int  timeBERunsMillis;
	//#连接在池中最小生存的时间
    private int  minEITimeMillis;


	public boolean isTestWhileIdle() {
		return testWhileIdle;
	}


	public void setTestWhileIdle(boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}


	public int getTimeBERunsMillis() {
		return timeBERunsMillis;
	}


	/**
	 * 空闲连接关闭时间
	 * @param timeBERunsMillis
	 */
	public void setTimeBERunsMillis(int timeBERunsMillis) {
		this.timeBERunsMillis = timeBERunsMillis;
	}


	/**
	 * 连接在池中最小生存的时间
	 * @return
	 */
	public int getMinEITimeMillis() {
		return minEITimeMillis;
	}


	public void setMinEITimeMillis(int minEITimeMillis) {
		this.minEITimeMillis = minEITimeMillis;
	}


	//构造方法
    public SrcPoolBean(){
        super();
    }


    public String getPool() {
		return pool;
	}


	public void setPool(String pool) {
		this.pool = pool;
	}


	/**
     * 获取数据源名称
     * @return
     */
    public String getPoolName() {
		return poolName;
	}
    /**
     * 设置数据源名称
     * @param poolName
     */
	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}


	public String getDriver() {
		return driver;
	}


	public void setDriver(String driver) {
		this.driver = driver;
	}


	public String getDbUrl() {
		return dbUrl;
	}


	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}


	public String getDbUname() {
		return dbUname;
	}


	public void setDbUname(String dbUname) {
		this.dbUname = dbUname;
	}


	public String getDbPasswd() {
		return dbPasswd;
	}


	public void setDbPasswd(String dbPasswd) {
		this.dbPasswd = dbPasswd;
	}


	/**
	 * 最大连接数
	 * @return
	 */
	public int getMaxConns() {
		return maxConns;
	}


	public void setMaxConns(int maxConns) {
		this.maxConns = maxConns;
	}

	/**
	 * 最小连接数
	 * @param minConns
	 */
	public int getMinConns() {
		return minConns;
	}



	public void setMinConns(int minConns) {
		this.minConns = minConns;
	}


	/**
	 * 连接池初始连接数
	 * @return
	 */
	public int getInitConns() {
		return initConns;
	}


	public String getTestSql() {
		return testSql;
	}


	public void setTestSql(String testSql) {
		this.testSql = testSql;
	}


	public Object getDs() {
		return ds;
	}


	public void setDs(Object ds) {
		this.ds = ds;
	}


	public void setInitConns(int initConns) {
		this.initConns = initConns;
	}


	/**
	 * 每增加链接数
	 * @return
	 */
	public int getInAddConns() {
		return inAddConns;
	}


	public void setInAddConns(int inAddConns) {
		this.inAddConns = inAddConns;
	}


	public int getRconnMiss() {
		return rconnMiss;
	}


	public void setRconnMiss(int rconnMiss) {
		this.rconnMiss = rconnMiss;
	}


	/**
	 * 超时时间
	 * @return
	 */
	public int getTimeout() {
		return timeout;
	}


	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
    

}
