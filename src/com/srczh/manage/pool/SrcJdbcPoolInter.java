package com.srczh.manage.pool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 连接池接口
 * @author zhoujun
 *
 */
public interface SrcJdbcPoolInter {

    /**
     * 获得当前线程的连接库连接,如果为空则获取一个空闲的连接，如果等待超时则返回null
     * @return 数据库连接对象
     */
    public Connection getCurrentConnecton();

    /**
     * 释放当前线程数据库连接
     * @param conn 数据库连接对象
     * @throws SQLException
     */
    public void closeConn(Connection conn) throws SQLException;

    /**
     * 销毁清空当前连接池
     */
    public void destroy();

    /**
     * 连接池可用状态
     * @return 连接池是否可用
     */
    public boolean isActive();

    /**
     * 定时器，检查连接池
     */
    public void checkPool();

    /**
     * 获取线程池活动连接数
     * @return 线程池活动连接数
     */
    public int getActiveNum();

    /**
     * 获取线程池空闲连接数
     * @return 线程池空闲连接数
     */
    public int getFreeNum();
    /**
     * 测试连接是否真实可用
     * @return
     */
    public boolean testConn(Connection conn,String sql);

}
