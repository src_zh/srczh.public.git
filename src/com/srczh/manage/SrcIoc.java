package com.srczh.manage;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;


public class SrcIoc {
	
	/**
	 * 实例化Dao
	 * @param daoNames
	 * @param pathFile
	 */
	static void scanDao(List<String> daoNames,String pathFile,String bao) {
		File file = new File(pathFile);
		String fileList[] = file.list()==null?new String[] {}:file.list();
		for (String path : fileList) {
			File eachFile = new File(pathFile +File.separatorChar+ path);
			if (eachFile.isDirectory()) {
				scanDao(daoNames,eachFile.getPath(),bao);
			} else {
				String fname=eachFile.getName();
				if(fname.indexOf(".")>0){
					String ftype=fname.substring(fname.indexOf(".")+1);
					if("class".equals(ftype)){
						String sf=pathFile +File.separatorChar +fname;
						sf=sf.replace("/", File.separatorChar+"");
						String tbao=bao.replace(".", File.separatorChar+"");
						sf=sf.substring(sf.indexOf(tbao));
						sf=sf.replace(File.separatorChar+"", ".");
						daoNames.add(sf);
					}
				}
			}
		}
	}
	
	/**
	 * 实例化控制器
	 * @param actionNames
	 * @param control
	 * @param pathFile
	 */
	static void scanAction(List<String> actionNames,String control,String pathFile,String bao) {
		File file = new File(pathFile);
		String fileList[] = file.list()==null?new String[] {}:file.list();
		for (String path : fileList) {
			File eachFile = new File(pathFile +File.separatorChar+ path);
			//是目录
			if (eachFile.isDirectory()) {
				scanAction(actionNames,control,eachFile.getPath(),bao);
			} else {
				String fname=eachFile.getName();
				actionNames.add(fname);
				if(fname.indexOf(".")>0){
					//String ftype=fname.substring(fname.indexOf(".")+1);
					if(fname.toLowerCase().indexOf(control+".class")>0){
						String sf=pathFile +File.separatorChar +fname;
						sf=sf.replace("/", File.separatorChar+"");
						String tbao=bao.replace(".", File.separatorChar+"");
						sf=sf.substring(sf.indexOf(tbao));
						sf=sf.replace(File.separatorChar+"", ".");
					}
				}
			}
		}
	}
	
	
	static String replaceTo(String path) {
		return path.replaceAll("\\.", "/");
	}
	
	/**
	 * 得到所有注入对象
	 * @param daoImplMap
	 * @param daoNames
	 * @throws Exception
	 */
	static void daoInstance(Map daoImplMap,List<String> daoNames) throws Exception {
		if (daoNames.size() <= 0) {
			return;
		}
		for (String daoName : daoNames) {
			Class<?> cName = Class.forName(daoName.replace(".class", "").trim());
			String name=cName.getName();
			String type=cName.getTypeName();
			String key=cName.getSimpleName();
			
			boolean itef=Modifier.isInterface(cName.getModifiers());
			if(!itef){
				Object instance = cName.newInstance();
				daoImplMap.put(key, instance);
			}else{
				//daoMap.put(key, name);
			}
		}
	}

}
