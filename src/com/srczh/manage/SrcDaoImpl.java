package com.srczh.manage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
//import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.CLOB;

import com.srczh.manage.interna.Internation;
import com.srczh.manage.jdbc.SrcJdbcBean;
import com.srczh.manage.pool.SrcPoolManage;
import com.srczh.manage.jdbc.SrcJdbc;
import com.srczh.manage.util.Slog;
import com.srczh.manage.util.Page;
import com.srczh.manage.util.SrcCache;
import com.srczh.manage.util.SrcJson;
import com.srczh.manage.util.SrcUtil;
import com.srczh.util.SrcSqlUtil;

/**
 * SrcDaoImpl 实现接口SrcDao
 * @author zhoujun
 */
@SuppressWarnings("all")
public class SrcDaoImpl extends SrcDao {
	

	protected SrcDaoImpl() {
		super();
	}

	protected class SrcDsaoImpl{}
	private static ThreadLocal<Connection> connection=new ThreadLocal<>();
	private static ThreadLocal<PreparedStatement> prepStament=new ThreadLocal<>();
	
	/** 
	 * 创建数据库连接对象  */  
//	private static Connection connection = null;  
	/** 
	 * 创建PreparedStatement对象 */  
//	private PreparedStatement preparedStatement = null;  
	/** 
	 * 创建CallableStatement对象  */  
//	private CallableStatement callableStatement = null;  
	/**
	 * 结果集**/
//	private ResultSet resultSet = null;  
	
	String pamtxt=	Internation.out(SrcConfig.SRC_LANGUAGE, Internation.parameter);
	String plzx=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.execution);
	String rest=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.rest);
	String execfail=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.execfail);
	String returntxt=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.retuntxt);
	
	String gain=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.gain);
	String use=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.use);
	String cacheA=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.cacheA);
	String cacheB=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.cacheB);

	/**
	 * 按预定sql，参数 新增数据
	 * connName指定数据源
	 */
	public Object insert(String sql,Object[] params,String connName)throws SQLException {
		return this.execute(sql, params,connName);
	}
	/**
	 * 按预定sql，数组 新增数据
	 */
	public Object insert(String sql,Object[] params)throws SQLException {
		return this.execute(sql, params, SrcConfig.SRCDFPOOL);
	}
	/**
	 * 按预定sql，Map 新增数据
	 */
	public Object insert(String sql,Map<String,Object> map)throws SQLException {
		Map imap=SrcSqlUtil.initInsertSql(map,"map");
		Object[] vals=(Object[]) imap.get("vals");
		return this.execute(sql, vals, SrcConfig.SRCDFPOOL);
	}
	
	/**
	 * 按实体类新增
	 */
	public Object insert(String sql,Object obj)throws SQLException{
		Map map =SrcUtil.objToMap(obj);
		return insert(sql,map);
	}
	public Object insert(SrcJdbc jdbc,Object obj)throws SQLException{
		Map map =SrcUtil.objToMap(obj);
		return insert(jdbc,map);
	}

	/**
	 * json必须为表结构列的对象数据
	 */
	public Object insert(SrcJdbc jdbc,SrcJson json)throws SQLException {
		Map jsMap=json.getMap();
		return insert(jdbc,jsMap);
	}
	
	
	/**
	 * 按SrcJdbc对象新增map中一条数据
	 * map必须为表结构列的对象数据
	 */
	public Object insert(SrcJdbc jdbc,Map<String,Object> map) throws SQLException {
		String sql="";
		Iterator entries = map.entrySet().iterator();
		Map.Entry entry;
		String pname = "";
		String pval="";
		List<Object> vals = new ArrayList<Object>();
		Object[] pam=null;
		String connName="";
		//实例化ysql 语句参数
		if(jdbc.Ysql!=null&&!"".equals(jdbc.Ysql)) {
			connName=this.getConnNameBySqlId(jdbc);
			sql=getSrcSQL(jdbc);
			String tsql=sql.toUpperCase();
			String eobj=tsql.substring(tsql.indexOf(" INTO ")+5,tsql.indexOf("(")).trim();
			if(jdbc.getSqlPrame()==null) {
				Map pmap = this.initYsqlPram(sql, map);
				vals=(List) pmap.get("wls");
				pam=vals.toArray();
			}else {
				pam=jdbc.getSqlPrame();
			}
			SrcCache.remove(eobj.trim());
		}else {
			
			while(entries.hasNext()) {
				entry = (Map.Entry) entries.next();
				String colum=(String) entry.getKey();
				String val=entry.getValue()==null?"":entry.getValue().toString();
				
				if(val.indexOf(".")>0 ){ //有Oracle主键 
					String tkeys=val.substring(val.indexOf("."));
					if(tkeys==".NEXTVAL"||".NEXTVAL".equalsIgnoreCase(tkeys)) {
						if(pname==""){pname=colum;}else{pname=colum+","+pname;}
						if(pval==""){pval=val;}else{pval=val+","+pval;}
					}else {
						if(pname==""){pname=colum;}else{pname=pname+","+colum;}
						if(pval==""){pval="?";}else{pval=pval+",?";}
						vals.add(entry.getValue());
					}
				}else {
					if(pname==""){pname=colum;}else{pname=pname+","+colum;}
					if(pval==""){pval="?";}else{pval=pval+",?";}
					vals.add(entry.getValue());
				}
			}
			
			connName=this.getconnPoolName(jdbc);
			//新增时包含where条件
			if(jdbc.Where!=null && !"".equals(jdbc.Where.trim())) {
				String [] ws=jdbc.Where.split(",");
				sql = "insert into "+this.changeObject(jdbc.Object)+"("+pname+") select ";
				String vs="",whe="";
				for(int i=0;i<vals.size();i++) {
					if(vals.get(i) instanceof String) {
						if(vs=="") {vs="'"+vals.get(i)+"'";}
						else {vs = vs+ ",'"+vals.get(i)+"'";}
					}
				}
				for(int i=0;i<ws.length;i++) {
					if(whe=="") {whe= ws[i]+"='"+map.get(ws[i])+"'";}
					else {whe = whe+ " and "+ws[i]+"='"+map.get(ws[i])+"'";}
				}
				sql =sql+ pval+" from dual where not exists(select '1' from "+jdbc.Object+" where "+whe+")";
				pam= vals.toArray();
			}else {
				sql = "insert into "+this.changeObject(jdbc.Object)+"("+pname+")values("+pval+")";
				if(jdbc.getReturn()!=null && !"".equals(jdbc.getReturn())) {
					sql=sql+ " returning "+jdbc.getReturn()+" into ? ";
				}
				pam= vals.toArray();
			}
			SrcCache.remove(jdbc.Object);
		}
		return this.execute(sql,pam,connName);

	}
	


	
	/***
	 * 批量新增 
	 * 传入list数据中必须为map或javabean
	 */
	public Object insert(SrcJdbc jdbc,List list) {
		if(list==null||list.size()<=0){
			return 0;
		}
		
		int count=list.size();
		//预sql
		if(jdbc.getYsql()!=null &&!"".equals(jdbc.getYsql())){
			List cols=new ArrayList();
			String connName=getConnNameBySqlId(jdbc);
			
			String sql = getSrcSQL(jdbc).toUpperCase();
			String obj=sql.substring(sql.indexOf(" INTO ")+4,sql.indexOf("(")).trim();
			String into = sql.substring(sql.indexOf("(")+1,sql.indexOf(")")).trim();
			String ival = sql.substring(sql.lastIndexOf("(")+1,sql.lastIndexOf(")")).trim();
			String[] intoi=into.split(",");
			String[] ivali=ival.split(",");
			
			for(int i=0;i<ivali.length;i++) {
				String vl=ivali[i].trim();
				//是问号 封装起来
				if("?".equals(vl)) {
					cols.add(intoi[i]);
				}
			}
			Slog.println("	"+plzx+"SQL："+sql);
			try {
				connection.set(getConnection(connName));
				prepStament.set(connection.get().prepareStatement(sql.toString()));
				connection.get().setAutoCommit(false);//使用非自动提交
			
				//循环list 注入值
				for (int i = 0; i < list.size(); i++) {
					Object ob= list.get(i);
					//ConcurrentHashMap值
					if(ob instanceof Map) {
						Map imap = (Map) list.get(i);
						String das="";
						for(int j=0;j<cols.size();j++){
							Object val =  imap.get(cols.get(j));
							prepStament.get().setObject((j+1),val);
							if(val!=null) {
								das += ((val+"").length()>100?(val+"").substring(0,100)+"...":val+"")+",";
							}else {
								das +=",";
							}
						}
						Slog.println("	"+pamtxt+"-"+i+" = "+das);
					}
					//非map
					else {
						Class cls=list.get(i).getClass();
						Object vobj=list.get(i);
						String das="";
						for(int j=0;j<cols.size();j++){
							String colum= cols.get(j).toString();
						    String upperChar = colum.substring(0,1).toUpperCase();
						    String anotherStr = colum.substring(1);
						    String methodName = "get" + upperChar + anotherStr;
						    Method method = cls.getMethod(methodName, new Class[]{});
						    method.setAccessible(true);
						    Object val = method.invoke(vobj, new Object[]{});
						    prepStament.get().setObject((j+1), val);
							if(val!=null) {
								das += ((val+"").length()>100?(val+"").substring(0,100)+"...":val+"")+",";
							}else {
								das +=",";
							}
						}
						Slog.println("	"+pamtxt+"-"+i+" = "+das);
					}
					prepStament.get().addBatch();
					//500数量一提交
					if(i>0&&i%500==0){
						connection.get().commit();
					}
				}
				prepStament.get().executeBatch();
				connection.get().commit();
				//清除缓存
				SrcCache.remove(obj.toUpperCase());
			} catch (Exception e) {
				e.printStackTrace();
				try {
					connection.get().rollback();
				} catch (SQLException e1) {
				}
				count=0;
			}finally{
				closeAll(connName);
			}
		}
	
		//非预sql
		else {
		
			Map<String ,Object> map =new HashMap<>();
			Object obj=list.get(0);
			List<Object> cols = new ArrayList<Object>();
			
			if(obj instanceof Map) {
				map=(Map<String, Object>) obj;
			
				Iterator entries = map.entrySet().iterator();
				Map.Entry entry;
				String pname = "";
				String pval="";
				while(entries.hasNext()) {
					entry = (Map.Entry) entries.next();
					String colum=(String) entry.getKey();
					String val=entry.getValue().toString();
					if(val.indexOf(".")>0 ){ //有Oracle主键 
						String tkeys=val.substring(val.indexOf("."));
						if(tkeys==".NEXTVAL"||".NEXTVAL".equalsIgnoreCase(tkeys)) {
							if(pname==""){pname=colum;}else{pname=colum+","+pname;}
							if(pval==""){pval=val;}else{pval=val+","+pval;}
						}else {
							if(pname==""){pname=colum;}else{pname=pname+","+colum;}
							if(pval==""){pval="?";}else{pval=pval+",?";}
							cols.add( colum); //装入列名，批量插入
						}
					}else {
						if(pname==""){pname=colum;}else{pname=pname+","+colum;}
						if(pval==""){pval="?";}else{pval=pval+",?";}
						cols.add( colum);
					}
				}
				
				String object=changeObject(jdbc.Object);
				String sql = "insert into "+object+"("+pname+")values("+pval+")";
				Slog.println("	"+plzx+"SQL："+sql);
				try {
					String connName=this.getconnPoolName(jdbc);
					connection.set(getConnection(connName));
					prepStament.set(connection.get().prepareStatement(sql.toString()));
					connection.get().setAutoCommit(false);//使用非自动提交
				
				for (int i = 0; i < list.size(); i++) {
					Map<String ,Object> imap = (Map<String, Object>) list.get(i);
					for(int j=0;j<cols.size();j++){
						String fd=cols.get(i).toString();
						prepStament.get().setObject((j+1),imap.get(fd.trim()));
					}
					prepStament.get().addBatch();
					//500数量一提交
					if(i>0&&i%500==0){
						connection.get().commit();
					}
				}
				
				prepStament.get().executeBatch();
				connection.get().commit();
				} catch (SQLException e) {
					e.printStackTrace();
					try {
						connection.get().rollback();
					} catch (SQLException e1) {
					}
					count=0;
				}
				
				
			//非map 为javabean 
			}else  {
				 Class clz = obj.getClass();
				 Field[] fds =null;
				 try {
					fds =  clz.getDeclaredFields();
					String pname = "";
					String pval="";
					for(int i=0;i<fds.length;i++) {
						Field fd=fds[i];
						String colum=fd.getName();
						
					    String upperChar = colum.substring(0,1).toUpperCase();
					    String anotherStr = colum.substring(1);
					    String methodName = "get" + upperChar + anotherStr;
					    Method method = clz.getMethod(methodName, new Class[]{});
					    method.setAccessible(true);
					    Object val = method.invoke(obj, new Object[]{});
						
						if((val+"").indexOf(".")>0 ){ //有Oracle主键 
							String tkeys=(val+"").substring((val+"").indexOf("."));
							if(tkeys==".NEXTVAL"||".NEXTVAL".equalsIgnoreCase(tkeys)) {
								if(pname==""){pname=colum;}else{pname=colum+","+pname;}
								if(pval==""){pval=val+"";}else{pval=val+","+pval;}
							}else {
								if(pname==""){pname=colum;}else{pname=pname+","+colum;}
								if(pval==""){pval="?";}else{pval=pval+",?";}
								cols.add(colum);
							}
						}else {
							if(pname==""){pname=colum+"";}else{pname=pname+","+colum;}
							if(pval==""){pval="?";}else{pval=pval+",?";}
							cols.add( colum);
						}
					}
					
					String sql = "insert into "+changeObject(jdbc.Object)+"("+pname+")values("+pval+")";
					Slog.println("	"+plzx+"SQL："+sql);
					String connName=this.getconnPoolName(jdbc);
					try {
						Connection conn=getConnection(connName);
						prepStament.set(conn.prepareStatement(sql.toString()));
						connection.get().setAutoCommit(false);//使用非自动提交
						
					for (int i = 0; i < list.size(); i++) {
						obj=list.get(i);
						Class cls = obj.getClass();
	
						for(int j=0;j<cols.size();j++){
							String colum=cols.get(j).toString();
							
						    String upperChar = colum.substring(0,1).toUpperCase();
						    String anotherStr = colum.substring(1);
						    String methodName = "get" + upperChar + anotherStr;
						    Method method = cls.getMethod(methodName, new Class[]{});
						    method.setAccessible(true);
						    Object val = method.invoke(obj, new Object[]{});
						    
						    prepStament.get().setObject((j+1),val);
						}
						
						prepStament.get().addBatch();
						//500数量一提交
						if(i>0&&i%500==0){
							connection.get().commit();
						}
					}
					
					prepStament.get().executeBatch();
					connection.get().commit();
					} catch (SQLException e) {
						e.printStackTrace();
						try {
							connection.get().rollback();
						} catch (SQLException e1) {
						}
						count=0;
					}finally{
						closeAll(connName);
					}
				} catch (Exception e) {
					e.printStackTrace();
					count=0;
				}
			}
			
			//清除缓存
			SrcCache.remove(changeObject(jdbc.Object));
		}
		return count;
	}
	
	

	/***
	 * 组织事务中的sql数据
	 * @param jdbc
	 * @return
	 */
	private List initBase(SrcJdbc jdbc){
		Map map =jdbc.getMap();
		if(jdbc.Object==null||"".equals(jdbc.Object)){
			return null;
		}
		List ls=new ArrayList();
		if(map!=null){
			Iterator entries = map.entrySet().iterator();
			Map.Entry entry;
			String pname = "";
			String pval="";
			List<Object> vals = new ArrayList<Object>();
			//执行新增
			if(jdbc.DDL==1){
				while(entries.hasNext()) {
					entry = (Map.Entry) entries.next();
					String colum=(String) entry.getKey();
					String val=entry.getValue()==null?"":entry.getValue().toString();
					if(val.indexOf(".")>0 ){ //有Oracle主键 
						String tkeys=val.substring(val.indexOf("."));
						if(tkeys==".NEXTVAL"||".NEXTVAL".equalsIgnoreCase(tkeys)) {
							if(pname==""){pname=colum;}else{pname=colum+","+pname;}
							if(pval==""){pval=val;}else{pval=val+","+pval;}
						}else {
							if(pname==""){pname=colum;}else{pname=pname+","+colum;}
							if(pval==""){pval="?";}else{pval=pval+",?";}
							vals.add( entry.getValue());
						}
					}else {
						if(pname==""){pname=colum;}else{pname=pname+","+colum;}
						if(pval==""){pval="?";}else{pval=pval+",?";}
						vals.add( entry.getValue());
					}
				}

				String sql = "insert into "+changeObject(jdbc.Object)+"("+pname+")values("+pval+")";
				ls.add(sql);
				ls.add(vals);
				
			//如果是执行修改
			}else if(jdbc.DDL==2){
				List<Object> setvals = new ArrayList<Object>();

				String sets="";
				if(jdbc.Set==null||"".equals(jdbc.Set)){
					return null;
				}else{
					String[] set=jdbc.Set.split(",");
					for(int i=0;i<set.length;i++){
						if(set[i]!=""&& set[i].indexOf("=")<=0){
							if(sets==""){
								sets=set[i]+"=?";
							}else{
								sets=sets+","+set[i]+"=?";
							}
							setvals.add(map.get(set[i]));
						}else {
							if(sets==""){
								sets=set[i];
							}else{
								sets=sets+","+set[i];
							}
						}
					}
				}
				
				String whes="";
				if(jdbc.Where!=null&&!"".equals(jdbc.Where)){
					String[] whe=jdbc.Where.split(",");
					for(int i=0;i<whe.length;i++){
						if(whe[i]!=""){
							if(whes==""){
								whes=whe[i]+"=?";
							}else{
								whes=whes+" and "+whe[i]+"=?";
							}
							setvals.add(map.get(whe[i]));
						}
					}
				}
				
				if(whes!=""){whes=" where "+whes;}
				String sql = "update "+changeObject(jdbc.Object)+" set "+sets +" "+whes;
				ls.add(sql);
				ls.add(setvals);
			}else if(jdbc.DDL==3){
				List<Object> dvals = new ArrayList<Object>();
				String pwhe="";
				String whe=jdbc.Where==null?"":jdbc.Where;
				String[] whes=whe.split(",");
				for(int i=0;i<whes.length;i++){
					if(!"".equals(whes[i].trim())){
						if(pwhe==""){
							pwhe = " "+whes[i]+"=? ";
						}else{
							pwhe += " and "+whes[i]+"=? ";	
						}
						dvals.add( map.get(whes[i]));
					}
				}

				String sql = "delete from "+changeObject(jdbc.Object)+ "  Where "+pwhe ;
				ls.add(sql);
				ls.add(dvals);
			}else {
				String sql =  jdbc.getYsql();
				ls.add(sql);
				ls.add(new ArrayList());
			}
		}
		return ls;
	}
	

	/**
	 * update数据 参数是对象数组格式
	 * return Int 影响数据条数
	 */
	public int update(String sql,Object[] params,String connName)throws SQLException {
		Object no=this.execute(sql, params,connName);
		return Integer.parseInt(no+"");
	}
	public int update(String sql,Object[] params)throws SQLException {
		Object no=this.execute(sql, params, SrcConfig.SRCDFPOOL);
		return Integer.parseInt(no+"");
	}
	public int update(String sql,Map<String, Object> map)throws SQLException{
		Map imap=SrcSqlUtil.initDeleteSql(map,"map");
		Object[] vals=(Object[]) imap.get("vals");
		Object no=this.execute(sql, vals, SrcConfig.SRCDFPOOL);
		return Integer.parseInt(no+"");
	}
	/**
	 * update数据 参数是SrcJson格式
	 * return Int 影响数据条数
	 */
	public int update(SrcJdbc jdbc,SrcJson obj)throws SQLException {
		Map<String,Object> jsmap=obj.getMap();
		return update(jdbc,jsmap);
	}
	
	/**
	 * 批量修改 list 支持预SQL
	 * list中的值可以是map或是javabean
	 */
	public int update(SrcJdbc jdbc,List list) {
		if(list==null||list.size()<=0){
			return 0;
		}
		int count=list.size();
		
		//预sql
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.getYsql())) {
			List cols=new ArrayList();
			
			String sql = getSrcSQL(jdbc).toUpperCase();
			int ws=sql.indexOf(" WHERE ")==0?sql.length():sql.indexOf(" WHERE ");
			String sets = sql.substring(sql.indexOf(" SET ")+3,ws);
			String[] seti=sets.split(",");
			
			for(String set :seti) {
				if(set.indexOf("?")>0) {
					String val=set.substring(0,set.indexOf("=")).trim();
					cols.add(val);
				}
			}
			
			if(sql.indexOf(" WHERE ")>0) {
				String whes= sql.substring(ws+5);
				String[] whesi = whes.split(" AND ");
				for(String whe :whesi) {
					if(whe.indexOf("?")>0) {
						String val=whe.substring(0,whe.indexOf("=")).trim();
						cols.add(whe);
					}
				}
			}
			Slog.println("	"+plzx+"SQL："+sql);
			String connName=this.getConnNameBySqlId(jdbc);
			try {
				Connection conn=getConnection(connName);
				prepStament.set(conn.prepareStatement(sql.toString()));
				connection.get().setAutoCommit(false);//使用非自动提交
			
				//循环list 注入值
				for (int i = 0; i < list.size(); i++) {
					Object ob= list.get(i);
					if(ob instanceof Map) {
						Map imap = (Map) list.get(i);
						String das="";
						for(int j=0;j<cols.size();j++){
							prepStament.get().setObject((j+1), imap.get(cols.get(j)));
							das+=imap.get(cols.get(j))+",";
						}
						if(das!=null) {
							Slog.println("	"+pamtxt+"-"+i+" = "+(das.length()>60?das.substring(0,60)+"...":das));
						}else {
							Slog.println("	"+pamtxt+"-"+i+" = ");
						}
					}
					//非map
					else {
						Class cls=list.get(i).getClass();
						Object vobj=list.get(i);
						String das="";
						for(int j=0;j<cols.size();j++){
							String colum= cols.get(j).toString();
						    String upperChar = colum.substring(0,1).toUpperCase();
						    String anotherStr = colum.substring(1);
						    String methodName = "get" + upperChar + anotherStr;
						    Method method = cls.getMethod(methodName, new Class[]{});
						    method.setAccessible(true);
						    Object val = method.invoke(vobj, new Object[]{});
							prepStament.get().setObject((j+1), val);
							das+=val+",";
						}
						if(das!=null) {
							Slog.println("	"+pamtxt+"-"+i+" = "+(das.length()>60?das.substring(0,60)+"...":das));
						}else {
							Slog.println("	"+pamtxt+"-"+i+" = ");
						}
					}
					prepStament.get().addBatch();
					//500数量一提交
					if(i>0&&i%500==0){
						connection.get().commit();
					}
				}
				prepStament.get().executeBatch();
				connection.get().commit();
				//清除缓存
				SrcCache.remove(changeObject(jdbc.Object));
			} catch (Exception e) {
				e.printStackTrace();
				try {
					connection.get().rollback();
				} catch (SQLException e1) {
				}
				count=0;
			}finally{
				closeAll(connName);
			}
		}
			
		//非SRCSQL
		else {
			String wht=jdbc.getWhere()==null?"":jdbc.getWhere();
			String sets=jdbc.getSet()==null?"":jdbc.getSet();
			if(sets=="") {return 0;}

			List<Object> cols = new ArrayList<Object>();
			String[] sts=sets.trim().split(",");
			String[] whts=wht.trim().split(",");
			Object obj = list.get(0);
			String pname = " 1=1 ";
			String set="";
			String whe="";

			if(obj instanceof Map) {
				Map map=(Map) obj;

				//组织set
				for(String seti : sts) {
					if(!"".equals(seti)) {
						if(seti.indexOf("=")<0) {
							if(set==""){set=seti+"=?";}else{set=set+","+seti+"=?";}
							cols.add(map.get(seti));
						}else {
							if(set==""){set=seti;}else{set=set+","+seti;}
						}
					}
				}
				//组织where
				for(String whti : whts) {
					if(!"".equals(whti) ) {

						if(whti.indexOf("=")>0 || whti.indexOf(" ")>0) {
							pname=pname+" and "+whti;
						}
						else {
							if(map.get(whti)!=null) {
								pname=pname+" and "+whti+"=?";
								cols.add(whti);
							}else {
								pname=pname+" and "+whti+" is null ";
							}
						}
					}
				}

				String sql = "update "+changeObject(jdbc.Object)+" set "+set+" where "+pname;
				Slog.println("	"+plzx+"SQL："+sql);
				String connName = this.getconnPoolName(jdbc);
				try {
					connection.set(getConnection(connName));
					prepStament.set(connection.get().prepareStatement(sql.toString()));
					connection.get().setAutoCommit(false);//使用非自动提交

					for (int i = 0; i < list.size(); i++) {
						Map imap = (Map) list.get(i);
						String das="";
						for(int j=0;j<cols.size();j++){
							prepStament.get().setObject((j+1), imap.get(cols.get(j)));
							das+=imap.get(cols.get(j))+",";
						}
						if(das!=null) {
							Slog.println("	"+pamtxt+"-"+i+" = "+(das.length()>60?das.substring(0,60)+"...":das));
						}else {
							Slog.println("	"+pamtxt+"-"+i+" = ");
						}
						prepStament.get().addBatch();
						//500数量一提交
						if(i>0&&i%500==0){
							connection.get().commit();
						}
					}
					prepStament.get().executeBatch();
					connection.get().commit();
					//清除缓存
					SrcCache.remove(changeObject(jdbc.Object));
				} catch (SQLException e) {
					e.printStackTrace();
					try {
						connection.get().rollback();
					} catch (SQLException e1) {
					}
					count=0;
				}finally{
					closeAll(connName);
				}
			}
			
			//list中非map
			else {
				Class clz = obj.getClass();
				Field[] fds =null;
				try {
					fds =  clz.getDeclaredFields();

					//组织set
					for(String seti : sts) {
						if(!"".equals(seti)) {
							if(seti.indexOf("=")<0) {
								if(set==""){set=seti+"=?";}else{set=set+","+seti+"=?";}
								cols.add(seti);
							}else {
								if(set==""){set=seti;}else{set=set+","+seti;}
							}
						}
					}
					//组织where
					for(String whti : whts) {
						if(!"".equals(whti) ) {

							if(whti.indexOf("=")>0 || whti.indexOf(" ")>0) {
								pname=pname+" and "+whti;
							}
							else {
								if(isColm(fds,whti)) {
									pname=pname+" and "+whti+"=?";
									cols.add(whti);
								}else {
									pname=pname+" and "+whti+" is null ";
								}
							}
						}
					}


					String sql = "update "+this.changeObject(jdbc.Object)+" set "+set+" where "+pname;
					Slog.println("	"+plzx+"SQL："+sql);
					String connName=this.getconnPoolName(jdbc);
					try {
						connection.set(getConnection(connName));
						prepStament.set(connection.get().prepareStatement(sql.toString()));
						connection.get().setAutoCommit(false);//使用非自动提交

						for (int i = 0; i < list.size(); i++) {
							Class cls=list.get(i).getClass();
							Object vobj=list.get(i);
							String das="";
							for(int j=0;j<cols.size();j++){
								String colum= cols.get(j).toString();
								String upperChar = colum.substring(0,1).toUpperCase();
								String anotherStr = colum.substring(1);
								String methodName = "get" + upperChar + anotherStr;
								Method method = cls.getMethod(methodName, new Class[]{});
								method.setAccessible(true);
								Object val = method.invoke(vobj, new Object[]{});
								prepStament.get().setObject((j+1), val);
								das+=val+",";
							}
							if(das!=null) {
								Slog.println("	"+pamtxt+"-"+i+" = "+das);
							}else {
								Slog.println("	"+pamtxt+"-"+i+" = ");
							}
							prepStament.get().addBatch();
							//500数量一提交
							if(i>0&&i%500==0){
								connection.get().commit();
							}
						}
						prepStament.get().executeBatch();
						connection.get().commit();
						//清除缓存
						SrcCache.remove(changeObject(jdbc.Object));
					} catch (SQLException e) {
						e.printStackTrace();
						try {
							connection.get().rollback();
						} catch (SQLException e1) {
						}
						count=0;
					}finally{
						closeAll(connName);
					}
				}catch(Exception e) {
					e.printStackTrace();
					count=0;
				}
			}
		}
		return count;
	}
	
	
	
	/**
	  *  修改数据，参数是map
	 * return int 返回影响条数
	 */
	public int update(SrcJdbc jdbc,Map<String, Object> map) throws SQLException{
		String sql="";
		String set="";
		String conName="";
		String pname=" 1=1 ";
		
		String wht=jdbc.getWhere()==null?"":jdbc.getWhere().trim();
		String sets=jdbc.getSet()==null?"":jdbc.getSet().trim();
		if(sets=="") {return 0;}
		
		List<Object> cols = new ArrayList<Object>();
		String[] sts=sets.trim().split(",");
		String[] whts=wht.trim().split(",");
		Object[] val=new Object[] {};
		
		//组织set
		for(String seti : sts) {
			if(!"".equals(seti)) {
				//无等号 不带赋值
				if(seti.indexOf("=")<0) {
					//where中不包含
					if(!Arrays.asList(whts).contains(sets)){
						if(set==""){set=seti+"=?";}else{set=set+","+seti+"=?";}
						 cols.add(map.get(seti));
					}
				}else {
					if(set==""){set=seti;}else{set=set+","+seti;}
				}
			}
		}
		//组织where
		for(String whti : whts) {
			if(!"".equals(whti) ) {
				
				if(whti.indexOf("=")>0 || whti.indexOf(" ")>0) {
					pname=pname+" and "+whti;
				}
				else {
					if(map.get(whti)!=null) {
						pname=pname+" and "+whti+"=?";
						cols.add(map.get(whti));
					}else {
						//pname=pname+" and "+whti+" is null ";
					}
				}
			}
		}
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.Ysql)) {
			conName=this.getConnNameBySqlId(jdbc);
			sql=getSrcSQL(jdbc);
			//sql=sql.toUpperCase();
			//val=jdbc.getSqlPrame();
			if(jdbc.getSqlPrame()==null) {
				Map pmap = this.initYsqlPram(sql, map);
				List vls=(List) pmap.get("wls");
				val=vls.toArray();
			}else {
				val=jdbc.getSqlPrame();
			}
			SrcCache.remove(getSqlObj(sql));
		}else {
			conName=this.getconnPoolName(jdbc);
			sql = "update "+changeObject(jdbc.Object)+" set "+set +" where "+pname;
			val = cols.toArray();
			SrcCache.remove(changeObject(jdbc.Object));
		}
		Object no=this.execute(sql, val,conName);
		return Integer.parseInt(no+"");
	}
	
	public int update(SrcJdbc jdbc, Object obj) throws SQLException {
		Map map =SrcUtil.objToMap(obj);
		return this.update(jdbc, map);
	}
	
	
	public int delete(SrcJdbc jdbc,Map<String,Object> map)throws SQLException {
		String sql = "";
		String pwhe= "";
		List<Object> vals = new ArrayList<Object>();
		String whe=jdbc.Where==null?"":jdbc.Where;
		String[] whes=whe.split(",");
		Object[] pam=null;
		for(int i=0;i<whes.length;i++){
			if(!"".equals(whes[i].trim())){
				if(pwhe==""){
					pwhe = " "+whes[i]+"=? ";
				}else{
					pwhe += " and "+whes[i]+"=? ";	
				}
				vals.add( map.get(whes[i]));
			}
		}
		String connName="";
		if(jdbc.getYsql()!=null && !"".equals(jdbc.getYsql())) {
			connName=this.getConnNameBySqlId(jdbc);
			sql=getSrcSQL(jdbc);
			//sql=sql.toUpperCase();
			if(jdbc.getSqlPrame()==null) {
				Map pmap = this.initYsqlPram(sql, map);
				List vls=(List) pmap.get("wls");
				pam=vls.toArray();
			}else {
				pam=jdbc.getSqlPrame();
			}
			SrcCache.remove(getSqlObj(sql));
		}else {
			connName=this.getconnPoolName(jdbc);
			sql ="delete from "+changeObject(jdbc.Object)+ "  Where "+pwhe ;
			pam=vals.toArray();
			SrcCache.remove(changeObject(jdbc.Object));
		}
		Object nc=this.execute(sql, pam,connName);
		return Integer.parseInt(nc+"");
	}
	
	public int delete(SrcJdbc jdbc,Object obj)throws SQLException{
		Map map =SrcUtil.objToMap(obj);
		return this.delete(jdbc, map);
	}


	public int delete(SrcJdbc jdbc,SrcJson obj)throws SQLException {
		Map<String,Object> map=obj.getMap();
		return delete(jdbc,map);
	}

	public int delete(String sql,Object[] params,String connName) throws SQLException{
		Object oc= this.execute(sql, params,connName);
		return Integer.parseInt(oc.toString());
	}
	public int delete(String sql,Object[] params) throws SQLException{
		Object oc=  this.execute(sql, params, SrcConfig.SRCDFPOOL);
		return Integer.parseInt(oc.toString());
	}
	public int delete(String sql, Map map) throws SQLException{
		Map imap=SrcSqlUtil.initDeleteSql(map,"map");
		Object[] vals=(Object[]) imap.get("vals");
		Object oc=  this.execute(sql, vals, SrcConfig.SRCDFPOOL);
		return Integer.parseInt(oc.toString());
	}

	/**
	 * 传入json查询
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public List<Object> select(SrcJdbc jdbc,SrcJson obj) throws SQLException {
		Map<String,Object> map=obj.getMap();
		return select(jdbc,map);
	}

	
	@Override
	public List<Object> select(SrcJdbc jdbc, Object[] pams) throws SQLException {
		String sql="";
		Object[]params =null;
		String connName="";
		
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.getYsql())) {
			connName=this.getConnNameBySqlId(jdbc);
			if(SrcConfig.SQL_LODING) {
				sql=this.getSrcSQL(jdbc);
			}else {
				//现场加载YSQL
				
			}
			return this.select(sql,pams,connName);
		}else {
			sql = this.initSelectSQL(jdbc);
			connName=this.getconnPoolName(jdbc);
			Map pmap =initWhereArry(jdbc,pams);
			String whe=pmap.get("whe").toString();
			if(!"".equals(whe)){
				whe = " where "+whe;
			}
			sql=sql+" "+whe;
			
			//排序情况
			if(jdbc.Order!=null&&!"".equals(jdbc.Order)){
				sql+= " order by "+jdbc.Order;
			}
			//是否有条数限制
			if(jdbc.Number>0){
				sql = this.initNumSQL(sql, connName, jdbc.Number);
			}
			return this.select(sql,pams,connName);
		}
	}
	
	
	/**
	 * 只SrcJdbc对象查询
	 * @param jdbc
	 * @return
	 * @throws SQLException
	 */
	public List select(SrcJdbc jdbc) throws SQLException {
		StringBuffer pageSql=new StringBuffer();
		Map rmap=new HashMap<>();
		String sql="";
		String connName = "";
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.getYsql())) {
			sql=this.getSrcSQL(jdbc);
			connName = this.getConnNameBySqlId(jdbc);
			return this.select(sql,jdbc.getSqlPrame(),connName);
		}else {
			sql=initSelectSQL(jdbc)+" where 1=? ";
			connName=this.getconnPoolName(jdbc);
			return this.select(sql,new Object[]{1},connName);
		}
	}
	
	
	
	public Map initYsqlPram(String sql,Map map) {
		sql=sql.toLowerCase();
	//	List vls=new ArrayList();
	//	List pls=new ArrayList();
		List wls=new ArrayList();
		Map ymap = new HashMap<>();
		if(sql.indexOf("insert ")>0) {
			//insert into table() values()
			String cols = sql.substring(sql.indexOf("("+1,sql.indexOf(")")));
			String col[]=cols.split(",");
			String vals = sql.substring(sql.lastIndexOf("("+1,sql.lastIndexOf(")")));
			String val[]= vals.split(",");
			for(int i=0;i<col.length;i++) {
				if("?".equals(val[i].trim())) {
					wls.add(map.get(col[i].trim())==null?"":map.get(col[i].trim()));
				}
			}
			ymap.put("wls", wls);
		}
		else if(sql.indexOf("delete ")>0) {
			//delete from table where 
			String whe = sql.substring(sql.indexOf(" where ")+7);
			String ws[]=new String[] {};
			if(whe.indexOf(" and ")>0) {
				 ws=whe.split(" and ");
			}else {
				ws=new String[] {whe};
			}
			ymap.put("wls", initwhere(ws,wls,map));
		}else if(sql.indexOf("update ")>0) {
			//update table set a=?,b=? where a=? and
			String set = sql.substring(sql.indexOf(" set ")+5,sql.indexOf(" where "));
			String st[] = new String[] {};
			if(set.indexOf(",")>0) {
				st = set.split(",");
			}else {
				st = new String[] {set};
			}
			for(int i=0;i<st.length;i++) {
				String sts = st[i].trim();
				if(sts.indexOf("=")>0&&sts.indexOf("?")>0) {
					String[] tmp = sts.split("=");
					if("?".equals(tmp[1])){
						wls.add(map.get(tmp[0].trim())==null?"":map.get(tmp[0].trim()));
					}
				}
			}
			//ymap.put("sls", sls);
			String whe = sql.substring(sql.indexOf(" where ")+7);
			String ws[] = whe.split(" and ");
			if(whe.indexOf(" and ")>0) {
				ws = whe.split(" and ");
			}else {
				ws = new String[] {whe};
			}
			
			ymap.put("wls", initwhere(ws,wls,map));
		}
		else if(sql.indexOf("select ")>0) {
			//select * from table where a=? and b=?
			String whe = sql.substring(sql.indexOf(" where ")+7);
			String ws[] = whe.split(" and ");
			if(whe.indexOf(" and ")>0) {
				ws = whe.split(" and ");
			}else {
				ws = new String[] {whe};
			}
			
			ymap.put("wls", initwhere(ws,wls,map));
		}
		return ymap;
	}
	
	
	public List initwhere(String[] ws,List wls,Map map) {
		for(int i=0;i<ws.length;i++) {
			String wand = ws[i].trim();
			if(wand.indexOf("?")>0 && wand.indexOf(">=")>0) {
				String[] w = wand.split(">=");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
			else if(wand.indexOf("?")>0 && wand.indexOf("<=")>0) {
				String[] w = wand.split("<=");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
			else if(wand.indexOf("?")>0 && wand.indexOf("<>")>0) {
				String[] w = wand.split("<>");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}else if(wand.indexOf("?")>0 && wand.indexOf(">")>0) {
				String[] w = wand.split(">");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
			else if(wand.indexOf("?")>0 && wand.indexOf("<")>0) {
				String[] w = wand.split("<");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
			else if(wand.indexOf("?")>0 && wand.indexOf("!=")>0) {
				String[] w = wand.split("!=");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
			else if(wand.indexOf("?")>0 && wand.indexOf("=")>0) {
				String[] w = wand.split("=");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
			else if(wand.indexOf("?")>0 && wand.indexOf(" like ")>0) {
				String[] w = wand.split(" like ");
				if("?".equals(w[1].trim())) {
					wls.add(map.get(w[0].trim())==null?"":map.get(w[0].trim()));
				}
			}
		}
		return wls;
	}
	
	
	/**
	 * 传入参数map查询
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public List<Object> select(SrcJdbc jdbc,Map map) throws SQLException {
		String sql="";
		Object[]params =null;
		String connName="";
		
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.getYsql())) {
			connName=this.getConnNameBySqlId(jdbc);
			jdbc.setMap(map);
			sql=this.getSrcSQL(jdbc);
			if(jdbc.getSqlPrame()==null){
				Map pmap = this.initYsqlPram(sql, map);
				List vls=(List) pmap.get("wls");
				params=vls.toArray();
			}else{
				params=jdbc.getSqlPrame();
			}
			if(map.get("page")!=null){
				return this.select(sql, params,(Page)map.get("page"),connName);
			}
		}else {
			connName=this.getconnPoolName(jdbc);
			sql=initSelectSQL(jdbc);
			
			Map pmap =initWherePam(jdbc,map);
			String whe=pmap.get("whe").toString();
			List wls= (List) pmap.get("wheval");
			params= wls.toArray();
	
			if(!"".equals(whe)){whe = " where "+whe;}
			
			sql=sql+" "+whe;
			if(jdbc.Order!=null && !"".equals(jdbc.Order)){
				sql=sql+" order by "+jdbc.Order;
			}
			if(map.get("page")!=null){
				return this.select(sql, params,(Page)map.get("page"),connName);
			}
			//是否有条数限制	
			if(jdbc.Number>0){
				sql=this.initNumSQL(sql, connName, jdbc.Number);
			}
		}
		return this.select(sql,params,connName);
	}
	
	
	/**
	 * 直接传入sql语句和参数数组查询
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public List<Object> select(String sql, Object[] params) throws SQLException {
		if(params==null) {
			params = new Object[] {};
		}
		return select( sql, params, SrcConfig.SRCDFPOOL);
	}
	
	/**
	 * 直接传入sql语句，参数数组，数据源名进行查询
	 * @param sql
	 * @param params
	 * @param ConnName 默认可为空
	 * @return
	 * @throws SQLException
	 */
	public List<Object> select(String sql, Object[] params,String connName) throws SQLException{
		List<Object> list = new ArrayList<Object>(); 
		ResultSet resultSet =null;
		try {
			connName=connName==""?SrcConfig.SRCDFPOOL:connName;
			String dbtype=SrcPoolManage.getDbType(connName);
			connection.set(getConnection(connName));
			prepStament.set( connection.get().prepareStatement(sql));  
			Slog.println("	"+plzx+"SQL："+sql);
			Slog.println("	SQL"+pamtxt+"-->");
			for(int i=0;i<params.length;i++){
				if(params[i]!=null){
					prepStament.get().setObject(i + 1, params[i]);
					Slog.println("	   "+params[i]);
				}
			}
			Slog.println("	SQL"+pamtxt+"<--");

			// 执行  
			resultSet= prepStament.get().executeQuery();
			if(!resultSet.next()){
			}else {
				ResultSetMetaData md = (ResultSetMetaData)resultSet.getMetaData();
				int ct = md.getColumnCount();
				do {
					Map<String,Object> map = new HashMap<>();
					for(int i =1;i<=ct;i++){
						String colm=md.getColumnLabel(i)==null?"":md.getColumnLabel(i);
						Object obj=resultSet.getObject(i);
						if(dbtype=="oracle" ||"oracle".equals(dbtype)) {
							if(obj instanceof  oracle.sql.CLOB){
								oracle.sql.CLOB clob=(CLOB) obj;
								String detailinfo = "";
								if(clob != null){
									detailinfo = clob.getSubString((long)1,(int)clob.length());
									map.put(colm,detailinfo); 
								}
							}else {
								map.put(colm,obj);
							}
						}else {
							map.put(colm,obj);
						}
					}
					Slog.println("	"+rest+"："+map);
					list.add(map);
				}while(resultSet.next());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Slog.error("["+execfail+"]："+e.getMessage());  
			return null;
		}finally{
			if(resultSet!=null) {
				resultSet.close();
				resultSet=null;
			}
			closeAll(connName);
		}
		return list;  
	}
	
	public List select(String sql,Object[] params,Page page) throws SQLException{
		//if(page!=null){
			return this.select(sql, params,page,SrcConfig.SRCDFPOOL);
		//}
	}
	public List select(String sql,Object[] params,String connName,Page page) throws SQLException{
		//if(page!=null){
			return this.select(sql, params,page,connName);
		//}
	}	
	/**
	 * 直接传入sql语句和参数数组和分页数据查询
	 * @param sql
	 * @param params
	 * @param page
	 * @return
	 * @throws SQLException
	 */
	private  List<Object> select(String sql, Object[] params, Page page,String connName) throws SQLException{
		List<Object> list = new ArrayList<Object>(); 
		int count = 0;
		connName=connName==null?SrcConfig.SRCDFPOOL:connName;
		String dbtype=SrcPoolManage.getDbType(connName);
		ResultSet resultSet=null;
		try {
			connection.set(getConnection(connName));
			if(page!=null){
				String countSql = "select count(1) from (" + sql + ")  tmp_count"; 
				Slog.println("	"+plzx+"SQL："+countSql);
				prepStament.set(connection.get().prepareStatement(countSql));
				Slog.println("	SQL"+pamtxt+"-->");
				for(int i=0;i<params.length;i++){
					if(params[i]!=null){
						prepStament.get().setObject(i + 1, params[i]);
						Slog.println("	   "+params[i]);
					}
				}
				Slog.println("	SQL"+pamtxt+"<--");
				ResultSet rs = prepStament.get().executeQuery();
				if (rs.next()) {
					count = rs.getInt(1);
					Slog.println("	"+returntxt+": "+count);
				}
				if(rs!=null) {
					rs.close();
					rs=null;
				}
				sql=page.initPageSql(SrcPoolManage.getDbType(connName),sql);
				page.setPageCount(count);
			}
//			map.put("page", page);
			Slog.println("	"+plzx+"SQL："+sql);
			prepStament.set(connection.get().prepareStatement(sql));  
			
			Slog.println("	SQL"+pamtxt+"-->");
			for(int i=0;i<params.length;i++){
				if(params[i]!=null){
					prepStament.get().setObject(i + 1, params[i]);
					Slog.println("	   "+params[i]);
				}
			}
			Slog.println("	SQL"+pamtxt+"<--");
			resultSet = prepStament.get().executeQuery(); 
			if(!resultSet.next()){
			}else {
				ResultSetMetaData md = (ResultSetMetaData)resultSet.getMetaData();
				int ct = md.getColumnCount();
				do{
			        Map<String,Object> rmap = new HashMap<>();
			        Map<String,Object> pmap = new HashMap<>();
			        for(int i =1;i<=ct;i++){
			        	String colm=md.getColumnName(i);
			        	Object obj=resultSet.getObject(i);

			        	if(dbtype=="oracle" ||"oracle".equals(dbtype)) {
			        		if(obj instanceof  oracle.sql.CLOB){
								oracle.sql.CLOB clob=(CLOB) obj;
								String detailinfo = "";
							    if(clob != null){
							    	detailinfo = clob.getSubString((long)1,(int)clob.length());
							    	rmap.put(colm,detailinfo); 
							    	pmap.put(colm,"...."); 
							    }
							}else {
								rmap.put(colm,obj);
								pmap.put(colm,obj);
							}
			        	}else {
			        		
							rmap.put(colm,obj);
							pmap.put(colm,obj);
						}
			        }
			        list.add(rmap);
			        Slog.println("	"+rest+"："+pmap);
			    }while(resultSet.next());
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
			Slog.error("["+execfail+"]: "+e.getMessage());  
			return null;
		}finally{
			if(resultSet!=null) {
				resultSet.close();
				resultSet=null;
			}
			closeAll(connName);
		}
		return list;  
	}
	
	
	
	/**
	 * 判断数据库类型
	 * @param conn
	 * @param driver
	 * @return
	 */
	public boolean eqConnDriver(String dname) {
		try {
			String drivers=connection.get().getMetaData().getDriverName().trim().toUpperCase();
			if(drivers.indexOf(dname.toUpperCase())>-1) {
				return true;
			}
		} catch (SQLException e) {
		}
		return false;
	}

	
	/**
	 * 直接sql语句获取数据
	 * @param sql
	 * @param params
	 */
	public Map<String,Object> get(String sql,Object[] params) throws SQLException{
		return get(sql, params, SrcConfig.SRCDFPOOL);
	}
	public Map<String,Object> get(String sql,Object[] params,String connName) throws SQLException{
		Map rmap = null,pmap=null;
		connection.set(getConnection(connName));
		String dbtype=SrcPoolManage.getDbType(connName);
		ResultSet resultSet =null;
		try {
			Slog.println("	"+plzx+"SQL："+sql);
			prepStament.set( connection.get().prepareStatement(sql));  
			Slog.println("	SQL"+pamtxt+"-->");
			for(int i=0;i<params.length;i++){
				if(params[i]!=null){
					prepStament.get().setObject(i + 1, params[i]);
					Slog.println("	   "+params[i]);
				}
			}
			Slog.println("	SQL"+pamtxt+"<--");
			
			// 执行  
			resultSet= prepStament.get().executeQuery(); 
			ResultSetMetaData rsmd = resultSet.getMetaData();  
			int columnCount = rsmd.getColumnCount();  
			while (resultSet.next()) {
				if(rmap==null) {rmap= new HashMap<>();}
				if(pmap==null) {pmap= new HashMap<>();}
				for (int i = columnCount; i >= 1; i--) {
					String colm=rsmd.getColumnLabel(i);
					Object obj=resultSet.getObject(i);
					
					if(dbtype=="oracle" ||"oracle".equals(dbtype)) {
						if(obj instanceof  oracle.sql.CLOB){
							oracle.sql.CLOB clob=(CLOB) obj;
							String detailinfo = "";
						    if(clob != null){
						    	detailinfo = clob.getSubString((long)1,(int)clob.length());
								rmap.put(colm,detailinfo); 
								pmap.put(colm, "....");
						    }
						}else{
							rmap.put(colm, obj);
						//	if(rs.length()>80) {rs="...";}
							pmap.put(colm,obj);  
						}
					} else{
						rmap.put(colm, obj);
					//	if(cval.length()>80) {cval="...";}
						pmap.put(colm, obj);   
					}
				} 
			}
			
			Slog.println("	"+rest+"："+pmap);
		} catch (SQLException e) {
			Slog.error("["+execfail+"]："+e.getMessage());  
			return null;
		}finally{
			if(resultSet!=null) {
				resultSet.close();
				resultSet=null;
			}
			closeAll(connName);
		}
		
		return rmap; 
	}
	public Map<String,Object> get(SrcJdbc jdbc,Object obj) throws SQLException{
		Map map =SrcUtil.objToMap(obj);
		return get(jdbc,map);
	}
	//获取单条数据
	public Map<String,Object> get(SrcJdbc jdbc,Map map) throws SQLException{
		StringBuffer pageSql=new StringBuffer();
		String sql="";
		
		Object[]params=null;
		String connName="";
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.getYsql())) {
			connName=this.getConnNameBySqlId(jdbc);
			sql=this.getSrcSQL(jdbc);
			//params=jdbc.SqlPrame;
			if(jdbc.getSqlPrame()==null) {
				Map pmap = this.initYsqlPram(sql, map);
				List vls=(List) pmap.get("wls");
				params=vls.toArray();
			}else {
				params=jdbc.getSqlPrame();
			}
		}else {
			connName=this.getconnPoolName(jdbc);
			sql=initSelectSQL(jdbc);
			Map pmap=initWherePam(jdbc,map);
			String ws=pmap.get("whe").toString();
			List wls= (List) pmap.get("wheval");
			params= wls.toArray();
			
			if("".equals(ws.trim())){
				sql=sql+"  where 1=? ";
				params=new Object[]{1};
			}else{
				sql=sql+" where "+ws;
			}
		}
		return get(sql, params, connName);
	}
	
	/**
	 * get缓存获取数据
	 * **/
	public  Map<String,Object> cacheGet(String sql,Object[] params,String connName) throws SQLException{
		List ls = cacheSelect( sql, params , connName);
		if(ls!=null && ls.size()>0) {
			return (Map<String, Object>) ls.get(0);
		}
		return new HashMap<>();
	}
	public  Map<String,Object> cacheGet(String sql,Object[] params) throws SQLException{
		return cacheGet(sql, params , SrcConfig.SRCDFPOOL);
	}
	public  Map<String,Object> cacheGet(SrcJdbc jdbc,Map map) throws SQLException{
		return get(jdbc,map);
	}
	public  Map<String,Object> cacheGet(SrcJdbc jdbc,Object obj) throws SQLException{
		Map map =SrcUtil.objToMap(obj);
		return get(jdbc,map);
	}
	
	
	/**使用缓存查询*/
	public List<Object> cacheSelect(SrcJdbc jdbc,Map map) throws SQLException{
		String obj="",key="";
		List pvls=new ArrayList();
		if(jdbc.getYsql()!=null&&!"".equals(jdbc.getYsql())) {
			String[] ks=getkeyval(jdbc.getYsql(), jdbc.getSqlPrame());
			key=ks[0];
			obj=ks[1];
		}else {
			
			obj=jdbc.Object;
			String[] whe=jdbc.Where.split(",");
			String ord=jdbc.Order==null?"":jdbc.Order;
			String lk=jdbc.Like==null?"":jdbc.Like;
			
			key=obj+",";
			String w="";
			for(int i=0;i<whe.length;i++){
				if(!"".equals(whe[i])){
					String val = map.get(whe[i])==null?"": map.get(whe[i]).toString();
					w+=whe[i]+"="+val+",";
					pvls.add(val);
				}
			}	
			key+=w+lk+ord;
			key=key.toLowerCase();
			obj=obj.toLowerCase();
		}
		
	//	String[] ks=getkeyval( sql, params);
		
		//先在缓存中获取,先查询大类
		Map<String,Object> ocs= SrcCache.get(obj);
		
		//大类存在 查询小类
		if(ocs!=null){
			Slog.println("	"+gain+cacheA+"----"+obj);
			List vobj=(List) ocs.get(key);
			if(vobj!=null){
				Slog.println("	"+gain+cacheB+"------"+key);
				return vobj;
			}
		}
		
		
		List rls=this.srcDao.select(jdbc,map);
		//List rls=(List) rmap.get("list");
		//查询数据库后加入到缓存
		//已查询到数据
		//已经有A级
		if(rls!=null && rls.size()>0){
			if(ocs!=null){
				Slog.println("	"+use+cacheB+"------"+key);
				ocs.put(key, rls);
			
			}else{
				Slog.println("	"+use+cacheB+"------"+key);
				Slog.println("	"+use+cacheA+"----"+obj);
				Map vobjmap=new HashMap<>();
				vobjmap.put(key, rls);
				SrcCache.put(obj, vobjmap);
			}
		}
		return rls;
	}
	
	/**使用sql进行缓存查询*/
	public List<Object> cacheSelect(String sql,Object[] params)throws SQLException{
		return cacheSelect( sql, params, SrcConfig.SRCDFPOOL);
	}
	public List<Object> cacheSelect(String sql,Object[] params ,String connName)throws SQLException{
		String[] ks=getkeyval( sql, params);
		
		//先在缓存中获取,先查询大类
		Map<String,Object> ocs= SrcCache.get(ks[1]);
		
		//大类存在 查询小类
		if(ocs!=null){
			Slog.println("	"+gain+cacheA+"----"+ks[1]);
			List<Object> vobj=(List) ocs.get(ks[0]);
			if(vobj!=null){
				Slog.println("	"+gain+cacheB+"------"+ks[0]);
				return vobj;
			}
		}
		
		List rls=this.select(sql, params,connName);
		//查询数据库后加入到缓存
		//已查询到数据
		//已经有A级
		if(rls.size()>0){
			if(ocs!=null){
				Slog.println("	"+use+cacheB+"------"+ks[0]);
				ocs.put(ks[0], rls);
			
			}else{
				Slog.println("	"+use+cacheB+"------"+ks[0]);
				Slog.println("	"+use+cacheA+"----"+ks[1]);
				Map vobjmap=new HashMap<>();
				vobjmap.put(ks[0], rls);
				SrcCache.put(ks[1], vobjmap);
			}
		}
		return rls;
	}

	/**
	 * 通过sql语句和参数取得第一第二级缓存
	 * 缓存规则 select部分+where部分 为2级,form部分为1级
	 * @param sql
	 * @param params
	 * @return
	 */
	protected String[] getkeyval(String sql,Object[] params) {
		sql=sql.toUpperCase();
		String key= sql.substring(sql.indexOf("SELECT")+6,sql.lastIndexOf("FROM"));
		String obj= sql.substring(sql.lastIndexOf("FROM")+4,sql.lastIndexOf("WHERE"));
		String wer=sql.substring(sql.lastIndexOf("WHERE")+5);
		key+=wer+params.toString();
		return new String[]{key,obj};
	}

	
	
	/** 
	 * insert update delete SQL语句的执行的统一方法 
	 * @param params 参数数组
	 * //conn.setAutoCommit(false);//设置数据手动提交，自己管理事务  
	 * //conn.commit();
	 **/ 
	public Object execute(String sql, Object[] params){
		return execute( sql,  params,SrcConfig.SRCDFPOOL);
	}
	public Object execute(String sql, Object[] params,String connName){
		
		// 受影响的行数  
		int affectedLine = 0;
		ResultSet resultSet=null; 
		try {
			connection.set(getConnection(connName));
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		try {  
			
			// 获得连接  
			// 调用SQL   返回主键值
			if(sql.toUpperCase().indexOf("INSERT")>=0 && eqConnDriver("ORACLE")){
				PreparedStatement pstmt  = connection.get().prepareStatement(sql);
				OraclePreparedStatement pstt = pstmt.unwrap(OraclePreparedStatement.class);
				Slog.println("	"+plzx+"SQL="+sql);
				if (params != null) {
					Slog.println("	SQL"+pamtxt+"-->");
					for (int i = 0; i < params.length; i++) { 
						Object pam= params[i];
						pstt.setObject(i + 1,pam); 
						Slog.println((i+1)+"	   "+pam);
					}
					Slog.println("	SQL"+pamtxt+"<--");
				}

				///需要带有返回值
				if(sql.indexOf("returning ")>1) {
					pstt.registerReturnParameter((params.length+1), OracleTypes.VARCHAR);
					pstt.executeUpdate();
					resultSet = pstt.getReturnResultSet(); // rest is not null
					resultSet.beforeFirst();
					while (resultSet.next()) {
						String id = resultSet.getString(1);
						Slog.println("	"+returntxt+"："+id);
						return id;
					}
				}else {
					//只返回执行码
					affectedLine=pstt.executeUpdate();
				}

			}else{
				Slog.println("	"+plzx+"SQL="+sql);
				prepStament.set(connection.get().prepareStatement(sql));
				if (params != null) {
					Slog.println("	SQL"+pamtxt+"-->");
					for (int i = 0; i < params.length; i++) {  
						prepStament.get().setObject(i + 1, params[i]); 
						Slog.println("	   "+params[i]);
					}
					Slog.println("	SQL"+pamtxt+"<--");
				}
				affectedLine = prepStament.get().executeUpdate();
			}
		} catch (SQLException e) {  
			e.printStackTrace();
			Slog.error(e.getMessage());
			affectedLine=0;
			try {
				connection.get().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if(resultSet!=null) {
				try {
					resultSet.close();
					resultSet=null;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			// 释放资源  
			closeAll(connName);  
		}
		Slog.println("	"+returntxt+"："+affectedLine);
		return affectedLine;  
	}  

	/**
	 * 调用存储过程
	 * @param call 过程语句
	 * @param data 输入参数
	 * @param out  输出参数
	 * @param connName  数据源名 默认可以为空或null
	 * @return
	 */
	public Map call(String call,Object[] data,Object[] out){
		return call( call,data,out,SrcConfig.SRCDFPOOL);
	}
	
	/**
	 * srcjdbc调用存储过程需初始化call属性，预sql需初始化ysql属性，多数据源情况请指明数据源pool
	 * @param jdbc
	 * @param data
	 * @param out
	 * @return
	 */
	public Map call(SrcJdbc jdbc,Object[] data,Object[] out){
		String call="";
		String connName="";
		if(jdbc.getYsql()!=null&& !"".equals(jdbc.getYsql())) {
			connName=this.getConnNameBySqlId(jdbc);
			String sql=this.getSrcSQL(jdbc);
		}else {
			call=jdbc.getCall();
			connName=jdbc.getPool();
		}
		return call( call,data,out,connName);
	}
	
	public Map call(String call,Object[] data,Object[] out,String connName){
		  Map rmap = new HashMap<>();
			try {
				connection.set(getConnection(connName));
				CallableStatement cs = connection.get().prepareCall("{call "+call+"}");  //调用格式 {call 存储过程名(参数)}
		       
				for(int i=0;i<data.length;i++) {
					cs.setObject(i+1,data[i]);
				}
				int cp=data.length+out.length;
				for(int i=data.length; i<cp; i++) {
					cs.registerOutParameter(i+1,Types.VARCHAR);
				}
				
		        cs.execute();  //执行
		        
		        //sb=new Object[out.length];
		        //如果有返回参数
		        for(int i=data.length; i<cp; i++) {
		        	String testPrint = cs.getString(i+1);
		        	//sb[i-data.length]=testPrint;
		        	rmap.put(out[i-data.length], testPrint);
		        }
		        cs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				closeAll(connName);
			}
		Slog.println("call "+call+" 完成..");
		return rmap;
	}
	
	

	/**
	 * 获取连接通道
	 * @throws SQLException
	 */
	public static Connection getConnection(String connName) throws SQLException{
//		if(connection==null||connection.get().isClosed()){
			 return SrcPoolManage.getConnection(connName);
			
//			Map srcPoolMap =SrcConfig.CONNMAP;
//			SrcPool sp=(SrcPool) srcPoolMap.get(connName);
//			connName=connName==null?SrcConfig.SRCDFPOOL:connName;
//			connName=connName==""?SrcConfig.SRCDFPOOL:connName;
//			connection=sp.getConnection(connName);
//		}
	//	return connection;
	}
	
	/**
	 * 关闭所有资源
	 */
	private void closeAll(String connName) {
		

		// 关闭PreparedStatement对象  
		if (prepStament.get() != null) {  
			try {  
				prepStament.get().close();  
			} catch (SQLException e) {
				e.printStackTrace();
			}  
		}  

		// 关闭CallableStatement 对象  
//		if (callableStatement != null) {  
//			try {  
//				callableStatement.close();  
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}  
//		}  

		// 返还Connection 对象  
		try {
			SrcPoolManage.closeConnection(connName,connection.get());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (connection.get() != null) {  
			try {  
				connection.get().close();  
			} catch (SQLException e) {  
				Slog.error(e.getMessage());  
			}  
		}     
	}
	
	/**
	 * @param fieldClassName
	 * @param fieldName
	 * @param rs
	 * @param fieldValue
	 * @throws SQLException
	 */
	private void toMapList(String fieldClassName, String fieldName, ResultSet resultSet, Map fieldValue)throws SQLException  
	{  
		fieldName = fieldName.toLowerCase();  
		// 优先规则：常用类型靠前  
		if (fieldClassName.equals("java.lang.String"))  
		{  
			String s = resultSet.getString(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Integer"))  
		{  
			int s = resultSet.getInt(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);// 早期jdk需要包装，jdk1.5后不需要包装  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Long"))  
		{  
			long s = resultSet.getLong(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Boolean"))  
		{  
			boolean s = resultSet.getBoolean(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Short"))  
		{  
			short s = resultSet.getShort(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Float"))  
		{  
			float s = resultSet.getFloat(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Double"))  
		{  
			double s = resultSet.getDouble(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.sql.Timestamp"))  
		{  
			java.sql.Timestamp s = resultSet.getTimestamp(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.sql.Date") || fieldClassName.equals("java.util.Date"))  
		{  
			java.util.Date s = resultSet.getDate(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.sql.Time"))  
		{  
			java.sql.Time s = resultSet.getTime(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.Byte"))  
		{  
			byte s = resultSet.getByte(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, new Byte(s));  
			}  
		}  
		else if (fieldClassName.equals("[B") || fieldClassName.equals("byte[]"))  
		{  
			// byte[]出现在SQL Server中  
			byte[] s = resultSet.getBytes(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.math.BigDecimal"))  
		{  
			BigDecimal s = resultSet.getBigDecimal(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.lang.jdbc.Object") || fieldClassName.equals("oracle.sql.STRUCT"))  
		{  
			Object s = resultSet.getObject(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.sql.Array") || fieldClassName.equals("oracle.sql.ARRAY"))  
		{  
			java.sql.Array s = resultSet.getArray(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.sql.Clob"))  
		{  
			java.sql.Clob s = resultSet.getClob(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
		else if (fieldClassName.equals("java.sql.Blob"))  
		{  
			java.sql.Blob s = resultSet.getBlob(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}else  {  
				fieldValue.put(fieldName, s);  
			}  
		}else  {// 对于其它任何未知类型的处理  
			Object s = resultSet.getObject(fieldName);  
			if (resultSet.wasNull())  
			{  
				fieldValue.put(fieldName, null);  
			}  
			else  
			{  
				fieldValue.put(fieldName, s);  
			}  
		}  
	}

	/**
	 * 批量执行一组业务，同一事务
	 * @param ls 参数ls中需要包含值
	 * @return
	 */
	public String[] BatchSrcJdbc(List<SrcJdbc> ls) {
		if(ls==null || ls.size()<=0) {
			return new String[] {};
		}
		
		SrcJdbc ojd=ls.get(0);
		String connName=this.getconnPoolName(ojd);
		//批量执行返回
		String[] ek = new String[ls.size()];
		String obs="";
		ResultSet resultSet=null;
		 try {
			 connection.set(getConnection(connName)) ;
			 connection.get().setAutoCommit(false);

			for(int i=0;i<ls.size();i++){
				SrcJdbc jdbc=ls.get(i);
				if(jdbc!=null){
					obs=jdbc.Object;
					List sls=initBase(jdbc);
					if(sls!=null){
						String sql=sls.get(0).toString().toUpperCase();
						List dls=(List) sls.get(1);
						Object[] params=dls.toArray();
						
						//oracle新增
						if(eqConnDriver("oracle") &&  sql.indexOf("INSERT ")>=0){
							sql=" "+sql+" returning id into ?";
							Slog.println("	"+plzx+"SQL："+sql);
//							connection = connection.get().unwrap(OracleConnection.class);
							OraclePreparedStatement pstt = (OraclePreparedStatement) connection.get().prepareStatement(sql);
						
							if (params != null) {
								Slog.println("	SQL"+pamtxt+"-->");
								for (int j = 0; j < params.length; j++) {  
									pstt.setObject(j + 1, params[j]); 
									Slog.println((j+1)+"	   "+params[j]);
								}
								Slog.println("	SQL"+pamtxt+"<--");
							}
							 pstt.registerReturnParameter((params.length+1), OracleTypes.VARCHAR);
							 pstt.executeUpdate();
							 resultSet= pstt.getReturnResultSet(); 
				             while (resultSet.next()) {
				                String id = resultSet.getString(1);
						        //执行结果是null 回滚 返回
						        if(id==null || "".equals(id)){
						        	connection.get().rollback();
						        	return new String[]{"0"};
						        }
				                ek[i]=id;
				             }
				         //非oracle新增
						}else if(!eqConnDriver("oracle") &&sql.indexOf("INSERT ")>=0){
							Slog.println("	"+plzx+"SQL："+sql);
							prepStament.set(connection.get().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS));  
							if (params != null) {
								Slog.println("	SQL"+pamtxt+"-->");
								for (int j=0; j<params.length; j++){  
									prepStament.get().setObject(j+1, params[j]); 
									Slog.println("	   "+params[j]);
								}
								Slog.println("	SQL"+pamtxt+"<--");
							}
							int st = prepStament.get().executeUpdate();  
							if(st>0){
								resultSet = prepStament.get().getGeneratedKeys();
								//rs.beforeFirst();
								 if(resultSet.next()){
									 String id= resultSet.getString(1);
								        //执行结果是null 回滚 返回
								        if(id==null || "".equals(id)){
								        	connection.get().rollback();
								        	return new String[]{"0"};
								        }
									 ek[i]=id;
								 }
							}
							
						//非新增
						}else{
							Slog.println("	"+plzx+"SQL："+sql);
							prepStament.set(connection.get().prepareStatement(sql));
							if (params != null) {
								Slog.println("	SQL"+pamtxt+"-->");
								for (int j=0; j<params.length; j++){  
									prepStament.get().setObject(j+1, params[j]); 
									Slog.println("	   "+params[j]);
								}
								Slog.println("	SQL"+pamtxt+"<--");
							}
					        int num = prepStament.get().executeUpdate();
					        //执行结果是0 回滚 返回
					        if(num<=0){
					        	connection.get().rollback();
					        	return new String[]{"0"};
					        }
					        ek[i]=num+"";
						}
					}
				}
				//清除缓存
				if(!"".equals(obs)){
					SrcCache.remove(changeObject(obs));
				}
			}
			
			connection.get().commit();
		}catch (SQLException e) {
			e.printStackTrace();
			try {
				connection.get().rollback();
			} catch (SQLException e1) {
			}
			ek=new String[] {};
			return ek;
		}finally{
			// 关闭结果集对象  
			if (resultSet != null) {  
				try {  
					resultSet.close();
					resultSet=null;
				} catch (SQLException e) {
					e.printStackTrace();
				}  
			}  
			closeAll(connName);
		}
		return ek;
	}
	

	/**
	 * 批量执行一组业务，同一事务
	 * @param ls
	 * @param dls 包含的值
	 * @return
	 */
	public String[] BatchSrcJdbc(List<String> ls,List<Object[]> dls) {
		return BatchSrcJdbc(ls,dls,SrcConfig.SRCDFPOOL);
	}
	public String[] BatchSrcJdbc(List<String> ls,List<Object[]> dls,String connName) {
		//批量执行返回
		String[] ek = new String[ls.size()];
		Connection conn=null;
		String obs="";
		ResultSet resultSet=null;
		 try {
			conn=getConnection(connName) ;
			conn.setAutoCommit(false);
			for(int i=0;i<ls.size();i++){
				String sql=ls.get(i).toUpperCase();
				if(!"".equals(sql.trim())){
					obs=getSqlObj(sql);
		
						Object[] params=dls.get(i);
						
						//oracle新增
						if(eqConnDriver("oracle") && sql.indexOf("insert")>=0){
							sql=" "+sql+" returning id into ?";
							
							Slog.println("	"+plzx+"SQL："+sql);
							OraclePreparedStatement pstt = (OraclePreparedStatement) connection.get().prepareStatement(sql);
							if (params != null) {
								Slog.println("	SQL"+pamtxt+"-->");
								for (int j = 0; j < params.length; j++) {  
									pstt.setObject(j + 1, params[j]); 
									Slog.println((j+1)+"	   "+params[j]);
								}
								Slog.println("	SQL"+pamtxt+"<--");
							}
							 pstt.registerReturnParameter((params.length+1), OracleTypes.VARCHAR);
							 pstt.executeUpdate();
							 resultSet = pstt.getReturnResultSet(); 
				             while (resultSet.next()) {
				                String id = resultSet.getString(1);
				                ek[i]=id;
				             }
				         //非oracle新增
						}else if(!eqConnDriver("oracle") &&sql.indexOf("insert")>=0){
							Slog.println("	"+plzx+"SQL："+sql);
							prepStament.set(connection.get().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS));  
							if (params != null) {
								Slog.println("	SQL"+pamtxt+"-->");
								for (int j=0; j<params.length; j++){  
									prepStament.get().setObject(j+1, params[j]); 
									Slog.println("	   "+params[j]);
								}
								Slog.println("	SQL"+pamtxt+"<--");
							}
							int st = prepStament.get().executeUpdate();  
							if(st>0){
								resultSet = prepStament.get().getGeneratedKeys();
								 if(resultSet.next()){
									 String id= resultSet.getString(1);
									 ek[i]=id;
								 }
							}
							
						//非新增
						}else{
							Slog.println("	"+plzx+"SQL："+sql);
							prepStament.set( conn.prepareStatement(sql));
							if (params != null) {
								Slog.println("	SQL"+pamtxt+"-->");
								for (int j=0; j<params.length; j++){  
									prepStament.get().setObject(j+1, params[j]); 
									Slog.println("	   "+params[j]);
								}
								Slog.println("	SQL"+pamtxt+"<--");
							}
					        int num = prepStament.get().executeUpdate();
					        ek[i]=num+"";
						}
					}
				//清除缓存
				if(!"".equals(obs)){
					SrcCache.remove(obs);
				}
			}
			conn.commit();
		}catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
				resultSet=null;
			} catch (SQLException e1) {
			}
			ek=new String[] {};
			return ek;
		}finally{
			if(resultSet!=null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			closeAll(connName);
		}
		return ek;
	}
	

	/**
	 * 从SrcJdbc中获取where参数
	 * @param jdbc
	 * @param map
	 * @return
	 */
	private Map initWherePam(SrcJdbc jdbc,Map map){
		List<Object> wheval= new ArrayList<Object>();
		//处理whe
		String whe="";
		if(jdbc.Where!=null && !"".equals(jdbc.Where)){
			String[] ws=jdbc.Where.split(",");
			for(int i=0;i<ws.length;i++){
				
				String whr=ws[i].toString().trim();
				//必须存在查询条件map中并且有值
				if(!"".equals(whr) ){
					
					//带等号有指定值
					if(whr.indexOf("=")>1||whr.indexOf(">")>1||whr.indexOf("<")>1){
						if(whe==""){whe=whr;}else {whe = whe+" and "+whr;}
						continue;
					}
					
					//map中为null 修改为不包含
					 if( map.get(whr)==null||"null".equals(map.get(whr))){
						//if(whe==""){ whe = whr+" is null "; }else{ whe = whe+" and "+whr+" is null "; }
					//map中为空情况
					}else if("".equals(map.get(whr))){
						if(whe==""){ whe = whr+" ='' "; }else{ whe = whe+" and "+whr+" ='' "; }
						
					//map中不为空情况 !为非空
					}else if("!".equals(map.get(whr))){
						if(whe==""){ whe = whr+" is not null "; }else{ whe = whe+" and "+whr+" is not null "; }
					//有值
					}else{
						String wmap=map.get(whr)==null?"":map.get(whr)+"";
						//不等于情况
						if("!".equals(wmap.toString().substring(0,1))){
							wmap=wmap.toString().substring(1);
							if(whe==""){ whe = whr+" !=? "; }else{ whe = whe+" and "+whr+" !=? "; }
						}else{
							if(whe==""){ whe = whr+" =? "; }else{ whe = whe+" and "+whr+" =? "; }
						}
						wheval.add(wmap);
					}
				}
			}
		}
		
		
		//处理in
		if(jdbc.In!=null && !"".equals(jdbc.In)){
			if(!"".equals(whe)){
				whe=whe+" and "+jdbc.In;
			}else{
				whe=" "+jdbc.In;
			}
		}
		
		//处理like
		if(jdbc.Like!=null && !"".equals(jdbc.Like)){
			if(!"".equals(whe)){
				whe=whe+" and "+jdbc.Like;
			}else{
				whe=" "+jdbc.Like;
			}
		}
		
		Map rmap = new HashMap<>();
		rmap.put("whe",whe);
		rmap.put("wheval",wheval);
		return rmap;
	}
	
	/**
	 * 从数组整理where
	 * @param jdbc
	 * @param obj
	 * @return
	 */
	private Map initWhereArry(SrcJdbc jdbc,Object[] data){
		List<Object> wheval= new ArrayList<Object>();
		//处理whe
		String whe="";
		if(jdbc.Where!=null && !"".equals(jdbc.Where)){
			String[] ws=jdbc.Where.split(",");
			for(int i=0;i<ws.length;i++){
				String whr=ws[i].toString().trim();
				if(!"".equals(whr) && whr.indexOf("=")>0 ){
					if(whe==""){whe=whr;}else {whe = whe+" and "+whr;}
					continue;
				}
				
				if(data[i]==null) {
					if(whe==""){ whe = whr+" is null "; }else{ whe = whe+" and "+whr+" is null "; }
				}else {
					String wmap=data[i]==null?"":data[i]+"";
					//不等于情况
					if("!".equals(wmap.toString().substring(0,1))){
						wmap=wmap.toString().substring(1);
						if(whe==""){ whe = whr+" !=? "; }else{ whe = whe+" and "+whr+" !=? "; }
					}else{
						if(whe==""){ whe = whr+" =? "; }else{ whe = whe+" and "+whr+" =? "; }
					}
					wheval.add(wmap);
				}
			}
		}
		//处理in
		if(jdbc.In!=null && !"".equals(jdbc.In)){
			if(!"".equals(whe)){
				whe=whe+" and "+jdbc.In;
			}else{
				whe=" "+jdbc.In;
			}
		}
		
		//处理like
		if(jdbc.Like!=null && !"".equals(jdbc.Like)){
			if(!"".equals(whe)){
				whe=whe+" and "+jdbc.Like;
			}else{
				whe=" "+jdbc.Like;
			}
		}
		
		Map rmap = new HashMap<>();
		rmap.put("whe",whe);
		rmap.put("wheval",wheval);
		return rmap;
	}

	//实例化查询sql
	public String initSelectSQL(SrcJdbc jdbc){
		String sql="";
		String connName=this.getconnPoolName(jdbc);

		
			if(jdbc.getObject()==null) {
				return "";
			}
			
			//多数据源情况= 数据源.表
			if(jdbc.getObject().indexOf(".")>0) {
				String object=jdbc.getObject();
				jdbc.setObject(object.substring(object.indexOf(".")+1));
			}
			
			
			//多张表
			if(jdbc.Object.indexOf(",")>0){
				String[] ob=jdbc.Object.split(",");
				String os="";
				for(int i=0;i<ob.length;i++){
					//如果f为空或市*号
					if(jdbc.Fields==null || "*".equals(jdbc.Fields.trim())){
						String[] tb=new String[2];
						//如果对象有空格
						if(ob[i].indexOf(" ")>0){
							String tmp=ob[i].trim();//去除两边的空格
							tb=tmp.split(" ");
						}else{
							tb[0]=ob[i].toUpperCase();
							tb[1]=ob[i].toLowerCase();
						}
						
						if(os==""){
							os+=tb[1]+".*";
						}else{
							os=os+","+tb[1]+".*";
						}
					}else{
						os=jdbc.Fields;
					}
				}
				//有无join情况
				if(jdbc.Join==null){
					sql=" select "+os+" from "+jdbc.Object;
				}else{
					sql=" select "+os+" from "+jdbc.Join;
				}
				
			//单张表
			}else{
				if(jdbc.Fields==null){
					sql= "select * from "+jdbc.Object;
				}else{
					sql= "select "+jdbc.Fields+" from "+jdbc.Object;
				}
			}
			
		return sql;
	}
	
	/**
	 * select语句取 sql 对象
	 * @param sql
	 * @return
	 */
	private String getSqlObj(String sql) {
		String obj=sql.substring(sql.lastIndexOf(" FORM ")+5,sql.lastIndexOf(" WHERE "));
		return obj;
	}
	
	/**
	 * 获取预sql语句
	 * @param jdbc
	 * @return
	 */
	private String getSrcSQL(SrcJdbc jdbc) {
		Map sqlmap=SrcConfig.SQL_MAP;
		String[] sqls=jdbc.getYsql().split("\\.");
		String sql="";
		if(sqls.length==2) {
			Map objmap=(Map) sqlmap.get(sqls[0]);
			sql=objmap.get(sqls[1])==null?"":objmap.get(sqls[1]).toString();
		}else if(sqls.length==3){
			jdbc.Pool=sqls[0];
			Map objmap=(Map) sqlmap.get(sqls[1]);
			if(objmap!=null) {
				sql=objmap.get(sqls[2])==null?"":objmap.get(sqls[2]).toString();
			}
		}
		
		Object obj=jdbc.getMap();
		//sql是否还有标签需要处理
		if(!"".equals(sql)) {
			  sql=SrcLabelUtil.initSrcList(sql,obj);
		      //无list页面处理ifelse
			  sql=SrcLabelUtil.initSrcIfelse(sql,obj,SrcConfig.SRC_IFELSE,0);
			  sql=SrcLabelUtil.initSrcIfelse(sql,obj,SrcConfig.SRC_IF,0);
		      //处理独立绑定变量
			  sql=SrcLabelUtil.toSrcBind(sql,obj,"'",0);
		}
		
		if(sql!=null && sql.length()>0) {
			sql= sql.replaceAll("[\\t\\r\\n]", " ");
		}
		return sql;
	}
	
	/**
	 * 通过预sqlid获取数据源名
	 * //定义sqlid是数据源名+.+文件名+.+sqlId
	 * @param sqlid
	 * @return
	 */
	private String getConnNameBySqlId(SrcJdbc jdbc) {
		String sqlid = jdbc.getYsql();
		String[] sqlids=sqlid.split("\\.");
		String connName="";
		if(sqlids.length==2) { //有数据源情况数组长度大于1
			connName=jdbc.getPool(); //默认的数据源名
		}else if(sqlids.length==3){
			connName=sqlids[0];
		}
		return connName;
	}
	
	/**
	 *已过时 使用getconnPoolName
	 * 通过SrcJdbc.Object获取数据源名
	 * //定义objectName是数据源名+.+表名
	 * @param sqlid
	 * @return
	 */
	private String getConnNameByObject(String objectName) {
		String connName="";
		if(objectName.indexOf(".")<0) { 
			connName=SrcConfig.SRCDFPOOL; //默认的数据源名
		}else {
			String[] object=objectName.split("\\.");
			connName=object[0].trim();
		}
		return connName;
	}
	
	/**
	 * 数据源名通过SrcJdbc.pool 指定
	 * @return
	 */
	public String getconnPoolName(SrcJdbc jdbc){
		if(jdbc==null) {
			return SrcConfig.SRCDFPOOL;
		}else {
			return jdbc.getPool()==null?SrcConfig.SRCDFPOOL:jdbc.getPool();
		}
	}

	private void findColm(List cols,Field[] fds,String col) {
		for(int i=0;i<fds.length;i++) {
			String fdname = fds[i].getName();
			if(fdname.equalsIgnoreCase(col)) {
				cols.add(col);
				return;
			}
		}
	}
	
	private String changeObject(String Object){
		if(Object.indexOf(".")>0) {
			return Object.substring(Object.lastIndexOf(".")+1);
		}
		return Object;
	}
	private boolean isColm(Field[] fds,String col) {
		for(int i=0;i<fds.length;i++) {
			String fdname = fds[i].getName();
			if(fdname.equalsIgnoreCase(col)) {
				return true;
			}
		}
		return false;
	}


	public String initNumSQL(String sql,String connName,int number) {
		StringBuffer pageSql = new StringBuffer();
		String dbType=SrcPoolManage.getDbType(connName);

		if(("oracle").equals(dbType)){
			pageSql.append("select * from (select tmp_tb.*,ROWNUM row_id from (");
			pageSql.append(sql);
			pageSql.append(") tmp_tb where ROWNUM<=");
			pageSql.append(number);
			pageSql.append(") where row_id>");
			pageSql.append(0);
		}else if("mysql".equals(dbType)){
			pageSql.append(sql+" limit "+number);
			
		}else if("mssqlserver".equals(dbType)){
			sql = sql.replace("select ", "");
			pageSql.append("select top "+number+" "+sql);
		}else if("db2".equals(dbType)){
			pageSql.append(sql+" first "+number+"  rows only");
		}
		return  pageSql+"";
	}



}
