package com.srczh.manage;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.srczh.manage.interna.Internation;
import com.srczh.manage.util.Slog;
import com.srczh.manage.util.SrcIo;

/**
 * SrcFilter 重写Filter
 * @author zhoujun
 */
@SuppressWarnings("all")
public class SrcFilter implements Filter {
	private static String encoding; 
	
	//i18n输出
	String notfile= Internation.out(SrcConfig.SRC_LANGUAGE, Internation.notfile);
	String retuntxt=Internation.out(SrcConfig.SRC_LANGUAGE, Internation.retuntxt);
	
	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain fchain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletResponse resp = (HttpServletResponse) arg1;
		req.setCharacterEncoding(encoding);
		resp.setCharacterEncoding(encoding);

		SrcResponseWrapper mResp = new SrcResponseWrapper(resp); // 包装响应对象 resp 并缓存响应数据
		//解决跨域 session等问题
		resp.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
		resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		resp.setHeader("Access-Control-Max-Age", "3600");
		resp.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		resp.setHeader("Access-Control-Allow-Credentials","true"); //是否支持cookie跨域
		fchain.doFilter(req, mResp);
		Collection<String> hdds=mResp.getHeaderNames();
		
		//文件下载
		String chf=mResp.getHeader("Content-file")==null?"":mResp.getHeader("Content-file");
		String chd=mResp.getHeader("Content-Disposition")==null?"":mResp.getHeader("Content-Disposition");
		
		
		//纯文件下载
		//不需要处理内容
		if(!"".equals(chd)&& "src-down".equals(chf)){
			resp.getOutputStream().write(mResp.getBytes());
			resp.getOutputStream().flush();
			resp.getOutputStream().close();
			return;
		}
		//生成的图片
		if("srcview/img;charset=UTF-8".equals(chf)) {
			resp.getOutputStream().write(mResp.getBytes());
			resp.getOutputStream().flush();
			resp.getOutputStream().close();
			return;
		}
		//src_include
		/*
		 * if("srczh/include;charset=UTF-8".equals(chf)) { byte[] bytes =
		 * mResp.getBytes(); // 获取缓存的响应数据 String code=new String(bytes,encoding);
		 * //需要返回的页面 }
		 */
		String url = req.getRequestURI();
		byte[] bytes = mResp.getBytes(); // 获取缓存的响应数据
		String code=new String(bytes,encoding);  //需要返回的页面
		
		
		//如果是json
		String ctype="";
		if(url.indexOf(".")>0) {
			 ctype=url.substring(url.lastIndexOf(".")+1).toLowerCase();
		}
		String cname ="";
		if("js".equals(ctype)) {
			cname=url.substring(url.lastIndexOf("/")+1);
		}

		//jsp,html,htm,txt,doc,svc,
		if(!"srcview/file;charset=UTF-8".equals(chf)&&  !ctype.equals("eot")&&!ctype.equals("svg")&&!ctype.equals("ttf")&&!ctype.equals("less")&&
			!ctype.equals("font")&& !ctype.equals("png")&& !ctype.equals("jpg")&&!ctype.equals("woff")&&!ctype.equals("woff2")&&!ctype.equals("otf")&&
			!ctype.equals("gif")&&!ctype.equals("jpgb")&& !ctype.equals("png")&& !ctype.equals("ico")&&!ctype.equals("scss")&&!ctype.equals("txt")&&!ctype.equals("swf")){
					
			//application
			ServletContext context = null;
			try {
				context=req.getServletContext();
			}catch(Exception e){
			}
			
			//能够得到session中的值
			HttpSession session = null;
			String sessionId="";
			try {
				session=req.getSession();
				sessionId=session.getId();
			}catch(Exception e){
			}
			
			//去除html注释
			code=SrcLabelUtil.initNotes(code);
			
			//src_import标签 处理
			 Pattern ir = Pattern.compile(SrcConfig.SRC_IMPORT);
			 Matcher im = ir.matcher(code);
			 while(im.find()) {
				  String imtop=im.group(1).trim();
		    	  if(imtop!=null&&!"".equals(imtop.trim())){
		    		  resp.sendRedirect(imtop+"");
			    	  return;
		    	  }
			 }
			 
			 //src_include包含处理
			 Pattern irc = Pattern.compile(SrcConfig.SRC_INCLUDE);
			 Matcher imc = irc.matcher(code);
			 while(imc.find()) {
				  String imtop=imc.group(1).trim();
		    	  String imbqt=imc.group();
		    	  if(imtop!=null&&!"".equals(imtop.trim())){	    		  
		    		String inhtm="";
					try {
						String pr="";
						if(arg0.getServerPort()!=80 &&arg0.getServerPort()!=443) {
							pr=":"+arg0.getServerPort();
						}
						String htt=arg0.getScheme();
						String spth=htt+"://"+SrcConfig.DOMAIN+pr+imtop+"?JSESSIONID="+sessionId;
						
						if(htt.indexOf("https")>-1) {
							//inhtm=SrcIo.doReadHttps(spth,sessionId); //共享版不支持
						}else {
							inhtm=SrcIo.doRead(spth,sessionId);
						}
						Slog.println("include url=="+spth);
						//Slog.println("include html=="+inhtm);
					} catch (Exception e) {
						//Slog.error(notfile+": "+e.getMessage());
						e.printStackTrace();
					}
					code=code.replace(imbqt, inhtm);
		    	  }
			 }
			 
			
			//返回数据绑定
			Map obj = new HashMap<>();
			Enumeration<String> attributeNames = req.getAttributeNames();
			if (attributeNames != null) {
			    while (attributeNames.hasMoreElements()) {
			        String attName = attributeNames.nextElement();
			        if(attName=="org.apache.tomcat.util.net.secure_protocol_version"||
			        		attName=="javax.servlet.request.key_size"||
			        		attName=="javax.servlet.request.ssl_session_mgr"||
			        		attName=="javax.servlet.request.cipher_suite"||
			        		attName=="javax.servlet.request.ssl_session_id"||
			        				attName=="org.apache.tomcat.util.net.secure_requested_ciphers"||
			        				attName=="org.apache.tomcat.util.net.secure_requested_protocol_versions"
			        		) {
			        	continue;
			        }
			        //new String(request.getParameter("username").getBytes("ISO-8859-1"),"utf-8");
			        Object attObj = req.getAttribute(attName);
			        if(attName=="src_bind" || "src_bind".equals(attName)) {
			        	obj.putAll((Map) attObj);
			        }else {
			        	obj.put(attName, attObj);
			        	Slog.println("	"+retuntxt+": "+attName+ " => "+ attObj);
			        }
				}
		    }
			//obj.put("src_session", session);
			//obj.put("src_servletContext", context);
			SrcLabelUtil.sess=session;
			SrcLabelUtil.apps=context;
			
			if(obj!=null && !"".equals(obj) ){
			  
			  //list标签
			  code=SrcLabelUtil.initSrcList(code,obj);
		      //无list页面处理ifelse
		      code=SrcLabelUtil.initSrcIfelse(code,obj,SrcConfig.SRC_IFELSE,0);
		      code=SrcLabelUtil.initSrcIfelse(code,obj,SrcConfig.SRC_IF,0);
		      //处理独立绑定变量
		      code=SrcLabelUtil.toSrcBind(code,obj,"",0);
		      code=SrcLabelUtil.out(code,obj,0);
		      //格式化
		      code=SrcLabelUtil.toSrcFormat(code,obj,0);
		      code=SrcLabelUtil.toSrcWhen(code,obj,0);
		      code=SrcLabelUtil.keepSrcBind(code);
			}
			bytes=code.getBytes();
			obj=null;
		}
		
		
		
		//如果是文件导出
		//response.setContentType("application/vnd.ms-excel");
		//response.setHeader("Content-type","application/pdf");
		if(chf.length()>0 && bytes.length>0) {

			//导出下载word
			if("srcview/doc;charset=UTF-8".equals(chf)) {
				mResp.setContentType("application/msword");
				mResp.setHeader("Content-disposition","attachment; filename=templater.doc");
				
			//导出excel
			}else if("srcview/csv;charset=UTF-8".equals(chf)) {
				mResp.setContentType("application/vnd.ms-excel");
				mResp.setHeader("Content-disposition","attachment; filename=templater.csv");
			}
			
			resp.getOutputStream().write(bytes);
			resp.getOutputStream().flush();
			resp.getOutputStream().close();
			return;
		}

		//如果js文件需要加密
		if("true".equalsIgnoreCase(SrcConfig.SRC_ENCRYSCRIPT) && ctype.equals("js") && code.length()>0){
		//	Slog.print("	已加密js:"+cname,false);
			if(SrcConfig.SRC_ENCRYSCRIPT_FILE.indexOf(cname)>-1 ){
				//公告版不提供该方法
			}
		}
		
		//压缩内容 大于512才压缩
		String headEncoding = ((HttpServletRequest)arg0).getHeader("accept-encoding")==null?"":((HttpServletRequest)arg0).getHeader("accept-encoding");

		if("true".equals(SrcConfig.SRC_ZIP) && headEncoding.indexOf("gzip") != -1 && bytes.length>512){
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			GZIPOutputStream gzipOut = new GZIPOutputStream(bout); // 创建 GZIPOutputStream 对象

			gzipOut.write(bytes); // 将响应的数据写到 Gzip 压缩流中
			gzipOut.flush();
			gzipOut.close(); // 将数据刷新到  bout 字节流数组

			byte[] bts = bout.toByteArray();
			String uri= req.getRequestURI();
			//Slog.println("压缩地址：" +uri );
			//Slog.println("压缩前大小：" + bytes.length);
			//Slog.println("压缩后大小：" + bts.length);
			resp.setHeader("Content-Encoding", "gzip"); // 设置响应头信息
			resp.setContentLength(bts.length);
			resp.getOutputStream().write(bts); // 将压缩数据响应给客户端
		}else{
			resp.setContentLength(bytes.length);
			resp.getOutputStream().write(bytes); // 将未压缩数据响应给客户端
		}
	}


	/**
	 * 拦截请求参数
	 */
	private String getRequestBody(HttpServletRequest req) {
	    try {
	        BufferedReader reader = req.getReader();
	        StringBuffer sb = new StringBuffer();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	        String json = sb.toString();
	        return json;
	    } catch (IOException e) {
	        Slog.error("read error! ");
	    }
	    return "";
	}
	


	@Override
	public void init(FilterConfig fconfig) throws ServletException {
		encoding = fconfig.getInitParameter("srcEncoding");
	}
	
	
	
	//取html标签属性
    public static List<String> match(String source, String element, String attr) {
        List<String> result = new ArrayList<String>();
        String reg = "<\\s*" + element + "[^<>]*?\\s" + attr + "=['\"]?\\s*(.*?)['\"]?\\s.*?>";
        Matcher m = Pattern.compile(reg).matcher(source);
        while (m.find()) {
            String r = m.group(1);
            result.add(r);
        }
        return result;
    }
    
    
    
    private boolean isFilterRequest(HttpServletRequest request) {
    	String [] excludedPathArray= new String[]{"*.ico","/css/*","/fonts/*","/image/*",""};
    	if(null != excludedPathArray && excludedPathArray.length > 0) {
            String url = request.getRequestURI();
            for (String ecludedUrl : excludedPathArray) {
                if (ecludedUrl.startsWith("*.")) {
                    // 如果配置的是后缀匹配, 则把前面的*号干掉，然后用endWith来判断
                    if(url.endsWith(ecludedUrl.substring(1))){
                        return true;
                    }
                } else if (ecludedUrl.endsWith("/*")) {
                    if(!ecludedUrl.startsWith("/")) {
                        // 前缀匹配，必须要是/开头
                        ecludedUrl = "/" + ecludedUrl;
                    }
                    // 如果配置是前缀匹配, 则把最后的*号干掉，然后startWith来判断
                    String prffixStr = request.getContextPath() + ecludedUrl.substring(0, ecludedUrl.length() - 1);
                    if(url.startsWith(prffixStr)) {
                        return true;
                    }
                } else {
                    // 如果不是前缀匹配也不是后缀匹配,那就是全路径匹配
                    if(!ecludedUrl.startsWith("/")) {
                        // 全路径匹配，也必须要是/开头
                        ecludedUrl = "/" + ecludedUrl;
                    }
                    String targetUrl = request.getContextPath() + ecludedUrl;
                    if(url.equals(targetUrl)) {
                        return true;
                    }
                }
            }
        }
        return false;
      }

    
    /**
     * 通过URL使用POST方式 获取该网站的源码
     * @param url 请求的地址
     * @param param 请求的参数
     * @return 返回请求的结果
     */
    private  String getURLSource(String url,String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        StringBuilder htmlResult = new StringBuilder();
        try
        {
            URL realUrl = new URL(url);
            //打开和URL之间的连接
            HttpURLConnection conn = (HttpURLConnection)realUrl.openConnection();
            //设置请求的方式
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(5 * 1000);
            //设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            // charset=UTF-8以防止乱码！
            conn.setRequestProperty("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
            //发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            //获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            //发送请求参数
            out.println(param);
            //flush输出流的缓冲
            out.flush();
            //定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine())!= null)
            {
               // if(StringUtils.isNotBlank(line)){
            	if(!line.isEmpty()) {
                    htmlResult.append("\n" + line);
                }
            }
        }
        catch(Exception e){
            //throw new RuntimeException("发送POST请求出现异常！",e);
            Slog.error("include error");
        }
        //使用finally块来关闭输出流、输入流
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
                if (in != null)
                {
                    in.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        return htmlResult.toString();
    }

}


