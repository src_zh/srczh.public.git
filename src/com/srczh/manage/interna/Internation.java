package com.srczh.manage.interna;

public enum Internation {

	noconfig("未找到配置文件,使用默认配置..","Profile not found, use default configuration.."),
	onconfig("已加载配置文件 ","Profile loaded "),
	str("启动中...", "Starting..."), 
	fmemory("可用内存", "Free Memory"),
	cmemory("占用内存","Occupation Memory"),
	system("平台", "Platform System"),
	region("区域", "Region"),
	server("服务器","Server"),
	language("语言","Language"),
	environment("环境","Environment"),
	coding("编码","Coding"),
	AbsolutePath("绝对路径绝对路径","Absolute Path"),
	ControllerAddress("控制器地址","Controller Address"),
	Controller("控制器","Controller"),
	range("监控范围","Range"),//:/src/*
	visit("访问地址","Visit"),
	logAddr("日志路径","Log Address"),
	logLevel("日志输出等级","Log Level"),
	zip("启用zip压缩","Zip"),
	jsen("启用JavaScript加密","Javascript Encryption"),
	zhuru("已监控","Ccontrol"),
	
	gain("获取","Gain"),
	use("使用","Use"),
	free("空闲","Free"),
	activity("活动","Activity"),
	
	def("默认","default"),
	fail("失败","fall"),
	succ("成功","success"),
	retuntxt("返回","return"),
	loding("加载中","Loading"),
	rest("执行结果","results"),
	estab("创建","Establish"),
	wait("等待","Wait"),
	
	dblinknum("数据库连接池初始化连接数","Number of connection pool connections"),
	dbbing("创建数据库连接失败: ","Failed to create database connection: "),
	dbpoolerror("连接池不存在! ","Connection pool does not exist!"),
	dbpoolcolsefaild(" 关闭数据库连接失败"," Failed to close database connection"),
	
	poolSucc(" 数据库连接池创建成功!!"," SrcPool Create Success!!"),
	poolfail(" 数据库连接池创建失败!!"," SrcPool Create Failure!!"),
	jdbcerror("jdbc连接参数错误, 启动停止! ! !","JDBC connection parameter error!"),
	srcjdbcpool("实例化数据连接池","Instantiate data connection pool "),
	jdbctest("DB尝试连接","DB attempts to connect"),
	dbpoolwarning("警告!  没有获取到jdbc配置! ! !","Warning! No JDBC configuration obtained!!!"),
	sqlpoolerror("数据源配置读取失败!","Data source configuration read failed"),
	notjdbcDerver("没有发现JDBC驱动 ","No jdbc driver found "),
	closeConn("关闭连接","Close connections"),
	
	notfile("找不到文件","cannot find file"),
	notaction("找不到处理方法","Processing method not found"),
	urlfail("无效请求","invalid request"),
	initfail("实例化失败","Instantiation failed"),
	pamfail("参数异常","Parameter exception"),
	pemfail("内部错误","Permission exception"),
	profail("处理失败","Processing failure"),
	execfail("执行失败","Processing failure"),
	
	requesttxt("请求","request"),
	responsetxt("响应","response"),
	parameter("参数","parameter"),
	execution("执行","execution"),
	keep("保持","keep"),
	loginfail("登陆已失效, 请登录! ","Login is invalid, please login! "),
	
	plclass("处理类","Processing class"),
	plaction("处理方法","Processing method"),
	
	cacheA("一级缓存","First level cache"),
	cacheB("二级缓存","First level cache"),
	;
	
    private String cn;  
    private String en;  
    public  String bl;
    
    private Internation(String cn, String en) {  
        this.cn = cn;  
        this.en = en;  
    }
    
    public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getEn() {
		return en;
	}

	public void setEn(String en) {
		this.en = en;
	}

	public  String getBl() {
		return bl;
	}

	public void setBl(String bl) {
		this.bl = bl;
	}

	/**
	 * 国际化输出
	 * @param bl
	 * @param mess
	 * @return
	 */
	public static String out(String bl,Internation mess) {
    	if(bl.equalsIgnoreCase("ZH")) {
    		return mess.getCn();
    	}else {
    		return mess.getEn();
    	}
    }
    
    public static void main(String[] ss) {
    	String s=Internation.out("cn", Internation.str);
    	System.out.println(s);
    }
}
