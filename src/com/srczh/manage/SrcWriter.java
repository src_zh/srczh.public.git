package com.srczh.manage;

import java.io.PrintWriter;
import java.io.Writer;

/**
 * SrcWriter 重写PrintWriter
 * @author zhoujun
 */
public class SrcWriter extends PrintWriter{
	
	private StringBuilder buffer;
	 
	public SrcWriter(Writer out) {
		super(out);
		buffer = new StringBuilder();
	}
 
	@Override
	public void write(char[] buf, int off, int len) {
		// super.write(buf, off, len);
		char[] dest = new char[len];
		System.arraycopy(buf, off, dest, 0, len);
		buffer.append(dest);
	}
 
	@Override
	public void write(char[] buf) {
		super.write(buf);
	}
 
	@Override
	public void write(int c) {
		super.write(c);
	}
 
	@Override
	public void write(String s, int off, int len) {
		super.write(s, off, len);
	}
 
	@Override
	public void write(String s) {
		super.write(s);
	}
	
	public String getContent(){
		return buffer.toString();
	}

}
