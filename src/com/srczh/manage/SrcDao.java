package com.srczh.manage;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.srczh.manage.jdbc.SrcJdbc;
import com.srczh.manage.util.Page;
import com.srczh.manage.util.SrcJson;

/**
 * SrcDao 数据库操作主接口
 * @author zhoujun
 */
public abstract class SrcDao {
	
	public static SrcDao srcDao=null;
	
	public SrcDao() {
		srcDao=getInstance();
	}
	
	/**
	 * 线程安全  类的静态属性只会在第一次加载类的时候初始化。 线程安全，延迟加载，效率高
	 */
    private static class SrcDaoInstance {
        private static final SrcDao INSTANCE = new SrcDaoImpl();
    }
    private static SrcDao getInstance() {
        return SrcDaoInstance.INSTANCE;
    }
	

	/**
	 * 执行SQL语句
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
    public abstract Object execute(String sql, Object[] params)throws SQLException;
    /**
     * 执行SQL语句
     * @param sql
     * @param params  
     * @param connName  
     * @return
     * @throws SQLException
     */
    public abstract Object execute(String sql, Object[] params,String connName)throws SQLException;
		
	/**
	 * 按Map新增
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
    public abstract Object insert(SrcJdbc jdbc,Map<String,Object> map)throws SQLException;
	/**
	 * 按实体类新增
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
    public abstract Object insert(SrcJdbc jdbc,Object obj)throws SQLException;
	/**
	 * 按Json新增
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
    public abstract Object insert(SrcJdbc jdbc,SrcJson obj)throws SQLException;
	
	/**
	 * 按SQL语句，预编译数据对象新增
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
    public abstract Object insert(String sql,Object obj)throws SQLException;
    /**
     * 按sql语句，预编译参数数组新增
     * @param sql
     * @param params
     * @return
     * @throws SQLException
     */
    public abstract Object insert(String sql,Object[] params)throws SQLException;
    /**
     * 按sql语句，预编译参数数组，链接数据源名新增
     * @param sql
     * @param params
     * @param connName
     * @return
     * @throws SQLException
     */
    public abstract Object insert(String sql,Object[] params,String connName)throws SQLException;
    /**
     * 按sql语句，预编译Map数据新增
     * @param sql
     * @param map
     * @return
     * @throws SQLException
     */
    public abstract Object insert(String sql,Map<String,Object> map)throws SQLException ;
	/**
	 * 批量新增 
	 * @param jdbc
	 * @param list
	 * @return
	 */
    public abstract Object insert(SrcJdbc jdbc,List<Object> list);
	
	
	/**
	 * 按SrcJdbc，Map参数修改
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
    public abstract int update(SrcJdbc jdbc,Map<String,Object> map)throws SQLException;
	/**
	 * 按SrcJdbc，Object参数修改
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
    public abstract int update(SrcJdbc jdbc,Object obj)throws SQLException;
	
	/**
	 * 按SrcJdbc，SrcJson参数修改
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
    public abstract int update(SrcJdbc jdbc,SrcJson obj)throws SQLException;
	
	/**
	 * 按SQL语句，参数数组修改
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
    public abstract int update(String sql,Object[] params)throws SQLException;
    /**
     * 按SQL语句，参数数组，数据源连接名修改
     * @param sql
     * @param params
     * @param connName
     * @return
     * @throws SQLException
     */
    public abstract int update(String sql,Object[] params,String connName)throws SQLException;
    /**
     * 按SQL语句，Map参数修改
     * @param sql
     * @param map
     * @return
     * @throws SQLException
     */
    public abstract int update(String sql,Map<String, Object> map)throws SQLException;
	/**
	 * 批量修改 list数据中必须是Map
	 * map中需要set值和where值（where可以无，1个或多个）
	 * @param jdbc
	 * @param list
	 * @return
	 */
    public abstract int update(SrcJdbc jdbc,List<?> list) throws SQLException;
	
	/**
	 * 按SrcJdbc对象，Map参数删除
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
    public abstract int delete(SrcJdbc jdbc,Map<String,Object> map)throws SQLException;
    /**
     * 按SrcJdbc对象，Object参数对象删除
     * @param jdbc
     * @param obj
     * @return
     * @throws SQLException
     */
	public abstract int delete(SrcJdbc jdbc,Object obj)throws SQLException;
	/**
	 *  按SrcJdbc对象，Json参数删除
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public abstract int delete(SrcJdbc jdbc,SrcJson obj)throws SQLException;
	
	/**
	 * 按SQL语句，数组参数删除
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public abstract int delete(String sql,Object[] params)throws SQLException;
	/**
	 * 按SQL语句，数组参数，数据源名进行删除
	 * @param sql
	 * @param params
	 * @param connName
	 * @return
	 * @throws SQLException
	 */
	public abstract int delete(String sql,Object[] params,String connName)throws SQLException;
	/**
	 * 按SQL语句，Map参数进行删除
	 * @param sql
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public abstract int delete(String sql,Map<String,Object> map)throws SQLException;
	/**
	 * 按SrcJdbc对象进行查询
	 * @param jdbc
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(SrcJdbc jdbc) throws SQLException;
	/**
	 * 按SrcJdbc对象，Map参数查询
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(SrcJdbc jdbc,Map<String,Object> map) throws SQLException;
	/**
	 * 按SrcJdbc对象，Object数组进行查询
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(SrcJdbc jdbc,Object[] obj) throws SQLException;

	/**
	 * 按SQL语句，Object参数数组查询
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(String sql, Object[] params)throws SQLException;
	/**
	 * 按SQL语句，Object参数数组，数据源名查询
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(String sql, Object[] params,String connName)throws SQLException;
	/**
	 * 按SQL语句，数组参数，分页对象查询
	 * @param sql
	 * @param params
	 * @param page
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(String sql,Object[] params,Page page) throws SQLException;
	/**
	 * 按SQL语句，数组参数，数据源名，分页对象查询
	 * @param sql
	 * @param params
	 * @param connName
	 * @param page
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> select(String sql, Object[] params,String connName,Page page)throws SQLException;
	/**
	 * 按SQL语句，数组参数单条数据查询
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> get(String sql,Object[] params) throws SQLException;
	/**
	 * 按sql语句，数组参数，数据源名单条数据查询
	 * @param sql
	 * @param params
	 * @param connName
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> get(String sql,Object[] params,String connName) throws SQLException;
	/**
	 * 按Srcjdbc，Map参数对象单条数据查询
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> get(SrcJdbc jdbc,Map<String,Object> map) throws SQLException;
	/**
	 * 按jdbc对象，Object实体对象单条数据查询
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> get(SrcJdbc jdbc,Object obj) throws SQLException;
	/**
	 * 按 Srcjdbc，Map参数进行缓存查询
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> cacheSelect(SrcJdbc jdbc,Map<String,Object> map) throws SQLException;
	/**
	 * 按SQL语句，数组参数进行缓存查询
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> cacheSelect(String sql,Object[] params)throws SQLException;
	/**
	 * 按SQL语句，数组参数，数据源名进行缓存查询
	 * @param sql
	 * @param params
	 * @param connName
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Object> cacheSelect(String sql,Object[] params,String connName)throws SQLException;
	
	/**
	 * 按SQL语句，数组参数进行单条缓存查询
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> cacheGet(String sql,Object[] params) throws SQLException;
	/**
	 * 按SQL语句，数组参数，数据源名进行单条缓存查询
	 * @param sql
	 * @param params
	 * @param connName
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> cacheGet(String sql,Object[] params,String connName) throws SQLException;
	/**
	 * 按SrcJdbc对象，Map参数进行缓存单条查询
	 * @param jdbc
	 * @param map
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> cacheGet(SrcJdbc jdbc,Map<String,Object> map) throws SQLException;
	/**
	 * 按SrcJdbc对象，Object参数进行缓存单条查询
	 * @param jdbc
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public abstract Map<String,Object> cacheGet(SrcJdbc jdbc,Object obj) throws SQLException;
	
	
	/**
	 * 按Srcjdbc列表进行批量处理
	 * @param ls
	 * @return
	 */
	public abstract String[] BatchSrcJdbc(List<SrcJdbc> ls);
	/**
	 * 按Srcjdbc列表，参数对象列表进行批量处理
	 * @param ls
	 * @param dls
	 * @return
	 */
	public abstract String[] BatchSrcJdbc(List<String> ls,List<Object[]> dls);
	/**
	 * 按Srcjdbc列表，参数对象列表，数据源名进行批量处理
	 * @param ls
	 * @param dls
	 * @param connName
	 * @return
	 */
	public abstract String[] BatchSrcJdbc(List<String> ls,List<Object[]> dls,String connName);
	/**
	 * 调用存储过程
	 * @param call 过程名称
	 * @param data 传入数据
	 * @param out  传出数据
	 * @return
	 */
	public abstract Map<String,Object> call(String call,Object[] data,Object[] out);
	/**
	 * 调用存储过程
	 * @param call 过程名称
	 * @param data 传入数据
	 * @param out  传出数据
	 * @param connName 数据源名称
	 * @return
	 */
	public abstract Map<String,Object> call(String call,Object[] data,Object[] out,String connName);
}
