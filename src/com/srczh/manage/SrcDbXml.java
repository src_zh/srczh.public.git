package com.srczh.manage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.srczh.manage.util.Slog;
import com.srczh.manage.util.SrcIo;

/**
 * srcXml解析类
 * @author zhoujun
 */
@SuppressWarnings("all")
public class SrcDbXml {

	private String sqlpath;
	
	public SrcDbXml() {
		super();
	}
	public SrcDbXml(String sqlpath) {
		super();
		this.sqlpath = sqlpath;
	}

	/**
	 * 获取所有的sql配置文件列表
	 * @return
	 */
	public Map getSqlFelds() {
		SrcIo sio=new SrcIo(sqlpath);
		Map fls=sio.getFelds("srcsql", "file");
		return fls;
	}
	/**
	 * 取sql文件内容
	 * @param file
	 * @return
	 */
	public String getSqlxml(String file) {
		try {
			return SrcIo.doReadFile(sqlpath+System.getProperty("file.separator")+file).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * 获取所有的sql配置sql
	 * @return
	 */
	public Map getSqls(Map filemap) {
		Map sqlmap=new HashMap<>();
		List fls=(List) filemap.get("srcsql");
		SrcDbXml srcxml=new SrcDbXml();
		for(int i=0;i<fls.size();i++) {
			String fname=fls.get(i).toString();
			String fkey=fname.substring(0,fname.lastIndexOf("."));
			String ftype=fname.substring(fname.lastIndexOf(".")+1);
			if("xml".equals(ftype)) {
				String sql=getSqlxml(fname);
				sqlmap.put(fkey,srcxml.parsingSQL(sql));
			}
		}
		return sqlmap;
	}

	
	
	
	/**
	 * 节点正则
	 */
	private String xnode="<(?!/|\\?)(.*?)>";
	/**节点内容正则*/
	private String nodeContentRegexTemplate="<#node#>(.*?)</#node#>";
	/**元素属性正则*/
	private String elementRegex="\\s{0,}(.*?)\\s{0,}=\\s{0,}\"(.*?)\"";
	private String outStr="";
	
	
	String sqlRegex="\\<\\s*sql\\s+id\\=[\\'\\\"](.*)[\\'\\\"].*\\s*\\>([\\s\\S]*?)\\<\\/\\s*sql\\s*\\>";
	
	/**
	 * 解析预SQL
	 * @param inStr
	 * @return
	 */
	public Map parsingSQL(String inStr) {
		Map psql=new HashMap<>();
	//	Matcher elementM=match(sqlRegex,inStr);
		Pattern r = Pattern.compile(sqlRegex);
		Matcher m = r.matcher(inStr);
		while(m.find()){
			String id=m.group(1);
			String sql=m.group(2);
			psql.put(id, sql);
		}
		return psql;
	}
	
	public void parsingXML(String inStr){
		Map root=new HashMap<>();
		String rootName=firstElementName(xnode,inStr);
		
		outStr+=rootName;
		String nodeContentRegex=nodeContentRegexTemplate.replace("#node#", rootName);
		Matcher rootContentM=match(nodeContentRegex,inStr);
		
		while(rootContentM.find()){
			String content=rootContentM.group(1);
			String nodeName=firstElementName(xnode,content);
			String nextNodeContentRegex=nodeContentRegexTemplate.replace("#node#", nodeName);
			Matcher nextContentM=match(nextNodeContentRegex,content);
			int i=1;
			Map obj;
			List ols=new ArrayList();
			while(nextContentM.find()){
				obj=new HashMap<>();
				outStr+="\r\n\t"+nodeName+(i++)+"\r\n\t\t";
				String elementContent=nextContentM.group(1);
				Matcher elementM=match(elementRegex,elementContent);
				
				Map par=new HashMap<>();
				while(elementM.find()){
					String pra=elementM.group(1).replace("<", "");
					String parms=elementM.group(2);
					par.put(pra, parms);
					outStr+=pra+":"+parms+"\r\n\t\t";
				}
				obj.put(nodeName, par);
				ols.add(obj);
			}
			root.put(rootName, ols);
		}
		Slog.println(root);
	}
	
	private Matcher match(String regex,String content){
		Pattern p=Pattern.compile(regex);
		Matcher m=p.matcher(content);
		return m;
	}
	
	/**
	 * 
	 * @Description: 匹配根元素
	 * @param regex
	 * @param content
	 * @return String
	 */
	private String firstElementName(String regex,String content){
		String firstElementName="";
		Matcher m=match(regex,content);
		while(m.find()){
			firstElementName=m.group(1);
			break;
		}
		return firstElementName;
	}
	
	
	public static void main(String args[]){
	//String in = "<?xml version=\"1.0\" ?><Books><Book><Name = \"The C++ Programming Language\" price=\"10.3\" Author=\"Bjarne Stroustrup\" /></Book><Book><Name = \"Effective C++\" Author = \"Scott Meyers\" /></Book></Books><as><a><name='zhou' age='22'></a></as>";
	//new SrcDbXml().parsingXML(in);
		String xmlpath="D:\\workspace\\srczh\\ROOT\\"+"WEB-INF\\classes\\srcsql";
		Map filemap=new SrcDbXml(xmlpath).getSqlFelds();
		//System.out.println(sqlmap);
		Map sqlmap=new HashMap<>();
		List fls=(List) filemap.get("srcsql");
		for(int i=0;i<fls.size();i++) {
			String fname=fls.get(i).toString();
			String fkey=fname.substring(0,fname.lastIndexOf("."));
			String ftype=fname.substring(fname.lastIndexOf(".")+1);
			if("xml".equals(ftype)) {
				//String sql=getSqlxml(xmlpath+"\\"+fname);
				//sqlmap.put(fkey,new SrcDbXml(xmlpath).parsingSQL(sql));
			}
			
		}
		//sqlmap.put(key, value)
		Slog.println(sqlmap);
	}

	   
}
