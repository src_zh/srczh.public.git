package com.srczh.manage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.srczh.manage.pool.SrcPoolBean;

/**
 * SrcConfig 配置定义类
 * @author zhoujun
 */
public class SrcConfig {

	public static final String WEB_LOGIN="weblogin";
	public static final String ADMIN_LOGON="adminlogin";
	public static final String APP_LOGIN="applogin";
	public static final String SSH_SERVER="sshserver";
	/*src prarm*/
    private static SrcConfig srcConfig = null;
    public static String DOMAIN; 
	public static String PACKAGE; 
	public static String CONTROL;
	public static String RANGE;
	public static Object ACTIONS;
	public static Object DAOMAP;
	public static String RUNTIME;
	public static String RUNTIMEVERSION;
	public static List<SrcPoolBean> SRCPOOLLIST;
	
	public static String POOL;
	public static Boolean SQL_LODING; 
	public static Map<String,Object> SQL_MAP; 
	
	public static String LOG_LEVEL;
	public static String LOG_PATH;
	
	public static String BASEPATH;
	public static String UPLOAD_PATH; 
	
	public static String SRC_CODING; 
	public static String SRC_ZIP; 
	public static String SRC_LANGUAGE;
	public static String SRC_ENCRYSCRIPT;
	public static String SRC_ENCRYSCRIPT_FILE;
	public static String SRC_OS;
	public static String SRC_OSVERSION;
	public static String SRC_HHF;
	public static String SRC_WJF;
	
	public static int SESSION_TIME_OUT;
	public static String SESSION_INVALID;
	public static String SRC_TASK;
	public static String SRC_TASK_TIMES;
	public static String MONITER;
	public static String MONITER_COMMD;
	public static String PAMAMETERSHIELDING;
	
	public static String SSLKEY;
	public static String SSLPASSWD;
	public static String GRANT;
	public static String V="V1.1.12";
	public static String LOGOPTIMIZE="0";
	
	public static  String SRCLOG="  ____                  _         "+"\r\n"+" / ___' _ _   ___ ____ | |__     "+"\r\n"+" \\___ \\| '_| / _||_/ / | |_  \\   "+"\r\n"+"  ___)|| |  |( _  / /_ | | | |   "+"\r\n"+ " |____||_|   \\__|/_/__||_| |_|  "+V;

//	Slog.println("     j____h");
//	Slog.println("   r/___s/|");
//	Slog.println("   | a__|_r");
//	Slog.println("   c/___z/");

	
	
	public static Map<String,Object> dmap=new HashMap<>();
	
	//default pool
	public static String SRCDFPOOL="srcjdbcDefulat";
	//ysql
	public static String SRCSQL ="srcsql";

	static String SRC_NOTES="<!--([\\s\\S]*?)-->?";
	static String SRC_LIST="\\<\\s*src_list\\s+name\\=[\\'\\\"](.*)[\\'\\\"].*\\s*\\>([\\s\\S]*?)\\<\\/src_list\\>{1}";
	public static String SRC_IFELSE="\\<\\s*src_ifelse\\s+whe\\=[\\'\\\"](.*)[\\'\\\"].*\\s*\\>([\\s\\S]*?)\\<\\s*src_else\\s*\\>([\\s\\S]*?)\\<\\/\\s*src_ifelse\\s*\\>{1}";
	public static String SRC_IF="\\<\\s*src_if\\s+whe\\=[\\'\\\"](.*)[\\'\\\"].*\\s*\\/?\\>([\\s\\S]*?)\\<\\/src_if\\>{1}";

	static String SRC_ELSE="\\<\\s*src_else\\s*\\>[\\s\\S]*?\\<\\/\\s*src_else\\s*\\>{1}";
	static String SRC_SYH="(?<=\\\').*?(?=\\\')|(?<=\\\").*?(?=\\\")"; 
	static String SRC_FMT="\\<src_fmt\\s+name\\=[\\'\\\"](.*?)[\\'\\\"]\\s+type\\=[\\'\\\"](.*?)[\\'\\\"]\\s*\\/?\\>\\<\\/src_fmt\\>{1}";
	static String SRC_OUT="\\<src_out\\s+type\\=[\\'\\\"](.*?)[\\'\\\"]\\>([\\s\\S]*?)\\<\\/src_out\\>{1}";
	static String SRC_BIND="\\{\\[(.*?)\\]\\}{1}";
	static String SRC_KEEPBIND="\\{\\!\\[(.*?)\\]\\}{1}";
	static String SRC_IMPORT="\\<\\s*src_import\\s+url\\=[\\'\\\"](.*?)[\\'\\\"].*\\s*\\>\\<\\/\\s*src_import\\s*\\>{1}";  
	static String SRC_INCLUDE="\\<\\s*src_include\\s+url\\=[\\'\\\"](.*?)[\\'\\\"].*\\s*\\>\\<\\/\\s*src_include\\s*\\>{1}"; 
	static String SRC_WHEN="\\<\\s*src_when\\s+name\\=[\\'\\\"](.*)[\\'\\\"]\\s+whe\\=[\\'\\\"](.*)[\\'\\\"]\\s*\\/?\\>\\<\\/\\s*src_when\\s*\\>{1}";
	static String SRC_ARRAY="\\<\\s*src_array\\s+name\\=[\\'\\\"](.*?)[\\'\\\"]\\s+char\\=[\\'\\\"](.*)[\\'\\\"]\\s*\\/?\\>{1}";
	static  String SRC_BQ="<.*\\s*=\\s*['\\\"](.*?\\<?\\>?)['\\\"]\\s?>";
	//"{((?<G>{)|}(?<-G>)|.*?)*}"
	String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>?"; 
	String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>?"; 
	String regEx_html = "<[^>]+>"; 

	public static int PAGENO;
	public static int PAGENUM;
	public static int SHOWNUM; 

	public SrcConfig() {
		super();
	}
	
	public SrcConfig(Map<String,Object> map) {
		super();
		MONITER=map.get("moniter")==null?"":map.get("moniter")+"";
		MONITER_COMMD=map.get("moniter_commd")==null?"":map.get("moniter_commd")+"";
		DOMAIN=map.get("domain")==null?"":map.get("domain")+"";
		RANGE=map.get("range")==null?"":map.get("range")+"";
		PACKAGE=map.get("package")==null?"":map.get("package")+"";
		CONTROL=map.get("control")==null?"":map.get("control")+"";
		DAOMAP=map.get("daoImpl")==null?new HashMap<String,Object>():map.get("daoImpl");
		ACTIONS=map.get("actions")==null?new ArrayList<String>():map.get("actions");
		
		POOL = map.get("pool")+"";
		SQL_LODING=map.get("SrcSQLPreLoding")=="true"?true:false;
		SQL_MAP=map.get("SrcSQL")==null?new HashMap<String,Object>(): (Map)map.get("SrcSQL");
		
		LOG_LEVEL=map.get("loglevel")==null?"":map.get("loglevel")+"";
		LOG_PATH=map.get("logpath")==null?"":map.get("logpath")+"";
		BASEPATH=map.get("basePath")==null?"":map.get("basePath")+"";
		
		SRC_CODING= map.get("coding")+"";
		SRC_ZIP= map.get("srczip")+"";
		SRC_ENCRYSCRIPT=map.get("encryScript")+"";
		SRC_ENCRYSCRIPT_FILE=map.get("encryScript_file")+"";
		
		SRC_OS=map.get("os").toString();
		SRC_LANGUAGE=map.get("oslanguage").toString();
		SRC_OSVERSION=map.get("osversion").toString();
		SRC_HHF=map.get("hhf").toString();
		SRC_WJF=map.get("wjf").toString();
		
		SESSION_TIME_OUT=Integer.parseInt(map.get("sessiontimeout").toString());
		SESSION_INVALID=map.get("sessiontinvalid")==null?"":map.get("sessiontinvalid").toString();
		SRC_TASK=map.get("srctask").toString();
		SRC_TASK_TIMES=map.get("srctasktimes")==null?"":map.get("srctasktimes").toString();
		
		PAMAMETERSHIELDING=map.get("parameterShielding").toString();
		
		RUNTIME=map.get("runtime").toString();
		RUNTIMEVERSION=map.get("runversion").toString();
		
		SSLKEY=map.get("sslkeys").toString();
		SSLPASSWD=map.get("sslpass").toString();
		GRANT=map.get("authorization").toString();
		V=map.get("v").toString();
		
		SRCPOOLLIST=map.get("srcpoolList")==null?new ArrayList<>():(ArrayList) map.get("srcpoolList");
	}
	
	public static synchronized SrcConfig initConfig(Map<String,Object> map){
        if (srcConfig == null) {
        	srcConfig = new SrcConfig(map);
        }
        return srcConfig;
	}
	

	
}
